package com.suncky.example

import android.app.Application
import com.suncky.frame.AFrame
import com.suncky.frame.http.config.OkHttpConfig
import com.suncky.frame.utils.LogUtils
import com.suncky.frame.utils.SingleClickManager
import okhttp3.logging.HttpLoggingInterceptor

/**
 * application
 * @author gl
 * @time 2021/4/13 10:32
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        //基础初始化(必须)，参数：context-上下文，debug-是否为debug模式，为true时有日志信息输出，为false时无日志输出
        AFrame.init(this, BuildConfig.DEBUG)
        //初始化http配置(可选)，使用http工具，需要先初始化http配置，基于retrofit2 + okhttp3开发，如不使用此工具可不进行http初始化
        AFrame.initHttp(OkHttpConfig.Builder()
//            .baseUrl("http://192.168.1.36")//设置接口服务地址
//            .connectOutTime(20)//连接超时时间(秒)，默认值20
//            .readOutTime(20)//IO读取超时时间(秒)，默认值20
//            .writeOutTime(20)//IO写入超时时间(秒)，默认值20
//            .addInterceptor(ResponseInterceptor())//添加拦截器
//            .addNetworkInterceptor(ParamsInterceptor())//添加网络拦截器
//            .cache(cache)//配置数据缓存
//            .cookieJar(CookieManger(this))//如需要，可配置cookie管理
//            .addConverterFactory(factory)//添加retrofit数据转换器
//            //https配置
//            .sslSocketFactory(sslSocketFactory)
//            .hostnameVerifier(hostnameVerifier)
            .logEnable(true)//log开关
            .setHttpLogger(object :HttpLoggingInterceptor.Logger{
                override fun log(message: String) {
                    LogUtils.d(message)
                }
            },HttpLoggingInterceptor.Level.BODY)//自定义log打印工具和日志级别，如不设置则使用okhttp默认的日志工具
            //使用默认配置(baseUrl必不可少),如果不想配置其他信息，可使用默认配置
            .defaultConfig("http://192.168.1.36"))

        SingleClickManager.clickInterval(500)
        LogUtils.setLogLevel(if (BuildConfig.DEBUG) LogUtils.LEVEL_ALL else LogUtils.LEVEL_NONE)
            .setGlobalTag("example").setLog2File(true)
    }
}