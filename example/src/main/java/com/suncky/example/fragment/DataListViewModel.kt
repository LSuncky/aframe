package com.suncky.example.fragment

import com.suncky.example.R
import com.suncky.example.bean.User
import com.suncky.example.http.coroutines.KHttpResult
import com.suncky.example.http.coroutines.KHttpStateLiveData
import com.suncky.example.http.coroutines.KViewModel
import kotlinx.coroutines.delay

class DataListViewModel:KViewModel() {
    val userList = KHttpStateLiveData<List<User>>()

    fun getData() {
        userList.onPreLoad()
        getRequest<List<User>>().execute({result ->
            userList.onLoadFinish()
            userList.value = result
        }){
            getUsers()
        }
    }

    //模拟网络数据请求
    private suspend fun getUsers():KHttpResult<List<User>> {
        delay(1000)
        val users = mutableListOf<User>()
        for (index in 1..10){
            val user = User(
                ("ID:"+(1000..index*1000).random()),
                listOf("杜归程","宁志远","夏风","汤剑","陈夕儿","落影","宁玉莲","陆小凤","叶孤城","西门雪","费通","荆无命","叶孤寒","叶凝霜").random(),
                R.drawable.thumb0,
                listOf("132", "136", "139", "153", "156","157", "158", "159", "182", "187")
                    .random() + (15378694..89567836).random()
            )
            users.add(user)
        }
        return KHttpResult.success(users,null)
    }
}