package com.suncky.example.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.suncky.example.R
import com.suncky.example.adapter.UserListAdapter
import com.suncky.example.databinding.LayoutRefreshRecyclerListBinding
import com.suncky.frame.base.BaseVmViewBindingFragment
import com.suncky.frame.widget.recyclerview.LinearDividerItemDecoration

class DataListFragment: BaseVmViewBindingFragment<LayoutRefreshRecyclerListBinding, DataListViewModel>() {

    private val adapter by lazy { UserListAdapter(requireContext()) }

    override fun initView(rootView: View?, savedInstanceState: Bundle?) {
        super.initView(rootView, savedInstanceState)
        binding.dataRv.layoutManager = LinearLayoutManager(context)
        binding.dataRv.adapter = adapter
        binding.dataRv.addItemDecoration(
            LinearDividerItemDecoration(
                2,
                resources.getColor(R.color.color_line),
                LinearDividerItemDecoration.VERTICAL
            ).also { it.setMargin(30) }
        )
        binding.refreshLayout.setEnableLoadMore(false)
        binding.refreshLayout.setOnRefreshListener {
            viewModel.getData()
        }
    }

    override fun initData(savedInstanceState: Bundle?) {
        super.initData(savedInstanceState)
        viewModel.userList.observeState(this){
            onSuccess={data, page ->
                adapter.data = data
            }
            onLoadFinish={
                binding.refreshLayout.finishRefresh()
            }
        }

    }

    override fun startLoading() {
        binding.refreshLayout.autoRefresh()
    }

    override fun isLazy(): Boolean {
        return true
    }
}