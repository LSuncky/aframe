package com.suncky.example.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.blankj.utilcode.util.SizeUtils
import com.suncky.example.databinding.ItemImageUploadListBinding
import com.suncky.frame.AppConfig
import com.suncky.frame.base.adapter.RecyclerViewBindingAdapter
import com.suncky.frame.base.adapter.holder.RecyclerViewBindingHolder

/**
 * 消纳场图片上传适配器
 * @author gl
 * @time 2021/5/6 18:13
 */
abstract class ImageUploadAdapter(context: Context, data: List<String>):RecyclerViewBindingAdapter<String, ItemImageUploadListBinding>(
    context, data
) {
    override fun onCreateDataBinding(parent: ViewGroup?, viewType:Int): ItemImageUploadListBinding {
        val binding=ItemImageUploadListBinding.inflate(LayoutInflater.from(mContext), parent, false)
        binding.llImage.getLayoutParams().width =
            (AppConfig.screenWidth - SizeUtils.dp2px(10f) * 2-SizeUtils.dp2px(15f) * 2) / 3
        binding.llImage.getLayoutParams().height = binding.llImage.getLayoutParams().width
        return binding
    }

    override fun convert(
        holder: RecyclerViewBindingHolder<ItemImageUploadListBinding>?,
        item: String?,
        position: Int
    ) {
        holder?.binding?.url=item
        holder?.binding?.position=position
        holder?.binding?.event=this
    }

    abstract fun onDelete(position: Int)
}