package com.suncky.example.adapter;

import android.content.Context;

import com.suncky.example.R;
import com.suncky.frame.base.adapter.RecyclerViewAdapter;
import com.suncky.frame.base.adapter.holder.RecyclerViewHolder;

import java.util.List;

public class StringRecyclerViewAdapter extends RecyclerViewAdapter<String> {
    public StringRecyclerViewAdapter(Context context, List<String> data) {
        super(context, data, R.layout.item_string_list);
    }

    @Override
    public void convert(RecyclerViewHolder holder, String item, int position) {
        holder.setText(R.id.text, item);
    }
}
