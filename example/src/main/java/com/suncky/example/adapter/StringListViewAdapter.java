package com.suncky.example.adapter;

import android.content.Context;

import com.suncky.example.R;
import com.suncky.frame.base.adapter.ListAdapter;
import com.suncky.frame.base.adapter.holder.ViewHolder;

public class StringListViewAdapter extends ListAdapter<String> {
    public StringListViewAdapter(Context context) {
        super(context,null, R.layout.item_string_list);
    }

    @Override
    public void convert(ViewHolder holder, String item, int position) {
        holder.setText(R.id.text, item);
    }
}
