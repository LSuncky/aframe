package com.suncky.example.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.suncky.example.databinding.ItemStringListBinding;
import com.suncky.frame.base.adapter.RecyclerViewBindingPageAdapter;
import com.suncky.frame.base.adapter.holder.RecyclerViewBindingHolder;

/**
 * @author gl
 * @time 2021/4/16 11:39
 */
public class StringListPagingAdapter extends RecyclerViewBindingPageAdapter<String, ItemStringListBinding> {
    public StringListPagingAdapter(Context context) {
        super(context, itemCallback);
    }

    @Override
    public ItemStringListBinding getDataBinding(@NonNull LayoutInflater inflater, ViewGroup parent, int viewType) {
        return ItemStringListBinding.inflate(inflater, parent, false);
    }

    @Override
    public void convert(RecyclerViewBindingHolder<ItemStringListBinding> holder, String item, int position) {
        holder.setText(holder.getBinding().text, position + item);
    }

    static final DiffUtil.ItemCallback<String> itemCallback=new DiffUtil.ItemCallback<String>() {
        @Override
        public boolean areItemsTheSame(@NonNull String oldItem, @NonNull String newItem) {
            return false;
        }

        @Override
        public boolean areContentsTheSame(@NonNull String oldItem, @NonNull String newItem) {
            return oldItem.equals(newItem);
        }
    };
}
