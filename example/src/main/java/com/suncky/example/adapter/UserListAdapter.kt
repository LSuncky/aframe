package com.suncky.example.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.suncky.example.bean.User
import com.suncky.example.databinding.ItemUserListBinding
import com.suncky.frame.base.adapter.RecyclerViewBindingAdapter
import com.suncky.frame.base.adapter.holder.RecyclerViewBindingHolder

class UserListAdapter(context: Context):RecyclerViewBindingAdapter<User,ItemUserListBinding>(context) {
    override fun onCreateDataBinding(inflater: LayoutInflater,parent: ViewGroup?, viewType: Int): ItemUserListBinding {
        return ItemUserListBinding.inflate(LayoutInflater.from(mContext), parent, false)
    }

    override fun convert(
        holder: RecyclerViewBindingHolder<ItemUserListBinding>,
        item: User?,
        position: Int,
    ) {
        holder.binding.data = item
    }
}