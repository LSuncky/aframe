package com.suncky.example.http.coroutines

import com.suncky.example.http.DataPage
import com.suncky.frame.http.coroutines.KResult
import com.suncky.frame.http.coroutines.KError
import kotlin.Exception

class KHttpResult<E>: KResult<E, DataPage> {
    private var data:E? = null
    private var code:Int? = null
    private var message:String? = null
    private var dataPage: DataPage? = null
    private var e: Exception? = null

    private constructor(data: E?, dataPage: DataPage?) {
        this.code = KError.SUCCESS.code
        this.data = data
        this.dataPage = dataPage
    }
    private constructor(code:Int?, message:String?) {
        this.code = code
        this.message = message
    }
    private constructor(error:KError?) {
        this.code = error?.code
        this.message = error?.message
    }

    private constructor(error:KError?, e:Exception?) {
        this.code = error?.code
        this.message = error?.message
        this.e = e
    }

    companion object{

        @JvmStatic
        fun <E> success(data: E?, dataPage: DataPage?):KHttpResult<E> {
            return KHttpResult(data, dataPage)
        }

        @JvmStatic
        fun <E,> error(code:Int?, message:String?):KHttpResult<E> {
            return KHttpResult(code, message)
        }
        @JvmStatic
        fun <E> error(error:KError?):KHttpResult<E>{
            return KHttpResult(error)
        }
        @JvmStatic
        fun <E> error(error:KError?, e:Exception):KHttpResult<E>{
            return KHttpResult(error,e)
        }
    }

    override fun getCode(): Int {
        return code?:0
    }

    override fun getData(): E? {
        return data
    }

    override fun getPage(): DataPage? {
        return dataPage
    }

    override fun getMessage(): String? {
        return message
    }

    override fun isSuccess(): Boolean {
        return code==KError.SUCCESS.code
    }

    override fun getException(): Exception? {
        return e
    }

    override fun setCode(code: Int?) {

    }

    override fun setPage(page: DataPage?) {

    }

    override fun setData(page: E?) {

    }

    override fun setMessage(message: String?) {

    }

    override fun setException(e: Exception?) {

    }

}