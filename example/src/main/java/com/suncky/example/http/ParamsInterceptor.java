package com.suncky.example.http;

import com.suncky.example.MyAccountManager;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 请求公共参数设置
 */
public class ParamsInterceptor implements Interceptor {

    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl.Builder urlBuilder = request.url().newBuilder();

        HttpUrl httpUrl = urlBuilder.build();
        Request.Builder reqBuilder = request.newBuilder().url(httpUrl);
        String token = MyAccountManager.getInstance().getToken();
//        if (token != null && token.length() != 0) {
            reqBuilder.addHeader("gd-auth-session", "04b08215-9e03-4142-8201-44479d44bf75");
//        }
        return chain.proceed(reqBuilder.build());
    }

}
