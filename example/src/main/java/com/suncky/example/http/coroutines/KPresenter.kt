package com.suncky.example.http.coroutines

import com.suncky.frame.base.mvp.IView
import com.suncky.frame.base.mvp.Presenter
import kotlinx.coroutines.cancel

open class KPresenter<V : IView> :Presenter<V>() {
    protected fun <E> getRequest():KHttpRequest<E> {
        return KHttpRequest.get()
    }

    fun updateView(action: (view : V)->Unit) {
        getView()?.let {
            action(it)
        }
    }

    override fun detach() {
        super.detach()
        getRequest<Any>().cancel()
    }
}