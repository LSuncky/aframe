package com.suncky.example.http;

import com.suncky.frame.base.mvvm.StateObserver;

public abstract class HttpStateObserver<E> extends StateObserver<E,DataPage,BaseHttpResult<E>> {
}
