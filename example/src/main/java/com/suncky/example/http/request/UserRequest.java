package com.suncky.example.http.request;

import com.suncky.example.bean.User;
import com.suncky.example.http.BaseHttpResult;
import com.suncky.example.http.param.LoginParam;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * author： gl
 * time： 2021/4/2 18:06
 * description：
 */
public interface UserRequest {
    /**
     * 用户名登录
     * @param param
     * @return
     */
    @POST("/user/login")
    Call<BaseHttpResult<User>> loginUsername(@Body LoginParam param);

    /**
     * 获取手机验证码
     * @return
     */
    @POST("/user/sendMsg")
    Call<BaseHttpResult<User>> getVerifyCode();

    /**
     * 验证码登录
     * @param code
     * @return
     */
    @POST("/user/login")
    @FormUrlEncoded
    Call<BaseHttpResult<User>> loginVerifyCode(@Field("dxCode") String code);

    @Multipart
    @POST("/office/upload/photos")
    Call<BaseHttpResult<List<String>>> uploadMulti(@Part("file") List<RequestBody> partList);

    @POST("/office/upload/photo")
    Call<BaseHttpResult<String>> upload(@Body RequestBody body);
}
