package com.suncky.example.http;

import java.util.List;

/**
 * 文件上传结果
 * @author gl
 * @time 2021/4/20 16:30
 */
public class UploadHttpResult extends BaseHttpResult<List<String>>{
}
