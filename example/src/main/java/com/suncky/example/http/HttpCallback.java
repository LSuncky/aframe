package com.suncky.example.http;

import androidx.annotation.NonNull;

import com.suncky.frame.http.DataCallback;

import retrofit2.Call;
import retrofit2.Response;

/**
 * 数据请求回调
 * @author： gl
 * @time： 2021/4/2 17:57
 */
public abstract class HttpCallback<E> extends DataCallback<E,DataPage,BaseHttpResult<E>> {

    @Override
    public void onResponse(@NonNull Call<BaseHttpResult<E>> call, Response<BaseHttpResult<E>> response) {
        if (response.code() == 200) {
            BaseHttpResult<E> body = response.body();
            if (body != null && body.getCode() == 10000) {
                //登录过期处理代码
                return;
            }
        }
        super.onResponse(call, response);
    }
}
