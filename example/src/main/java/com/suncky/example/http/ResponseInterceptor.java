package com.suncky.example.http;

import android.text.TextUtils;

import com.suncky.example.MyAccountManager;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class ResponseInterceptor implements Interceptor {
    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        String token = response.header("token");
        if (!TextUtils.isEmpty(token)) {
            MyAccountManager.getInstance().saveToken(token);
        }
        return response;
    }
}
