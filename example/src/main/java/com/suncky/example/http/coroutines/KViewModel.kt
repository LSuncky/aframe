package com.suncky.example.http.coroutines

import com.suncky.frame.base.mvvm.BaseViewModel
import kotlinx.coroutines.cancel

open class KViewModel:BaseViewModel() {
    protected fun <E> getRequest():KHttpRequest<E> {
         return KHttpRequest.get()
    }

    override fun onCleared() {
        super.onCleared()
        getRequest<Any>().cancel()
    }
}