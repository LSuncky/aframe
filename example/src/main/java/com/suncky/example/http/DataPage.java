package com.suncky.example.http;

import com.suncky.frame.http.bean.Page;

/**
 * @author gl
 * @time 2021/4/15 18:32
 */
public class DataPage implements Page {
    /**
     * currentPage : 0
     * currentResult : 0
     * showCount : 0
     * totalPage : 0
     * totalResult : 0
     */

    private int currentPage;
    private int currentResult;
    private int showCount;
    private int totalPage;
    private int totalResult;

    public DataPage() {
    }

    public DataPage(int currentPage, int currentResult, int showCount, int totalPage, int totalResult) {
        this.currentPage = currentPage;
        this.currentResult = currentResult;
        this.showCount = showCount;
        this.totalPage = totalPage;
        this.totalResult = totalResult;
    }

    @Override
    public int currentPage() {
        return currentPage;
    }

    @Override
    public int totalPage() {
        return totalPage;
    }

    @Override
    public int currentResult() {
        return currentResult;
    }

    @Override
    public int totalResult() {
        return totalResult;
    }

    @Override
    public int pageSize() {
        return showCount;
    }
}
