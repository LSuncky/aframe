package com.suncky.example.http;

import com.suncky.frame.http.bean.Result;

public class BaseHttpResult<E> implements Result<E,DataPage> {

    private int code;
    private String message;
    private DataPage page;//分页信息
    private E data;// 数据

    public BaseHttpResult() {
    }

    public BaseHttpResult(int code, String message, DataPage page, E data) {
        this.code = code;
        this.message = message;
        this.page = page;
        this.data = data;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public E getData() {
        return data;
    }

    @Override
    public DataPage getPage() {
        return page;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return code == 200;
    }
}
