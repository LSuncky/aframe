package com.suncky.example.http.request

import com.suncky.example.bean.User
import com.suncky.example.http.BaseHttpResult
import com.suncky.example.http.coroutines.KHttpResult
import com.suncky.example.http.param.LoginParam1
import retrofit2.http.Body
import retrofit2.http.POST

/**
 *
 * @author gl
 * @time 2021/12/9 17:31
 */
interface UserRequest1 {

    @POST("api/login")
    suspend fun login(@Body param: LoginParam1):KHttpResult<User>

    @POST("api/login")
    suspend fun login1(@Body param: LoginParam1):KHttpResult<List<User>>
}