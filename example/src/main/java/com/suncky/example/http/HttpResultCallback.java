package com.suncky.example.http;

import androidx.annotation.NonNull;

import com.suncky.frame.http.ResultCallback;

import retrofit2.Call;
import retrofit2.Response;

/**
 * @author gl
 * @time 2021/5/27 16:02
 */
public abstract class HttpResultCallback<E> extends ResultCallback<E,DataPage,BaseHttpResult<E>> {
    @Override
    public void onResponse(@NonNull Call<BaseHttpResult<E>> call, Response<BaseHttpResult<E>> response) {
        if (response.code() == 200) {
            BaseHttpResult<E> body = response.body();
            if (body != null && body.getCode() == 10000) {
                //登录过期处理代码
                return;
            }
        }
        super.onResponse(call, response);
    }
}
