package com.suncky.example.http;

import androidx.annotation.Nullable;

import com.suncky.frame.http.bean.ErrorResult;

public class HttpErrorResult implements ErrorResult {
    private String timestamp;
    private Integer status;
    private String error;
    private String message;
    private String path;
    @Nullable
    @Override
    public Integer getStatusCode() {
        return status;
    }

    @Nullable
    @Override
    public String getMessage() {
        return message;
    }
}
