package com.suncky.example.http.coroutines

import com.suncky.example.http.DataPage
import com.suncky.frame.base.mvvm.kt.KStateLiveData

class KHttpStateLiveData<E> :KStateLiveData<E,DataPage,KHttpResult<E>>()