package com.suncky.example.http;

import com.suncky.frame.http.file.UploadCallback;

import java.util.List;


/**
 * 文件上传回调
 * @author gl
 * @time 2021/4/20 16:54
 */
public class UploadHttpCallback extends UploadCallback<List<String>,DataPage, UploadHttpResult> {

    @Override
    public void onProgress(long progress, long total) {

    }

    @Override
    public void onSuccess(UploadHttpResult result) {

    }

    @Override
    public void onFailure(String message) {

    }
}
