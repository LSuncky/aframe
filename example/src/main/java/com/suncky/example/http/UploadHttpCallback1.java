package com.suncky.example.http;

import com.suncky.frame.http.file.UploadCallback;

import java.util.List;


/**
 * 文件上传回调
 * @author gl
 * @time 2021/4/20 16:54
 */
public class UploadHttpCallback1 extends UploadCallback<String,DataPage, UploadHttpResult1> {

    @Override
    public void onProgress(long progress, long total) {

    }

    @Override
    public void onSuccess(UploadHttpResult1 result) {

    }

    @Override
    public void onFailure(String message) {

    }
}
