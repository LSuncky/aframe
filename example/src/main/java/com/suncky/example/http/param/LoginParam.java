package com.suncky.example.http.param;

/**
 * author： gl
 * time： 2021/4/2 18:07
 * description：登录参数
 */
public class LoginParam {
    private String username;
    private String password;
    private String flag;
    private String registrationId;

    public LoginParam(String username, String password, String flag, String registrationId) {
        this.username = username;
        this.password = password;
        this.flag = flag;
        this.registrationId = registrationId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }
}
