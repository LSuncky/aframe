package com.suncky.example.bean;

/**
 * @author gl
 * @time 2021/4/19 17:05
 */
public class User {
    private String id;
    private String name;
    private int icon;
    private String phone;

    public User(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public User(String id, String name, int icon, String phone) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getIcon() {
        return icon;
    }

    public String getPhone() {
        return phone;
    }
}
