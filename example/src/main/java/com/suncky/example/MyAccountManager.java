package com.suncky.example;

import com.suncky.example.bean.User;
import com.suncky.example.http.DataPage;
import com.suncky.example.http.HttpCallback;
import com.suncky.example.http.param.LoginParam;
import com.suncky.example.http.request.UserRequest;
import com.suncky.frame.account.BaseAccountManager;
import com.suncky.frame.http.ApiCreator;

public class MyAccountManager extends BaseAccountManager<User> {

    private static MyAccountManager instance;

    private MyAccountManager() {
        super(User.class);
    }

    public synchronized static  MyAccountManager getInstance() {
        if (instance == null) {
            instance = new MyAccountManager();
        }
        return instance;
    }



    public void loginUsername(LoginParam param, HttpCallback<User> callback) {
        ApiCreator.createApi(UserRequest.class).loginUsername(param).enqueue(new HttpCallback<User>() {
            @Override
            public void onSuccess(User data, DataPage dataPage) {
//                if (!data.getNeddAuthntication()) {
                saveAccount(data);
//                }
                if (callback != null) {
                    callback.onSuccess(data, dataPage);
                }
            }

            @Override
            public void onError(int code, String message) {
                if (callback != null) {
                    callback.onError(code, message);
                }
            }
        });
    }

    public void loginVerifyCode(String code, HttpCallback<User> callback) {
        ApiCreator.createApi(UserRequest.class).loginVerifyCode(code).enqueue(new HttpCallback<User>() {
            @Override
            public void onSuccess(User data, DataPage page) {
                saveAccount(data);
                if (callback != null) {
                    callback.onSuccess(data, page);
                }
            }

            @Override
            public void onError(int code, String message) {
                if (callback != null) {
                    callback.onError(code, message);
                }
            }
        });

    }
}
