package com.suncky.example.activity.vm

import androidx.lifecycle.MutableLiveData
import com.blankj.utilcode.util.ToastUtils
import com.suncky.example.bean.User
import com.suncky.example.http.DataPage
import com.suncky.example.http.HttpStateLiveData
import com.suncky.example.http.HttpStateObserver
import com.suncky.example.http.coroutines.KHttpRequest
import com.suncky.example.http.coroutines.KHttpStateLiveData
import com.suncky.example.http.coroutines.KViewModel
import com.suncky.example.http.param.LoginParam1
import com.suncky.example.http.request.UserRequest1
import com.suncky.frame.http.ApiCreator
import kotlinx.coroutines.launch

class LoginViewModel:KViewModel() {
    private val userInfo = HttpStateLiveData<User>()
    private val userInfo1 = KHttpStateLiveData<User>()
    private val userInfo2 = MutableLiveData<User>()
    fun login() {
        getRequest<List<User>>().execute({ data: List<User>?, dataPage: DataPage? -> },
            { code: Int?, message: String? -> }) {
            ApiCreator.createApi(UserRequest1::class.java).login1(
                LoginParam1("", "")
            )
        }

        getRequest<User>().launch {
             val result= getRequest<User>().execute { ApiCreator.createApi(UserRequest1::class.java).login(
                LoginParam1("", "")) }
            userInfo1.value=result
        }

        getRequest<User>().execute({result ->
            userInfo1.value=result
        }){
            ApiCreator.createApi(UserRequest1::class.java).login(
                LoginParam1("", ""))
        }

        getRequest<User>().execute({ data: User?, dataPage: DataPage? ->
            userInfo2.value=data
        }, { code: Int?, message: String? ->

        }) {
            ApiCreator.createApi(UserRequest1::class.java).login(
                LoginParam1("", "")
            )
        }

    }

    fun stateObserverDemo() {
        //普通方式
        userInfo.observeState(null,object:HttpStateObserver<User>(){
            override fun onSuccess(data: User?, page: DataPage?) {

            }

            override fun onError(code: Int, message: String?) {

            }

            override fun onPreLoad() {
                super.onPreLoad()
            }

            override fun onLoading() {
                super.onLoading()
            }

            override fun onLoadFinish() {
                super.onLoadFinish()
            }
        })

        //kt DSL方式
        userInfo1.observeState(null){
            onSuccess={data, page ->

            }

            onError={code, message ->
                ToastUtils.showShort(message)
            }

            onPreLoad={}
            onLoading={}
            onLoadFinish={}
        }
    }
}