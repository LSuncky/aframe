package com.suncky.example.activity

import android.content.Context
import android.os.Bundle
import com.blankj.utilcode.util.ToastUtils
import com.suncky.example.R
import com.suncky.example.databinding.ActivityListViewBinding
import com.suncky.frame.base.BaseViewBindingActivity
import com.suncky.frame.base.adapter.ListAdapter
import com.suncky.frame.base.adapter.OptionListAdapter
import com.suncky.frame.base.adapter.Optional
import com.suncky.frame.base.adapter.holder.ViewHolder
import com.suncky.frame.base.adapter.listener.ListAdapterListener

class ListViewActivity : BaseViewBindingActivity<ActivityListViewBinding>() {

    private var adapter: StringListAdapter?=null
    private var optionAdapter: OptionStringListAdapter? = null

    override fun initView() {
        super.initView()
        adapter = StringListAdapter(context)
        binding.listView.adapter = adapter
        optionAdapter= OptionStringListAdapter(context)
        optionAdapter?.setOptionType(Optional.TYPE_MULTI)

        adapter?.setListener(object : ListAdapterListener {
            override fun onClickEvent(position: Int, viewId: Int) {
                if (viewId == R.id.text) {
                    //点击事件处理
                }
            }

            override fun onItemClickEvent(position: Int, subPosition: Int, viewId: Int) {
                if (viewId == R.id.listView) {
                    //嵌套列表item点击事件处理
                }
            }

        })
    }


    class StringListAdapter(context: Context) : ListAdapter<String>(context,
            null, R.layout.item_nest_list_view) {

        override fun convert(holder: ViewHolder?, item: String?, position: Int) {
            holder?.setText(R.id.text, item)//显示内容
                    ?.setClickListener(R.id.text)//设置控件点击事件
                    ?.setItemClickListener(R.id.listView)//设置嵌套列表item点击事件
        }

    }

    class OptionStringListAdapter(context: Context) : OptionListAdapter<String>(context,
            null, R.layout.item_optional_list) {

        override fun convert(holder: ViewHolder?, item: String?, position: Int) {
            holder?.setText(R.id.text, item)//显示内容
            when(isSelected(item)) {
                true -> holder?.setImageResource(R.id.select_img, R.drawable.checked)
                false -> holder?.setImageResource(R.id.select_img, R.drawable.check)
            }
        }

    }
}