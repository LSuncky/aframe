package com.suncky.example.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View
import com.suncky.example.R
import com.suncky.example.activity.vm.MvvmViewModel
import com.suncky.example.bean.User
import com.suncky.example.databinding.ActivityMvvmBinding
import com.suncky.example.http.BaseHttpResult
import com.suncky.example.http.DataPage
import com.suncky.example.http.HttpStateObserver

import com.suncky.frame.base.mvvm.BaseMvvmActivity;
import com.suncky.frame.base.mvvm.StateObserver
import com.suncky.frame.utils.ToastUtils

/**
 *
 * @author gl
 * @time 2021/12/9 17:21
 */
class MvvmActivity : BaseMvvmActivity<ActivityMvvmBinding, MvvmViewModel>() {

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, MvvmActivity::class.java)
            if (context !is Activity) {
                starter.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            context.startActivity(starter)
        }
    }

    override fun getLayoutResId(): Int {
        return R.layout.activity_mvvm
    }

    override fun initView() {
        super.initView()
        binding.activity = this
    }

    override fun initData() {
        super.initData()
        viewModel.user.observeState(this,object :HttpStateObserver<User>(){
            override fun onPreLoad() {
                showLoadingPop("正在登录")
            }

            override fun onLoadFinish() {
                dismissLoadingPop()
            }
            override fun onSuccess(data: User?, page: DataPage?) {
                ToastUtils.toast("${data?.name}已登录")
            }

            override fun onError(code: Int, message: String?) {

            }

        })
    }

    override fun onSingledClick(v: View?) {
        super.onSingledClick(v)
        when (v?.id) {
            binding.btnLogin.id -> viewModel.login()
        }
    }
}