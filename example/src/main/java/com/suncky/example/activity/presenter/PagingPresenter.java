package com.suncky.example.activity.presenter;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.suncky.example.activity.PagingActivity;
import com.suncky.example.adapter.StringListPagingAdapter;
import com.suncky.example.databinding.ItemStringListBinding;
import com.suncky.example.http.BaseHttpResult;
import com.suncky.example.http.DataPage;
import com.suncky.frame.base.adapter.RecyclerViewBindingPageAdapter;
import com.suncky.frame.base.mvp.Presenter;
import com.suncky.frame.paging.IntegerPageKeyedDataSource;
import com.suncky.frame.paging.PagingCallBack;
import com.suncky.frame.paging.PagingHelper;
import com.suncky.frame.paging.PagingLoadParams;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gl
 * @time 2021/4/15 17:24
 */
public class PagingPresenter extends Presenter<PagingActivity> {
    private RecyclerViewBindingPageAdapter<String, ItemStringListBinding> adapter;
    private PagingHelper<String, DataPage, BaseHttpResult<List<String>>> pagingHelper;

    public PagingPresenter(PagingActivity view) {
        super(view);
        init();
    }

    private void init() {
        pagingHelper = new PagingHelper<>(getView());
        pagingHelper.setDataSource(new IntegerPageKeyedDataSource<String, DataPage, BaseHttpResult<List<String>>>() {
            @Override
            public void load(PagingLoadParams loadParams, PagingCallBack<String, DataPage, BaseHttpResult<List<String>>> callBack) {
                getData(loadParams.getNextPage(),callBack);
            }
        });
        pagingHelper.setDataChangeListener(new PagingHelper.DataChangeListener<String>() {
            @Override
            public void onChanged(PagedList<String> pagedList) {
                adapter.submitList(pagedList);
            }
        });

        adapter = new StringListPagingAdapter(getView().getContext());
        getView().getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getView().getContext()));
        getView().getBinding().recyclerView.setAdapter(adapter);
        getView().getBinding().refreshLayout.setRefreshFooter(new ClassicsFooter(getView().getContext()));
        getView().getBinding().refreshLayout.setRefreshHeader(new ClassicsHeader(getView().getContext()));
        getView().getBinding().refreshLayout.setEnableLoadMore(false);
        getView().getBinding().refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                pagingHelper.refreshData();
            }
        });
    }

    public void getData(int page, PagingCallBack<String, DataPage, BaseHttpResult<List<String>>> callBack) {
        getView().getBinding().recyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                List<String> data = new ArrayList<>();
                for (int i = 0; i < 20; ++i) {
                    data.add(Integer.toString(i));
                }
                DataPage dataPage = new DataPage(page, 60, 20, 3, 60);
                callBack.onSuccess(data,dataPage);
                getView().getBinding().refreshLayout.finishRefresh();
                getView().getBinding().refreshLayout.finishLoadMore();
            }
        }, 1000);
    }
}