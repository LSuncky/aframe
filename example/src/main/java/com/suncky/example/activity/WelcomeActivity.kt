package com.suncky.example.activity

import android.Manifest
import android.content.Intent
import android.os.Bundle
import com.blankj.utilcode.util.BarUtils
import com.blankj.utilcode.util.ToastUtils
import com.suncky.example.R
import com.suncky.example.MyAccountManager
import com.suncky.example.bean.User
import com.suncky.example.databinding.ActivityWelcomeBinding
import com.suncky.example.http.DataPage
import com.suncky.example.http.HttpCallback
import com.suncky.example.http.param.LoginParam
import com.suncky.example.http.request.UserRequest
import com.suncky.frame.base.BaseViewBindingActivity
import com.suncky.frame.dialog.CustomAlertDialog
import com.suncky.frame.http.ApiCreator
import com.suncky.frame.utils.MD5Utils
import com.suncky.frame.utils.permission.PermissionUtil
import com.tbruyelle.rxpermissions3.RxPermissions
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable

class WelcomeActivity : BaseViewBindingActivity<ActivityWelcomeBinding>() {

    private var requested:Boolean=false
    private val permissionUtil by lazy { PermissionUtil(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        BarUtils.setNavBarVisibility(this,false)
    }

    override fun onResume() {
        super.onResume()
        if (!requested) {
//            requestPermissionsRx()
            requestPermissions()
            requested = true
        }
    }

    private fun requestPermissions() {
        permissionUtil.requestPermissions(1, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission
            .CAMERA)
        ) { requestCode, granted, denied ->
//            if (denied.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            if (denied.isNotEmpty()) {
                CustomAlertDialog.Builder(context).setCancelable(false).setMessage("需要相应权限才能使用")
                    .setPositiveButton("确定").setNegativeButton("取消").setTitle(null)
                    .setCallback { t, _, _ ->
                        when (t) {
                            CustomAlertDialog.RESULT_CONFIRM -> {
                                PermissionUtil.launchSetting(context)
                                requested = false
                            }
                            CustomAlertDialog.RESULT_CANCEL -> finish()
                        }
                    }.create().show()
            } else {
                val user = MyAccountManager.getInstance().account
                if (!MyAccountManager.getInstance().isLogin) {
//                  login()
                    startActivity(Intent(context, MainActivity::class.java))
                    finish()
                } else {
                    binding.ivIcon.postDelayed({
                        startActivity(Intent(context, MainActivity::class.java))
                        finish()
                    }, 1000)
                }
            }
        }
    }

    private fun requestPermissionsRx() {
        RxPermissions(this).let {
            it.request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission
                    .CAMERA).subscribe(object : Observer<Boolean> {
                override fun onSubscribe(d: Disposable?) {

                }

                override fun onNext(t: Boolean?) {

                }

                override fun onError(e: Throwable?) {

                }

                override fun onComplete() {
                    when (it.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        true -> {
                            val user = MyAccountManager.getInstance().account
                            if (!MyAccountManager.getInstance().isLogin) {
//                                login()
                                startActivity(Intent(context, MainActivity::class.java))
                                finish()
                            } else {
                                binding.ivIcon.postDelayed({
                                    startActivity(Intent(context, MainActivity::class.java))
                                    finish()
                                }, 1000)
                            }
                        }
                        false -> {
                            CustomAlertDialog.Builder(context).setCancelable(false).setMessage("需要存储权限才能使用").setPositiveButton("确定").setNegativeButton("取消").setTitle(null)
                                    .setCallback { t, _, _ ->
                                        when (t) {
                                            CustomAlertDialog.RESULT_CONFIRM -> {
                                                PermissionUtil.launchSetting(context)
                                                requested = false
                                            }
                                            CustomAlertDialog.RESULT_CANCEL -> finish()
                                        }
                                    }.create().show()
                        }
                    }
                }
            })
        }
    }

    private fun login():Unit{
        ApiCreator.createApi(UserRequest::class.java).loginUsername(LoginParam("admin", MD5Utils.md5("admin" + "zzszcg@123"), "zz", "1a0018970a73aefbd12"))
                .enqueue(object : HttpCallback<User>() {
                    override fun onSuccess(data: User?, page: DataPage?) {
                        binding.ivIcon.postDelayed({
                            startActivity(Intent(context, MainActivity::class.java))
                            finish()
                        }, 1000)
                    }

                    override fun onError(code: Int, message: String?) {
                        ToastUtils.showShort(message)
                    }

                })
    }

    override fun useDefaultTitle(): Boolean {
        return false
    }
}