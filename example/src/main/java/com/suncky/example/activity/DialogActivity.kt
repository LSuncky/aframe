package com.suncky.example.activity

import android.app.Dialog
import android.content.Context
import android.view.Gravity
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.blankj.utilcode.util.ScreenUtils
import com.blankj.utilcode.util.ToastUtils
import com.suncky.example.R
import com.suncky.example.databinding.ActivityDialogBinding
import com.suncky.example.databinding.DialogStringListBinding
import com.suncky.frame.AppConfig
import com.suncky.frame.base.BaseViewBindingActivity
import com.suncky.frame.base.adapter.RecyclerViewAdapter
import com.suncky.frame.base.adapter.holder.RecyclerViewHolder
import com.suncky.frame.base.dialog.BaseDialogWrapper
import com.suncky.frame.dialog.CustomAlertDialog
import com.suncky.frame.dialog.LoadingDialog

class DialogActivity : BaseViewBindingActivity<ActivityDialogBinding>() {

    private var dialog:StringListDialog?=null


    override fun buildLoadingDialog(title: String?): LoadingDialog {
        return super.buildLoadingDialog(title).cancelable(false)
    }

    override fun initData() {
        super.initData()
//        val data= listOf("111","222","333","444","5","6","7","8","9","10","11","12","13","14","15","16","17")
        val data= listOf("111","222","333")
        dialog = StringListDialog(context, data) { t, dialog, _ ->
            ToastUtils.showShort("你选择了$t")
            dialog.dismiss()
        }.apply {
            this.setMaxHeight(ScreenUtils.getScreenHeight() * 4 / 5)
            this.setMinHeight(ScreenUtils.getScreenHeight() / 3)
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            binding.btnDialog.id-> dialog?.show()
            binding.btnTitleLoadingDialog.id-> {
                showLoadingPop("加载中111")
                showLoadingPop("加载中222")
                showLoadingPop("加载中333")
                showLoadingPop("加载中444")
                showLoadingPop("加载中555")
                binding.root.postDelayed({
                    showLoadingPop("加载中666")
                    binding.root.postDelayed({
                        dismissLoadingPop()
                    },5000)
                },500)
            }
            binding.btnLoadingDialog.id-> {
                showLoadingPop()
                binding.root.postDelayed({
                    dismissLoadingPop()
                },5000)
            }
            binding.btnAlertDialog.id-> {
                CustomAlertDialog.Builder(context)
                    .setCancelable(false)
                    .setTitle("版本更新")
                    .setTitleGravity(Gravity.START)
                    .setMessageGravity(Gravity.START)
                    .setMessage("1. 浏览器登录https://android.googlesource.com/new-password，并用gmail帐号登录；\n" +
                            "2. 点击网页上的“允许访问”，得到类似\n" +
                            "1 machine android.googlesource.com login git-<userName>.gmail.com password <password>  2 machine android-review.googlesource.com login git-<userName>.gmail.com password <password>\n" +
                            "3. 把上面那段信息追加到~/.netrc文件结尾(请检查当前用户的权限, 如果不存在此文件则自己新建一个)；\n" +
                            "4. 下载地址的URI更改为https://android.googlesource.com/a/platform/manifest（中间加上了“/a”）。\n" +
                            "5. repo init -u https://android.googlesource.com/a/platform/manifest -b android-2.3.3_r1\n" +
                            "6. repo sync")
                    .setNegativeButton("知道了")
                    .setPositiveButton("立即下载")
                    .setBackgroundRes(R.color.pageBackgroundColor)
                    .setDivideLineColor(resources.getColor(R.color.colorAccent))
                    .setCallback { o, dialog, extras ->
                        if (o == CustomAlertDialog.RESULT_CONFIRM) {
                            ToastUtils.showShort("正在下载")
                        }
                    }.create().show()
            }
        }
    }

    class StringListDialog(context: Context, var data:List<String>, callback: Callback<String>) :
            BaseDialogWrapper<DialogStringListBinding, String>(context, R.layout.dialog_string_list, callback) {


        override fun onCreateBinding(contentView: View): DialogStringListBinding {
            return DialogStringListBinding.bind(contentView)
        }

        override fun initView() {
            super.initView()
            val adapter = StringListAdapter(context, data)
            getBinding().recyclerView.layoutManager=LinearLayoutManager(context)
            getBinding().recyclerView.adapter = adapter
            adapter.setOnItemClickListener { v, position ->
                callback?.onResult(data[position],this)
            }
        }

        override fun customizeDialog(dialog: Dialog?) {
            super.customizeDialog(dialog)
        }
    }

    class StringListAdapter(context: Context,data: List<String>) : RecyclerViewAdapter<String>(context,
            data, R.layout.item_string_list) {

        override fun convert(holder: RecyclerViewHolder?, item: String?, position: Int) {
            holder?.setText(R.id.text, item)//显示内容
        }
    }
}