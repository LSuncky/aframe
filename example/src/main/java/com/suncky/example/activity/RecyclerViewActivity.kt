package com.suncky.example.activity

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.suncky.example.R
import com.suncky.example.databinding.ActivityRecyclerViewBinding
import com.suncky.example.databinding.ItemStringListBinding
import com.suncky.frame.base.BaseViewBindingActivity
import com.suncky.frame.base.adapter.RecyclerViewAdapter
import com.suncky.frame.base.adapter.RecyclerViewBindingAdapter
import com.suncky.frame.base.adapter.holder.RecyclerViewBindingHolder
import com.suncky.frame.base.adapter.holder.RecyclerViewHolder
import com.suncky.frame.base.adapter.listener.RecyclerAdapterListener

class RecyclerViewActivity : BaseViewBindingActivity<ActivityRecyclerViewBinding>() {

    private var adapter: StringListAdapter? = null


    override fun initView() {
        super.initView()
        adapter = StringListAdapter(context)
//        binding.recyclerView.adapter = adapter
        binding.adapter=adapter
        binding.layoutManager = LinearLayoutManager(context)

        adapter?.setAdapterListener(object : RecyclerAdapterListener {

            override fun onClickEvent(position: Int, viewId: Int) {
                //处理控件点击事件
            }

            override fun onLongClickEvent(position: Int, viewId: Int) {
                //处理控件长按事件
            }

        })
    }

    class StringListAdapter(context: Context) : RecyclerViewAdapter<String>(context,
            arrayListOf("Jim","Broth","Joe","Mike","Jake"), R.layout.item_string_list) {

        override fun convert(holder: RecyclerViewHolder?, item: String?, position: Int) {
            holder?.setText(R.id.text, item)//显示内容
                    ?.setClickListener(R.id.text)//设置控件点击事件
        }
    }

    class DataBindingStringListAdapter(context: Context) : RecyclerViewBindingAdapter<String,ItemStringListBinding>(context) {

        override fun onCreateDataBinding(inflater: LayoutInflater,parent: ViewGroup?, viewType:Int): ItemStringListBinding {
            //创建ViewDataBinding对象
            return ItemStringListBinding.inflate(LayoutInflater.from(mContext), parent, false)
        }

        override fun convert(holder: RecyclerViewBindingHolder<ItemStringListBinding>?, item: String?, position: Int) {
            holder?.binding?.content=item//设置ViewDataBinding数据
            //或者通过holder的方法直接设置view的数据
            holder?.setText(holder.binding.text,item)
        }
    }
}