package com.suncky.example.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import com.suncky.example.R
import com.suncky.example.databinding.ActivityMainBinding
import com.suncky.frame.base.BaseDataBindingActivity

class MainActivity : BaseDataBindingActivity<ActivityMainBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun getLayoutResId(): Int {
        return R.layout.activity_main
    }

    override fun initView() {
        super.initView()
        defaultTitle.setTitle("AframeDemo")
        defaultTitle.leftImage.visibility = GONE
    }

    override fun initData() {
        super.initData()
        binding.context=this
    }

    override fun onSingledClick(v: View?) {
        super.onSingledClick(v)
        when (v?.id) {
            binding.tvMvpDatabinding.id -> startActivity(Intent(context, MVPDataBindingActivity::class.java))
            binding.tvMvvm.id -> MvvmActivity.start(context)
            binding.tvImagePicker.id -> startActivity(Intent(context, ImagePickerActivity::class.java))
            binding.tvStartForResult.id -> startActivity(Intent(context, ForResultActivity::class.java))
            binding.tvRefresh.id -> RefreshActivity.start(context)
            binding.tvPaging.id -> PagingActivity.start(context)
            binding.tvImageUpload.id -> ImageUploadActivity.start(context)
            binding.tvDialog.id -> startActivity(Intent(context, DialogActivity::class.java))
            binding.tvDownload.id -> startActivity(Intent(context, DownloadActivity::class.java))
            binding.tvListAdapter.id -> AdapterActivity.start(context)
            binding.tvRvAdapter.id -> startActivity(Intent(context, RecyclerViewActivity::class.java))
            binding.tvPagerFragment.id -> PagerFragmentActivity.start(context)
            binding.tvDropPop.id -> DropPopActivity.start(context)
        }
    }
}