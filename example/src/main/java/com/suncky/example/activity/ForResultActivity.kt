package com.suncky.example.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import com.blankj.utilcode.util.ToastUtils
import com.suncky.example.R
import com.suncky.example.databinding.ActivityForResultBinding
import com.suncky.frame.base.BaseViewBindingActivity
import com.suncky.frame.utils.LogUtils
import com.suncky.frame.utils.activityresult.ActivityResultHelper

class ForResultActivity : BaseViewBindingActivity<ActivityForResultBinding>() {
    private lateinit var activityResultHelper:ActivityResultHelper
    private lateinit var activityResultLauncher:ActivityResultLauncher<Intent>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun initView() {
        super.initView()
        defaultTitle.setTitle("startFrResult")
    }

    override fun initData() {
        super.initData()
        activityResultHelper = ActivityResultHelper(this)
        activityResultLauncher=registerForActivityResult(ActivityResultContracts.StartActivityForResult(),object :ActivityResultCallback<ActivityResult>{
            override fun onActivityResult(result: ActivityResult) {
                val str = result.data?.getStringExtra("result") + "---2"
                ToastUtils.showShort(str)
                LogUtils.d(str)
            }

        })
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            binding.tvJump.id -> {
//                activityResultHelper.startForResult(ResultActivity.buildIntent(context, "hello!"), 1) { _, _, data ->
//                    ToastUtils.showShort(data?.getStringExtra("result") + "---1")
//                }
                activityResultLauncher.launch(ResultActivity.buildIntent(context, "hello!"))
            }
        }
    }

}