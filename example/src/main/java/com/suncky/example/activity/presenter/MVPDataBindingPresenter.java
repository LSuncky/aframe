package com.suncky.example.activity.presenter;

import android.widget.Toast;

import com.suncky.example.activity.MVPDataBindingActivity;
import com.suncky.frame.base.mvp.Presenter;

/**
 * description： 方面公开防盗门
 * @author： gl
 * @time： 2021/4/1 15:29
 */
public class MVPDataBindingPresenter extends Presenter<MVPDataBindingActivity> {

    public void showMessage() {
//        updateView("点我干嘛", message -> Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show());
        updateView(view->Toast.makeText(view.getContext(), "点我干嘛", Toast.LENGTH_SHORT).show());
    }

}