package com.suncky.example.activity.vm

import com.suncky.example.bean.User
import com.suncky.example.http.BaseHttpResult
import com.suncky.example.http.DataPage
import com.suncky.example.http.HttpStateLiveData
import com.suncky.example.http.coroutines.KViewModel
import com.suncky.frame.utils.LogUtils
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 *
 * @author gl
 * @time 2021/12/9 17:28
 */
class MvvmViewModel : KViewModel() {
    val user = HttpStateLiveData<User>()

    fun login() {
        user.onPreLoad()
        getRequest<User>().launch {
            delay(3000)
            user.onLoadFinish()
            user.value= BaseHttpResult<User>(200,"",DataPage(),User("1","管理员"))
            LogUtils.i("登录完成")
        }
    }
}