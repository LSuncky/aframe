package com.suncky.example.activity

import android.view.View
import com.suncky.example.R
import com.suncky.example.databinding.ActivityMvpDatabindingBinding
import com.suncky.example.activity.presenter.MVPDataBindingPresenter
import com.suncky.frame.base.mvp.BaseMvpDataBindingActivity

class MVPDataBindingActivity : BaseMvpDataBindingActivity<ActivityMvpDatabindingBinding,
        MVPDataBindingPresenter>() {

    override fun getLayoutResId(): Int {
        return R.layout.activity_mvp_databinding
    }

    override fun initView() {
        super.initView()
        defaultTitle.setTitle("MVP+DataBinding")
        binding.text.text="您好,点我试试!"
    }

    override fun onSingledClick(v: View?) {
        super.onSingledClick(v)
        when(v?.id) {
            binding.text.id -> presenter.showMessage()//Toast.makeText(context, "点我干嘛", Toast.LENGTH_SHORT).show()
        }
    }
}