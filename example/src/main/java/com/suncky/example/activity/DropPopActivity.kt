package com.suncky.example.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.suncky.example.R
import com.suncky.example.databinding.ActivityDropPopBinding
import com.suncky.frame.base.BaseViewBindingActivity
import com.suncky.frame.widget.dropwindow.StringDropPopup

class DropPopActivity : BaseViewBindingActivity<ActivityDropPopBinding>() {
    companion object {

        fun start(context: Context) {
            val starter = Intent(context, DropPopActivity::class.java)
            if (context !is Activity) {
                starter.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            context.startActivity(starter)
        }
    }

    private val dropPopGrade by lazy {
        StringDropPopup(context).also {
            it.setOnItemSelectListener { position, item ->
                binding.tvGrade.text = item
            }
        }
    }
    private val dropPopClass by lazy {
        StringDropPopup(context).also {
            it.setOnItemSelectListener { position, item ->
                binding.tvClass.text = item
            }
        }
    }
    private val dropPopTeam by lazy {
        StringDropPopup(context).also {
            it.setOnItemSelectListener { position, item ->
                binding.tvTeam.text = item
            }
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        dropPopGrade.setItems(listOf("一年级", "二年级", "三年级", "四年级", "五年级", "六年级"))
        dropPopGrade.setBackgroundRes(R.drawable.rect_solid_white_corner_5)
        dropPopClass.setItems(
            listOf("舞蹈班", "美术班", "文学班", "数学班", "科学版", "计算机班", "体育班")
        )
        dropPopClass.setBackgroundRes(R.color.white)
        dropPopClass.setMinWidth(360)
        dropPopTeam.setItems(listOf("后勤组", "政务组", "医学护理组", "对外关系组", "饭组"))
        dropPopTeam.setItemLayoutId(R.layout.layout_drop_popup_item_1)
        dropPopTeam.width = 300
    }

    override fun onSingledClick(v: View?) {
        super.onSingledClick(v)
        when (v?.id) {
            binding.tvGrade.id -> {
                dropPopGrade.show(binding.tvGrade)
            }

            binding.tvClass.id -> {
                dropPopClass.show(binding.tvClass)
            }

            binding.tvTeam.id -> {
                dropPopTeam.show(binding.tvTeam)
            }
        }
    }
}