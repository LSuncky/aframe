package com.suncky.example.activity.presenter;

import com.suncky.example.activity.RefreshActivity;
import com.suncky.example.http.DataPage;
import com.suncky.frame.base.mvp.Presenter;
import com.suncky.frame.refresh.RefreshLoadMore;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gl
 * @time 2021/4/14 17:55
 */
public class RefreshPresenter extends Presenter<RefreshActivity> {

    public RefreshPresenter(RefreshActivity view) {
        super(view);
        init();
    }

    private void init() {

    }


    public void getData() {
        List<String> data = new ArrayList<>();
        for (int i = 0; i < 20; ++i) {
            data.add(Integer.toString(i));
        }
        updateView(data,(t,view) -> {
            int page = view.refreshLoadMore.nextPage();
            view.refreshLoadMore.finish();
            view.refreshLoadMore.onResult(t, new DataPage(page, page * 20, 20, 5, 100));
            if (view.refreshLoadMore.getRefreshType() == RefreshLoadMore.TYPE_REFRESH) {
                view.adapter.setData(t);
            } else {
                view.adapter.appendData(t);
            }
        });
    }
}