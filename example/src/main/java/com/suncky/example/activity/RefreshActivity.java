package com.suncky.example.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.suncky.example.R;
import com.suncky.example.activity.presenter.RefreshPresenter;
import com.suncky.example.databinding.ActivityRefreshBinding;
import com.suncky.example.http.DataPage;
import com.suncky.frame.base.adapter.RecyclerViewAdapter;
import com.suncky.frame.base.adapter.holder.RecyclerViewHolder;
import com.suncky.frame.base.mvp.BaseMvpViewBindingActivity;
import com.suncky.frame.refresh.RefreshLoadMoreListener;
import com.suncky.frame.refresh.SmartRefreshLoadMore;
import com.suncky.frame.refresh.SmartRefreshView;

/**
 * @author gl
 * @time 2021/4/14 17:54
 */
public class RefreshActivity extends BaseMvpViewBindingActivity<ActivityRefreshBinding, RefreshPresenter> {

    public static void start(Context context) {
        Intent starter = new Intent(context, RefreshActivity.class);
        if (!(context instanceof Activity)) {
            starter.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(starter);
    }

    public SmartRefreshLoadMore<String, DataPage> refreshLoadMore;
    public RecyclerViewAdapter<String> adapter;

    @Override
    protected void initView() {
        getDefaultTitle().setTitle("DataRefresh");
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        getBinding().refreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        getBinding().refreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        refreshLoadMore = new SmartRefreshLoadMore<>(new SmartRefreshView(getBinding().refreshLayout));
        refreshLoadMore.setRefreshLoadMoreListener(new RefreshLoadMoreListener() {
            @Override
            public void onRefresh() {
                getBinding().recyclerView.postDelayed(() -> getPresenter().getData(), 3000);

            }

            @Override
            public void onLoadMore() {
                getBinding().recyclerView.postDelayed(() -> getPresenter().getData(), 3000);
            }
        });
        adapter=new RecyclerViewAdapter<String>(getContext(),null,R.layout.item_number_list) {

            @Override
            public void convert(RecyclerViewHolder holder, String item, int position) {
                holder.setText(R.id.text, Integer.toString(position+1));
            }
        };
        getBinding().recyclerView.setAdapter(adapter);
    }

    @Override
    protected void initData() {
        refreshLoadMore.autoRefresh();
    }
}