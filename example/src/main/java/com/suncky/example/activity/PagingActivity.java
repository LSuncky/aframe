package com.suncky.example.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.suncky.example.R;
import com.suncky.example.activity.presenter.PagingPresenter;
import com.suncky.example.databinding.ActivityPagingBinding;
import com.suncky.frame.base.mvp.BaseMvpViewBindingActivity;

/**
 * @author gl
 * @time 2021/4/15 17:22
 */
public class PagingActivity extends BaseMvpViewBindingActivity<ActivityPagingBinding,
        PagingPresenter> {

    public static void start(Context context) {
        Intent starter = new Intent(context, PagingActivity.class);
        if (!(context instanceof Activity)) {
            starter.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(starter);
    }


    @Override
    protected void initView() {
        getDefaultTitle().setTitle("Paging");
    }

    @Override
    protected void initData() {

    }
}