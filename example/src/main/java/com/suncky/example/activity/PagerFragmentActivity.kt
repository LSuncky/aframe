package com.suncky.example.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.suncky.example.databinding.ActivityPagerFragmentBinding
import com.suncky.example.fragment.DataListFragment
import com.suncky.frame.base.BaseViewBindingActivity
import com.suncky.frame.base.adapter.CustomFragmentPagerAdapter

class PagerFragmentActivity : BaseViewBindingActivity<ActivityPagerFragmentBinding>() {
    companion object{

        fun start(context: Context) {
            val starter = Intent(context, PagerFragmentActivity::class.java)
            if (context !is Activity) {
                starter.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            context.startActivity(starter)
        }
    }

    private val pagerAdapter by lazy {
        CustomFragmentPagerAdapter(
            supportFragmentManager,
            listOf(
                DataListFragment(),
                DataListFragment(),
                DataListFragment(),
                DataListFragment(),
                DataListFragment()
            ),
            listOf(
                "用户组1",
                "用户组2",
                "用户组3",
                "用户组4",
                "用户组5"
            ).toTypedArray()
        )
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
    }

    override fun initData() {
        super.initData()
        setTitle("PagerFragment")
        binding.viewpager.offscreenPageLimit = 3
        binding.viewpager.adapter = pagerAdapter
        binding.tabLayout.setupWithViewPager(binding.viewpager)
    }
}