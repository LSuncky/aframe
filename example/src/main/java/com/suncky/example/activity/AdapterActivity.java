package com.suncky.example.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.blankj.utilcode.util.ToastUtils;
import com.google.android.material.tabs.TabLayout;
import com.suncky.example.R;
import com.suncky.example.activity.presenter.AdapterPresenter;
import com.suncky.example.adapter.StringListViewAdapter;
import com.suncky.example.adapter.StringRecyclerViewAdapter;
import com.suncky.example.databinding.ActivityAdapterBinding;
import com.suncky.frame.base.adapter.ConcatAdapterBuilder;
import com.suncky.frame.base.adapter.ExpandableListAdapter;
import com.suncky.frame.base.adapter.holder.ExpandableViewHolder;
import com.suncky.frame.base.adapter.holder.ViewHolder;
import com.suncky.frame.base.adapter.listener.OnChildClickCheckListener;
import com.suncky.frame.base.adapter.listener.OnGroupClickCheckListener;
import com.suncky.frame.base.adapter.listener.OnItemClickCheckListener;
import com.suncky.frame.base.adapter.listener.OnItemClickListener;
import com.suncky.frame.base.mvp.BaseMvpViewBindingActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author gl
 * @time 2021/4/22 10:13
 */
public class AdapterActivity extends BaseMvpViewBindingActivity<ActivityAdapterBinding, AdapterPresenter> {

    public static void start(Context context) {
        Intent starter = new Intent(context, AdapterActivity.class);
        if (!(context instanceof Activity)) {
            starter.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(starter);
    }



    private StringListViewAdapter listViewAdapter;
    private StringRecyclerViewAdapter recyclerViewAdapter;
    private ConcatAdapter concatAdapter;
    private ExpandableListAdapter<Integer, Integer> expandableListAdapter;

    @Override
    protected void initView() {
        setTitle("List Adapter");
        getBinding().recyclerView.setVisibility(View.GONE);
        getBinding().expandableView.setVisibility(View.GONE);
        listViewAdapter = new StringListViewAdapter(getContext());
        List<String> listViewData = new ArrayList<>();
        for (int i = 0; i < 20; ++i) {
            listViewData.add("ListView_item" + i);
        }
        listViewAdapter.setData(listViewData);
        getBinding().listView.setAdapter(listViewAdapter);
        getBinding().listView.setOnItemClickListener(new OnItemClickCheckListener() {
            @Override
            public void onItemClickChecked(AdapterView<?> parent, View view, int position, long id) {
                ToastUtils.showShort(listViewAdapter.getItem(position));
            }
        });


        recyclerViewAdapter = new StringRecyclerViewAdapter(getContext(),null);
        List<String> recyclerViewData = new ArrayList<>();
        for (int i = 0; i < 40; ++i) {
            recyclerViewData.add("RecyclerView_item" + i);
        }
        recyclerViewAdapter.setData(recyclerViewData);
        getBinding().recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        getBinding().recyclerView.setAdapter(recyclerViewAdapter);
        recyclerViewAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                ToastUtils.showShort(recyclerViewAdapter.getItem(position));
            }
        });


        List<String> concatSubData1 = new ArrayList<>();
        for (int i = 0; i < 10; ++i) {
            concatSubData1.add("ConcatSub1_item" + i);
        }
        List<String> concatSubData2 = new ArrayList<>();
        for (int i = 0; i < 10; ++i) {
            concatSubData2.add("ConcatSub2_item" + i);
        }
        List<String> concatSubData3 = new ArrayList<>();
        for (int i = 0; i < 10; ++i) {
            concatSubData3.add("ConcatSub3_item" + i);
        }
        StringRecyclerViewAdapter concatSubAdapter1 = new StringRecyclerViewAdapter(getContext(), concatSubData1);
        StringRecyclerViewAdapter concatSubAdapter2 = new StringRecyclerViewAdapter(getContext(), concatSubData2);
        StringRecyclerViewAdapter concatSubAdapter3 = new StringRecyclerViewAdapter(getContext(), concatSubData3);
        concatSubAdapter1.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                ToastUtils.showShort(concatSubAdapter1.getItem(position));
            }
        });
        concatSubAdapter2.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                ToastUtils.showShort(concatSubAdapter2.getItem(position));
            }
        });
        concatSubAdapter3.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                ToastUtils.showShort(concatSubAdapter3.getItem(position));
            }
        });
        concatAdapter = ConcatAdapterBuilder.defaultConfigAdapter(concatSubAdapter1, concatSubAdapter2, concatSubAdapter3);

        expandableListAdapter = new ExpandableListAdapter<Integer, Integer>(getContext(), Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), R.layout.item_string_list, R.layout.item_string_list) {

            @Override
            protected ExpandableViewHolder getGroupViewHolder(int groupPosition, View convertView, ViewGroup parent) {
                ExpandableViewHolder holder = super.getGroupViewHolder(groupPosition, convertView, parent);
                TextView textView = holder.getView(R.id.text);
                textView.setGravity(Gravity.START);
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) textView.getLayoutParams();
                lp.leftMargin = getContext().getResources().getDimensionPixelOffset(R.dimen.dp_15);
                return holder;
            }

            @Override
            protected ExpandableViewHolder getChildViewHolder(int groupPosition, int childPosition, View convertView, ViewGroup parent) {
                ExpandableViewHolder holder = super.getChildViewHolder(groupPosition, childPosition, convertView, parent);
                TextView textView = holder.getView(R.id.text);
                textView.setGravity(Gravity.START);
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) textView.getLayoutParams();
                lp.leftMargin = getContext().getResources().getDimensionPixelOffset(R.dimen.dp_30);
                return holder;
            }

            @Nullable
            @Override
            public List<Integer> getChildren(Integer group) {
                return Arrays.asList(1, 2, 3, 4, 5);
            }

            @Override
            public void convertGroup(ExpandableViewHolder holder, Integer groupItem, int groupPosition) {
                holder.setText(R.id.text, "Group item " + groupItem);
            }

            @Override
            public void convertChild(ExpandableViewHolder holder, Integer childItem, int groupPosition, int childPosition) {
                holder.setText(R.id.text, "Group item " + getGroup(groupPosition) + " Child item " + childItem);
            }
        };
        getBinding().expandableView.setAdapter(expandableListAdapter);
        getBinding().expandableView.setOnGroupClickListener(new OnGroupClickCheckListener() {
            @Override
            public boolean onGroupClickChecked(ExpandableListView parent, View v, int groupPosition, long id) {
                ToastUtils.showShort("Group item " + expandableListAdapter.getGroup(groupPosition));
                return false;
            }
        });
        getBinding().expandableView.setOnChildClickListener(new OnChildClickCheckListener() {
            @Override
            public boolean onChildClickChecked(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                ToastUtils.showShort("Child item " + expandableListAdapter.getChild(groupPosition, childPosition));
                return false;
            }
        });

        getBinding().tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        getBinding().listView.setVisibility(View.VISIBLE);
                        getBinding().recyclerView.setVisibility(View.GONE);
                        getBinding().expandableView.setVisibility(View.GONE);
                        break;
                    case 1:
                        getBinding().listView.setVisibility(View.GONE);
                        getBinding().recyclerView.setVisibility(View.VISIBLE);
                        getBinding().expandableView.setVisibility(View.GONE);
                        getBinding().recyclerView.setAdapter(recyclerViewAdapter);
                        break;
                    case 2:
                        getBinding().listView.setVisibility(View.GONE);
                        getBinding().recyclerView.setVisibility(View.VISIBLE);
                        getBinding().expandableView.setVisibility(View.GONE);
                        getBinding().recyclerView.setAdapter(concatAdapter);
                        break;
                    case 3:
                        getBinding().listView.setVisibility(View.GONE);
                        getBinding().recyclerView.setVisibility(View.GONE);
                        getBinding().expandableView.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int id = v.getId();
    }
}