package com.suncky.example.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.suncky.example.R
import com.suncky.example.databinding.ActivityResultBinding
import com.suncky.frame.base.BaseViewBindingActivity

class ResultActivity : BaseViewBindingActivity<ActivityResultBinding>() {

    companion object{
        fun buildIntent(context: Context, content: String): Intent {
            return Intent(context, ResultActivity::class.java).putExtra("content", content)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun initView() {
        super.initView()
        defaultTitle.setTitle("result")
        binding.tvResult.setOnClickListener(this)
        binding.tvResult.text = intent.getStringExtra("content")
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            binding.tvResult.id->{
                setResult(RESULT_OK, Intent().putExtra("result","hello!"))
                finish()
            }
        }
    }
}