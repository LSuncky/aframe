package com.suncky.example.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.view.View
import com.blankj.utilcode.util.ToastUtils
import com.suncky.example.R
import com.suncky.example.databinding.ActivityDownloadBinding
import com.suncky.frame.AppConfig
import com.suncky.frame.base.BaseViewBindingActivity
import com.suncky.frame.http.file.DownloadCallback
import com.suncky.frame.http.file.Downloader
import com.suncky.frame.utils.FileUtil
import java.io.File

class DownloadActivity : BaseViewBindingActivity<ActivityDownloadBinding>() {

    private val downloader by lazy {
        Downloader.Builder(context)
            .destDir(AppConfig.filePath)
            .url("https://down.360safe.com/360/inst_bd.exe")
            .downloadCallBack(object : DownloadCallback<File>() {
                var preProgress = 0

                @SuppressLint("SetTextI18n")
                override fun onProgress(progress: Long, total: Long) {
                    val pro = (progress * 100 / total).toInt()
                    if (pro - preProgress >= 1 || pro == 100) {
                        binding.tvProgress.text = "${pro}%"
                        preProgress = pro
                    }
                }

                override fun onSuccess(result: File?) {
                    preProgress = 0
                    ToastUtils.showShort("下载成功")
                    install(result)
                }

                override fun onFailure(message: String?) {
                    ToastUtils.showShort(message)
                }

                override fun onCanceled() {
                    super.onCanceled()
                    preProgress = 0
                    binding.tvProgress.text = "0%"
                    ToastUtils.showShort("下载已取消")
                }

            })
            .build()
    }

    override fun initView() {
        super.initView()
        setTitle("Downloader")
    }

    override fun initData() {
        super.initData()
    }

    override fun onSingledClick(v: View?) {
        super.onSingledClick(v)
        when (v?.id) {
            binding.btnDownload.id->{
                if (downloader.isDownloading) {
                    ToastUtils.showShort("正在下载中")
                    return
                }
                downloader.start()
//                if (downloader.isPaused) {
//                    downloader.resume()
////                    download(true)
//                }else{
//                    downloader.start()
////                    download(false)
//                }
            }
            binding.btnPause.id->{
                downloader.pause()
            }
            binding.btnCancel.id->{
                downloader.cancel()
            }
        }
    }

    override fun onDestroy() {
        downloader.cancel()
        super.onDestroy()
    }

    private fun download(append:Boolean) {
        downloader.download(append, object : DownloadCallback<File>() {
            var preProgress = 0

            @SuppressLint("SetTextI18n")
            override fun onProgress(progress: Long, total: Long) {
                val pro = (progress * 100 / total).toInt()
                if (pro - preProgress >= 1 || pro == 100) {
                    binding.tvProgress.text = "${pro}%"
                    preProgress = pro
                }
            }

            override fun onSuccess(result: File?) {
                ToastUtils.showShort("下载成功")
                install(result)
            }

            override fun onFailure(message: String?) {
                ToastUtils.showShort(message)
            }

            override fun onCanceled() {
                super.onCanceled()
                preProgress = 0
                binding.tvProgress.text = "0%"
                ToastUtils.showShort("下载已取消")
            }

        })
    }

    private fun install(file: File?) {
        if (file == null) {
            return
        }
        val intent = Intent(Intent.ACTION_VIEW)
        val contentUri = FileUtil.getUriForFile(context, file)
        intent.setDataAndType(contentUri, "application/vnd.android.package-archive")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        } else {
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }
        context.startActivity(intent)
    }
}