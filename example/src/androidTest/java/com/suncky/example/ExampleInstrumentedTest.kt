package com.suncky.example

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.suncky.frame.utils.CollectionUtils
import com.suncky.frame.utils.FileUtil
import com.suncky.frame.utils.GsonUtils
import com.suncky.frame.utils.LogUtils

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import java.util.Objects

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.suncky.example", appContext.packageName)
        LogUtils.i(CollectionUtils.joinToString(listOf(null,"lily",null,"mike","david","jake","tom"),null,2,5))
        LogUtils.i(FileUtil.getFilePostfix("photo.png",true))
        val data = "{\"name\":\"张三\",\"id\":1,\"no\":\"0001\",\"score\":235.03}"
        val user =User(1,"0002",145.02,"李四")
        val map:Map<String,String> = GsonUtils.jsonToMap(data)
        val map1 = GsonUtils.objectToMap<String>(user)
    }
}