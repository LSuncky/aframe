package com.suncky.example

data class User(
    val id:Int,
    val no:String,
    val scope:Double,
    val name:String
)
