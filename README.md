# AFrame

## Abstract 摘要
可作为项目框架引入，包含了最常用的轮子(包括Activity,Fragment,Adapter,Http框架,图片处理,权限工具,数据刷新/分页工具, 常用的Dialog,MVP/MVVM的实现,其他常用的工具类等)，旨在减少样板代码，提高开发效率

## Usage 使用方法
### Step 1
#### Gradle 配置
```
(1)先在项目根目录的 build.gradle 的 repositories 添加:

allprojects {
    repositories {
        ...
        maven { url "https://jitpack.io" }
    }
}

(2)在module的build.gradle中添加依赖：

implementation 'com.gitee.LSuncky:aframe:1.5.0'
```

### Step 2
在Application中调用初始化接口
```
override fun onCreate() {
    super.onCreate()
    AFrame.init(this, true)
    //初始化http配置(可选)，使用http工具，需要先初始化http配置，基于retrofit2 + okhttp3开发，如不使用此工具可不进行http初始化
    AFrame.initHttp(HttpConfigImpl.Builder()
            .baseUrl("http://192.168.1.36")//设置接口服务地址
            .connectOutTime(20)//连接超时时间(秒)，默认值20
            .readOutTime(20)//IO读取超时时间(秒)，默认值20
            .writeOutTime(20)//IO写入超时时间(秒)，默认值20
            .addInterceptor(ResponseInterceptor())//添加拦截器
            .addNetworkInterceptor(ParamsInterceptor())//添加网络拦截器
            .cache(cache)//配置数据缓存
            .cookieJar(CookieManger(this))//如需要，可配置cookie管理
            .addConverterFactory(factory)//添加retrofit数据转换器
            //https配置
            .sslSocketFactory(sslSocketFactory)
            .hostnameVerifier(hostnameVerifier)
            .logEnable(true)//log开关
            .setHttpLogger(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    LogUtils.d(message)
                }
            }, HttpLoggingInterceptor.BODY)//自定义log打印工具和日志级别，如不设置则使用okhttp默认的日志工具
            //使用默认配置(baseUrl必不可少),如果不想配置其他信息，可使用默认配置
            .defaultConfig("http://192.168.1.36"))
}
```


## License
The work done has been licensed under Apache License 2.0. The license file can be found
[here](LICENSE). You can find out more about the license at:

http://www.apache.org/licenses/LICENSE-2.0

