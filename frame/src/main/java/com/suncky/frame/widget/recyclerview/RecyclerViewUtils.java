package com.suncky.frame.widget.recyclerview;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewUtils {

    @BindingAdapter({"adapter"})
    public static void setAdapter(@NonNull RecyclerView rv, @NonNull RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter) {
        rv.setAdapter(adapter);
    }

    @BindingAdapter({"addItemDecoration"})
    public static void addItemDecoration(@NonNull RecyclerView rv, @NonNull RecyclerView.ItemDecoration decor) {
        rv.addItemDecoration(decor);
    }

    @BindingAdapter({"setNestedScrollingEnabled"})
    public static void setNestedScrollingEnabled(@NonNull RecyclerView rv, boolean enable) {
        rv.setNestedScrollingEnabled(enable);
    }

    @BindingAdapter({"setLayoutManager"})
    public static void setLayoutManager(@NonNull RecyclerView rv, @NonNull RecyclerView.LayoutManager layoutManager) {
        rv.setLayoutManager(layoutManager);
    }
}
