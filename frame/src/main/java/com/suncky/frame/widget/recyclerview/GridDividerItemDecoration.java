package com.suncky.frame.widget.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.suncky.frame.utils.LogUtils;

/**
 * 网格分割线
 * Created by HY_PC on 2019 2/12 0012 17:09.
 */
public class GridDividerItemDecoration extends RecyclerView.ItemDecoration {
    private static final String TAG = "GridDividerItem";
    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};

    public static final int HORIZONTAL = GridLayoutManager.HORIZONTAL;
    public static final int VERTICAL = GridLayoutManager.VERTICAL;

    private Drawable mDivider;

    private final Rect mBounds = new Rect();
    private int orientation;//排列方向
    private int width = 1;//划线宽度

    public GridDividerItemDecoration(Context context, int orientation) {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        if (mDivider == null) {
            Log.w(TAG, "@android:attr/listDivider was not set in the theme used for this "
                    + "GridDividerItemDecoration. Please set that attribute all call setDrawable()");
        }
        a.recycle();
        this.orientation = orientation;
        setUpWidth();
    }

    public GridDividerItemDecoration(Drawable divider, int orientation) {
        this.mDivider = divider;
        this.orientation = orientation;
        setUpWidth();
    }

    public GridDividerItemDecoration(int width, int color, int orientation) {
        this.mDivider = new ColorDrawable(color);
        this.width = width;
        this.orientation = orientation;
    }

    /**
     * Sets the {@link Drawable} for this divider.
     *
     * @param drawable Drawable that should be used as a divider.
     */
    public void setDrawable(@NonNull Drawable drawable) {
        if (drawable == null) {
            throw new IllegalArgumentException("Drawable cannot be null.");
        }
        mDivider = drawable;
        setUpWidth();
    }

    private void setUpWidth(){
        if (mDivider == null) {
            return;
        }
        if (orientation == HORIZONTAL) {
            width = mDivider.getIntrinsicWidth();
        } else if (orientation == VERTICAL) {
            width = mDivider.getIntrinsicHeight();
        }
    }

    @Override
    public void onDraw(@NonNull Canvas c, RecyclerView parent, @NonNull RecyclerView.State state) {
        if (parent.getLayoutManager() == null || mDivider == null) {
            return;
        }
        c.save();
        drawDivider(c, parent);
        c.restore();
    }

    private void drawDivider(Canvas canvas, RecyclerView parent){
        int orientation = ((GridLayoutManager)parent.getLayoutManager()).getOrientation();
        int spanCount = ((GridLayoutManager)parent.getLayoutManager()).getSpanCount();
        final int childCount = parent.getChildCount();
        int extraCount = childCount % spanCount;
        extraCount = extraCount == 0 ? spanCount : extraCount;
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            parent.getDecoratedBoundsWithMargins(child, mBounds);

            final int bottom = mBounds.bottom + Math.round(child.getTranslationY());
            final int top = bottom - width;
            int right = mBounds.right + Math.round(child.getTranslationX());
            int left = right - width/2;
            if (orientation == GridLayoutManager.VERTICAL) {
                if ((i + 1) % spanCount != 0) {
                    //画竖线
                    mDivider.setBounds(left, child.getTop(), right, bottom);
                    mDivider.draw(canvas);
                }

                if (i < childCount - extraCount) {
                    //画横线
                    mDivider.setBounds(child.getLeft(), top, right, bottom);
                    mDivider.draw(canvas);
                }
                if (i % spanCount != 0) {
                    left = mBounds.left + Math.round(child.getTranslationX());
                    right = left + width / 2;
                    mDivider.setBounds(left, child.getTop(), right, bottom);
                    mDivider.draw(canvas);
                }
            } else if (orientation == GridLayoutManager.HORIZONTAL) {
                if ((i + 1) % spanCount != 0) {
                    //画横线
                    mDivider.setBounds(child.getLeft(), top, right, bottom);
                    mDivider.draw(canvas);

                }
                if (i < childCount - extraCount) {
                    //画竖线
                    mDivider.setBounds(left, child.getTop(), right, bottom);
                    mDivider.draw(canvas);
                }
            }


        }
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent,
                               @NonNull RecyclerView.State state) {
        if (mDivider == null) {
            outRect.set(0, 0, 0, 0);
            return;
        }
        int spanCount = ((GridLayoutManager)parent.getLayoutManager()).getSpanCount();
        int position =  parent.getChildAdapterPosition(view);

        int childCount = parent.getAdapter().getItemCount();
        int extraCount = childCount % spanCount;
        extraCount = extraCount == 0 ? spanCount : extraCount;

        outRect.set(width/2, 0, (position + 1) % spanCount == 0 ? 0 : width/2, (position < childCount - extraCount) ? width : 0);
//        LogUtils.i("getItemOffsets","position=" + position + "  spanCount=" + spanCount + "  childCount=" + childCount + "  extraCount=" + extraCount);
    }


}
