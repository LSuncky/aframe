package com.suncky.frame.widget.dropwindow;

import android.content.Context;

import androidx.annotation.Nullable;

import java.util.List;

public class StringDropPopup extends DropPopup<String>{
    public StringDropPopup(Context context) {
        super(context);
    }

    public StringDropPopup(Context context, List<String> items) {
        super(context, items);
    }

    public StringDropPopup(Context context, List<String> items, int itemLayoutId) {
        super(context, items, itemLayoutId);
    }

    public StringDropPopup(Context context, int width, int height, List<String> items) {
        super(context, width, height, items);
    }

    public StringDropPopup(Context context, int width, int height, List<String> items, int itemLayoutId) {
        super(context, width, height, items, itemLayoutId);
    }

    @Nullable
    @Override
    public String getItemText(String item, int position) {
        return item;
    }
}
