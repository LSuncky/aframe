package com.suncky.frame.widget.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * a {@link RecyclerView.ItemDecoration} that can be used as a divider
 * between items of a {@link LinearLayoutManager}. It supports both {@link #HORIZONTAL} and
 * {@link #VERTICAL} orientations.
 *
 * <pre>
 *     mDividerItemDecoration = new LinearDividerItemDecoration(recyclerView.getContext(),
 *             mLayoutManager.getOrientation());
 *     recyclerView.addItemDecoration(mDividerItemDecoration);
 * </pre>
 */
public class LinearDividerItemDecoration extends RecyclerView.ItemDecoration {
    private static final String TAG = "LinearDividerItem";
    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};

    public static final int HORIZONTAL = LinearLayoutManager.HORIZONTAL;
    public static final int VERTICAL = LinearLayoutManager.VERTICAL;

    private Drawable mDivider;

    private final Rect mBounds = new Rect();

    private int width = 1;//划线宽度
    private int orientation = HORIZONTAL;//排列方向
    private int margin = 0;//边缘间隔


    public LinearDividerItemDecoration(Context context, int orientation) {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        if (mDivider == null) {
            Log.w(TAG, "@android:attr/listDivider was not set in the theme used for this "
                    + "GridDividerItemDecoration. Please set that attribute all call setDrawable()");
        }
        a.recycle();
        this.orientation = orientation;
        setUpWidth();
    }

    /**
     * Creates a divider {@link RecyclerView.ItemDecoration} that can be used with a
     * {@link LinearLayoutManager}.
     * @param width divider的宽度
     * @param color divider的颜色
     * @param orientation 分割线排列排列方式. Should be {@link #HORIZONTAL} or {@link #VERTICAL}.
     */
    public LinearDividerItemDecoration(int width, int color, int orientation) {
        mDivider = new ColorDrawable(color);
        this.width = width;
        this.orientation = orientation;
    }

    /**
     * Creates a divider {@link RecyclerView.ItemDecoration} that can be used with a
     * {@link LinearLayoutManager}.
     * @param drawable divider的Drawable实例
     * @param orientation 分割线排列排列方式. Should be {@link #HORIZONTAL} or {@link #VERTICAL}.
     */
    public LinearDividerItemDecoration(@NonNull Drawable drawable, int orientation) {
        mDivider = drawable;
        this.orientation = orientation;
        setUpWidth();
    }

    /**
     * Sets the {@link Drawable} for this divider.
     *
     * @param drawable Drawable that should be used as a divider.
     */
    public void setDrawable(@NonNull Drawable drawable) {
        mDivider = drawable;
        setUpWidth();
    }
    /**
     * Sets the orientation for this divider. This should be called if
     * {@link RecyclerView.LayoutManager} changes orientation.
     *
     * @param orientation {@link #HORIZONTAL} or {@link #VERTICAL}
     */
    public void setOrientation(int orientation) {
        if (orientation != HORIZONTAL && orientation != VERTICAL) {
            throw new IllegalArgumentException(
                    "Invalid orientation. It should be either HORIZONTAL or VERTICAL");
        }
        this.orientation = orientation;
        setUpWidth();
    }

    public void setMargin(int margin){
        this.margin = margin;
    }

    private void setUpWidth(){
        if (mDivider == null) {
            return;
        }
        if (orientation == HORIZONTAL) {
            width = mDivider.getIntrinsicWidth();
        } else if (orientation == VERTICAL) {
            width = mDivider.getIntrinsicHeight();
        }
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (parent.getLayoutManager() == null || mDivider == null) {
            return;
        }
        c.save();
        drawDivider(c, parent);
        c.restore();
    }

    private void drawDivider(Canvas canvas, RecyclerView parent){
        int orientation = ((LinearLayoutManager)parent.getLayoutManager()).getOrientation();
        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            parent.getDecoratedBoundsWithMargins(child, mBounds);

            final int bottom = mBounds.bottom + Math.round(child.getTranslationY());
            final int top = bottom - width;
            final int right = mBounds.right + Math.round(child.getTranslationX());
            final int left = right - width;
            if (orientation == LinearLayoutManager.VERTICAL) {
                if (i + 1 != childCount) {
                    //画横线
                    mDivider.setBounds(child.getLeft() + margin, top, right - margin, bottom);
                    mDivider.draw(canvas);
                }
            } else if (orientation == GridLayoutManager.HORIZONTAL) {
                if (i + 1 != childCount) {
                    //画竖线
                    mDivider.setBounds(left, child.getTop() + margin, right, bottom - margin);
                    mDivider.draw(canvas);
                }
            }
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        if (mDivider == null) {
            outRect.set(0, 0, 0, 0);
            return;
        }
        if (orientation == VERTICAL) {
            outRect.set(0, 0, 0, width);
        } else if (orientation == HORIZONTAL) {
            outRect.set(0, 0, width, 0);
        }
    }


}
