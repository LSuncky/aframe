package com.suncky.frame;

import android.content.Context;

import androidx.annotation.NonNull;

import com.suncky.frame.http.ApiCreator;
import com.suncky.frame.http.config.IOkHttpConfig;

public class AFrame {
    /**
     * 应用框架初始化(在Application中进行)
     * @param context
     * @param debug 是否为debug模式
     */
    public static void init(@NonNull Context context, boolean debug) {
        AppConfig.initInApp(context.getApplicationContext());
        AppConfig.debug = debug;
    }

    /**
     * 初始化http配置
     * @param config
     */
    public static void initHttp(IOkHttpConfig config) {
        ApiCreator.init(config);
    }
}
