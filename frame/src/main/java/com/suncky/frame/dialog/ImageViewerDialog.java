package com.suncky.frame.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.suncky.frame.R;
import com.suncky.frame.base.adapter.PagerAdapter;
import com.suncky.frame.base.adapter.holder.PagerViewHolder;
import com.suncky.frame.base.dialog.BaseDialogWrapper;
import com.suncky.frame.databinding.LayoutImageViewerDialogBinding;
import com.suncky.frame.utils.CollectionUtils;
import com.suncky.frame.widget.PinchImageView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * 图片查看器
 *
 * @param <T>
 */
public abstract class ImageViewerDialog<T> extends BaseDialogWrapper<LayoutImageViewerDialogBinding, T> {

    private List<T> imageData;
    private final PagerAdapter<T> adapter;
    private int position = 0;

    public ImageViewerDialog(Context context, List<T> imageData) {
        this(context, imageData, null);
    }

    public ImageViewerDialog(Context context, List<T> imageData, Callback<T> callback) {
        super(context, R.style.image_viewer_dialog_style, R.layout.layout_image_viewer_dialog, callback);
        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        setGravity(Gravity.CENTER);
        setWindowAnimationRes(R.style.fade_window_animation);
        setBackgroundResource(android.R.color.black);
        if (imageData != null) {
            this.imageData = imageData;
        } else {
            this.imageData = new ArrayList<>();
        }
        adapter = new ImageAdapter(context, imageData);
    }

    @Override
    public LayoutImageViewerDialogBinding onCreateBinding(@NotNull View contentView) {
        return LayoutImageViewerDialogBinding.bind(contentView);
    }

    @Override
    protected void initView() {
        super.initView();
        binding.closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close();
            }
        });
        binding.viewpager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                binding.descTxt.setText(onGetDesc(imageData.get(position), position));
                if (callback != null) {
                    callback.onResult(imageData.get(position),ImageViewerDialog.this, position);
                }
            }
        });
    }

    @Override
    protected void initData() {
        binding.viewpager.setAdapter(adapter);
        binding.descTxt.setText(onGetDesc(imageData.get(position), position));
    }

    @Override
    public void customizeDialog(Dialog dialog) {
        super.customizeDialog(dialog);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        //兼容刘海屏,是允许在刘海区域布局
        if (Build.VERSION.SDK_INT >= 28) {
            lp.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        dialog.getWindow().setAttributes(lp);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            dialog.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            dialog.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            dialog.getWindow().setStatusBarColor(Color.TRANSPARENT);
        }else {
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    @Nullable
    protected abstract String onGetDesc(T item, int position);

    protected abstract void onShowImage(T item, int position, ImageView imageView);

    public List<T> getImageData() {
        return imageData;
    }

    public void setImageData(List<T> imageData) {
        this.imageData = imageData;
        adapter.setData(imageData);
    }

    public void addImageData(List<T> imageData) {
        if (CollectionUtils.isEmpty(imageData)) {
            return;
        }
        if (this.imageData == null) {
            List<T> datas = new ArrayList<>(imageData);
            adapter.setData(datas);
            this.imageData = datas;
        } else {
            this.imageData.addAll(imageData);
            notifyImageDataChanged();
        }
    }

    public void addImageData(T image) {
        if (this.imageData == null) {
            List<T> datas = new ArrayList<>();
            datas.add(image);
            adapter.setData(datas);
            this.imageData = datas;
        } else {
            this.imageData.add(image);
            notifyImageDataChanged();
        }
    }

    public void removeImageData(int position) {
        if (this.imageData == null || position < 0 || position >= this.imageData.size()) {
            return;
        }
        this.imageData.remove(position);
        notifyImageDataChanged();
    }

    public void removeImageData(T image) {
        if (this.imageData == null) {
            return;
        }
        this.imageData.remove(image);
        notifyImageDataChanged();
    }

    public void notifyImageDataChanged() {
        adapter.notifyDataSetChanged();
    }

    public void open() {
        open(0);
    }

    public void open(int position) {
        show();
        this.position = position;
        binding.viewpager.setCurrentItem(position);
    }

    public void close() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void setDescTextSize(float size) {
        binding.descTxt.setTextSize(size);
    }

    public void setDescTextColor(int color) {
        binding.descTxt.setTextColor(color);
    }

    class ImageAdapter extends PagerAdapter<T> {
        public ImageAdapter(Context context, List<T> dataList) {
            super(context, dataList);
        }

        @Override
        protected View getItemView(int position) {
            PinchImageView imageView = new PinchImageView(context);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    close();
                }
            });
            imageView.setId(android.R.id.icon);
            //根布局为ImageView时，setTag会导致glide报错的问题
            FrameLayout warpLayout = new FrameLayout(mContext);
            warpLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            warpLayout.addView(imageView);
            return warpLayout;
        }

        @Override
        protected void convert(PagerViewHolder holder, T item, int position) {
            onShowImage(item, position, holder.getView(android.R.id.icon));
        }
    }

}
