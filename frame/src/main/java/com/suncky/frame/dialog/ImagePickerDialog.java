package com.suncky.frame.dialog;

import android.content.Context;
import android.view.View;

import com.suncky.frame.R;
import com.suncky.frame.base.dialog.BaseDialogWrapper;
import com.suncky.frame.databinding.LayoutImagePickerDialogBinding;

import org.jetbrains.annotations.NotNull;

/**
 * 图片选取
 */
public class ImagePickerDialog extends BaseDialogWrapper<LayoutImagePickerDialogBinding, Integer>
        implements View.OnClickListener {

    public static final int PICKER_TYPE_ALBUM = 1;//相册
    public static final int PICKER_TYPE_CAMERA = 2;//拍照

    public ImagePickerDialog(Context context) {
        super(context, R.style.custom_dialog_style, R.layout.layout_image_picker_dialog);
    }
    public ImagePickerDialog(Context context, Callback<Integer> callback) {
        super(context, R.style.custom_dialog_style, R.layout.layout_image_picker_dialog, callback);
    }

    @Override
    public LayoutImagePickerDialogBinding onCreateBinding(@NotNull View contentView) {
        return LayoutImagePickerDialogBinding.bind(contentView);
    }

    @Override
    protected void initView() {
        super.initView();
        binding.tvAlbum.setOnClickListener(this);
        binding.tvCamera.setOnClickListener(this);
        binding.tvCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        dismiss();
        if (id == binding.tvAlbum.getId()) {
            if (callback != null) {
                callback.onResult(PICKER_TYPE_ALBUM,this);
            }
        }else if (id == binding.tvCamera.getId()) {
            if (callback != null) {
                callback.onResult(PICKER_TYPE_CAMERA,this);
            }
        }
    }

    public void setTitle(String title) {
        binding.tvTitle.setText(title);
    }
}
