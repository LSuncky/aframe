package com.suncky.frame.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import com.suncky.frame.AppConfig;
import com.suncky.frame.R;
import com.suncky.frame.base.dialog.BaseDialogWrapper;
import com.suncky.frame.databinding.LayoutCustomAlertDialogBinding;

import org.jetbrains.annotations.NotNull;

/**
 * 自定义提示框
 * @author gl
 * @time 2021/4/13 17:07
 */
public class CustomAlertDialog extends BaseDialogWrapper<LayoutCustomAlertDialogBinding, Integer> implements View.OnClickListener {

    public static final int RESULT_CONFIRM = 1;
    public static final int RESULT_CANCEL = 0;

    private CustomAlertDialog(Context context) {
        this(context, null);
    }

    private CustomAlertDialog(Context context, Callback<Integer> callback) {
        super(context, R.style.custom_alert_dialog_style, R.layout.layout_custom_alert_dialog, callback);
    }

    @Override
    public LayoutCustomAlertDialogBinding onCreateBinding(@NotNull View contentView) {
        return LayoutCustomAlertDialogBinding.bind(contentView);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        dismiss();
        if (id == binding.positiveButton.getId()) {
            if (callback != null) {
                callback.onResult(RESULT_CONFIRM,this);
            }
        } else if (id == binding.negativeButton.getId()) {
            if (callback != null) {
                callback.onResult(RESULT_CANCEL,this);
            }
        }
    }

    public static class Builder {
        private int msgGravity = Gravity.CENTER;
        private int titleGravity = Gravity.CENTER;
        private boolean cancelable = true;
        private final Context context;
        private float titleTextSize=0;
        private int titleTextColor= Color.BLACK;
        private float messageTextSize = 0;
        private int messageTextColor = Color.parseColor("#404040");
        private float positiveTextSize=0;
        private int positiveTextColor=Color.parseColor("#1E7CD3");
        private float negativeTextSize=0;
        private int negativeTextColor=Color.parseColor("#8a8a8a");
        private int divideLineColor=Color.parseColor("#F0F0F0");
        private String title;
        private String message;
        private String positiveButtonText;
        private String negativeButtonText;
        private int width = 0;
        private int maxHeight = 0;
        private int backgroundRes = 0;
        private Callback<Integer> callback;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setTitleTextSize(int pixelSize) {
            this.titleTextSize = pixelSize;
            return this;
        }

        public Builder setTitleTextColor(int color) {
            this.titleTextColor = color;
            return this;
        }

        public Builder setMessageTextSize(int pixelSize) {
            this.messageTextSize = pixelSize;
            return this;
        }

        public Builder setMessageTextColor(int color) {
            this.messageTextColor = color;
            return this;
        }

        public Builder setPositiveTextSize(int pixelSize) {
            this.positiveTextSize = pixelSize;
            return this;
        }

        public Builder setPositiveTextColor(int color) {
            this.positiveTextColor = color;
            return this;
        }

        public Builder setNegativeTextSize(int pixelSize) {
            this.negativeTextSize = pixelSize;
            return this;
        }

        public Builder setNegativeTextColor(int color) {
            this.negativeTextColor = color;
            return this;
        }

        public Builder setMessageGravity(int gravity) {
            this.msgGravity = gravity;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        public Builder setTitleGravity(int gravity) {
            this.titleGravity = gravity;
            return this;
        }

        public Builder setTitle(int title) {
            this.title = (String) context.getText(title);
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setPositiveButton(int positiveButtonText) {
            this.positiveButtonText = (String) context
                    .getText(positiveButtonText);
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText) {
            this.positiveButtonText = positiveButtonText;
            return this;
        }

        public Builder setNegativeButton(int negativeButtonText) {
            this.negativeButtonText = (String) context
                    .getText(negativeButtonText);
            return this;
        }

        public Builder setNegativeButton(String negativeButtonText) {
            this.negativeButtonText = negativeButtonText;
            return this;
        }

        public Builder setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public Builder setWidth(int width) {
            this.width = width;
            return this;
        }

        public Builder setMaxHeight(int maxHeight) {
            this.maxHeight = maxHeight;
            return this;
        }

        public Builder setBackgroundRes(int backgroundRes) {
            this.backgroundRes = backgroundRes;
            return this;
        }

        public Builder setDivideLineColor(int color) {
            this.divideLineColor = color;
            return this;
        }

        public Builder setCallback(Callback<Integer> callback) {
            this.callback = callback;
            return this;
        }

        public CustomAlertDialog create() {
            CustomAlertDialog customAlertDialog= new CustomAlertDialog(context, callback) {

                @Override
                protected void initView() {
                    super.initView();
                    binding.message.setGravity(msgGravity);
                    binding.title.setGravity(titleGravity);
                    if (message != null) {
                        binding.message.setText(message);
                    } else {
                        binding.messageLayout.setVisibility(View.GONE);
                    }
                    if (title != null) {
                        binding.title.setText(title);
                    } else {
                        binding.title.setVisibility(View.GONE);
                    }
                    if (message == null && title == null) {
                        binding.line1.setVisibility(View.GONE);
                        binding.rootLayout.setPadding(0, 0, 0, 0);
                    }
                    if (negativeButtonText != null) {
                        binding.negativeButton.setText(negativeButtonText);
                    } else {
                        binding.negativeButton.setVisibility(View.GONE);
                        binding.line2.setVisibility(View.GONE);
                    }
                    if (positiveButtonText != null) {
                        binding.positiveButton.setText(positiveButtonText);
                    } else {
                        binding.positiveButton.setVisibility(View.GONE);
                        binding.line2.setVisibility(View.GONE);
                    }

                    if (titleTextSize > 0) {
                        binding.title.setTextSize(TypedValue.COMPLEX_UNIT_PX, titleTextSize);
                    }
                    binding.title.setTextColor(titleTextColor);

                    if (messageTextSize > 0) {
                        binding.message.setTextSize(TypedValue.COMPLEX_UNIT_PX, messageTextSize);
                    }
                    binding.message.setTextColor(messageTextColor);

                    if (positiveTextSize > 0) {
                        binding.positiveButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, positiveTextSize);
                    }
                    binding.positiveButton.setTextColor(positiveTextColor);

                    if (negativeTextSize > 0) {
                        binding.negativeButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, negativeTextSize);
                    }
                    if (backgroundRes != 0) {
                        binding.rootLayout.setBackgroundResource(backgroundRes);
                    }
                    binding.negativeButton.setTextColor(negativeTextColor);
                    binding.line1.setBackgroundColor(divideLineColor);
                    binding.line2.setBackgroundColor(divideLineColor);

                    binding.negativeButton.setOnClickListener(this);
                    binding.positiveButton.setOnClickListener(this);
                }
            };
            customAlertDialog.setCancelable(cancelable);
            customAlertDialog.setWidth(AppConfig.screenWidth * 4 / 5);
            customAlertDialog.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
            customAlertDialog.setMaxHeight(AppConfig.screenHeight * 3 / 5);
            customAlertDialog.setGravity(Gravity.CENTER);
            customAlertDialog.setWindowAnimationRes(R.style.fade_window_animation);
            return customAlertDialog;
        }
    }
}
