package com.suncky.frame.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;

import com.suncky.frame.R;
import com.suncky.frame.base.dialog.BaseDialogWrapper;
import com.suncky.frame.databinding.LayoutLoadingDialogBinding;

/**
 * 加载框
 */
public class LoadingDialog extends BaseDialogWrapper<LayoutLoadingDialogBinding,Object> {

    private LoadingDialog(Context context) {
        super(context, R.layout.layout_loading_dialog);
        //通过配置信息初始化样式，若调用了相关的设置方法，此处的初始化效果会被覆盖
        icon= ResourcesCompat.getDrawable(getContext().getResources(),R.drawable.loading_icon,null);
        progressImg=ResourcesCompat.getDrawable(getContext().getResources(),R.drawable.loading_progress,null);
        background = ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.loading_background, null);
        cancelable = getContext().getResources().getBoolean(R.bool.loading_cancelable);
        loadingAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);
    }

    @Override
    public LayoutLoadingDialogBinding onCreateBinding(@NonNull View contentView) {
        return LayoutLoadingDialogBinding.bind(contentView);
    }

    public static LoadingDialog with(Context context){
        return new LoadingDialog(context);
    }

    private Drawable progressImg;
    private Drawable icon;
    private String message;
    private boolean cancelable;
    private Animation loadingAnimation;
    private Drawable background;

    @Override
    protected void initView() {
        super.initView();
        if (icon == null) {
            getBinding().ivLoadingIcon.setVisibility(View.GONE);
        } else {
            getBinding().ivLoadingIcon.setVisibility(View.VISIBLE);
            getBinding().ivLoadingIcon.setImageDrawable(icon);
        }
        if (progressImg != null) {
            getBinding().ivLoadingProgress.setImageDrawable(progressImg);
            getBinding().ivLoadingProgress.setVisibility(View.VISIBLE);
        } else {
            getBinding().ivLoadingProgress.setVisibility(View.GONE);
        }
        getBinding().getRoot().setBackground(background);
        if (TextUtils.isEmpty(message)) {
            getBinding().tvLoadingMessage.setVisibility(View.GONE);
        } else {
            getBinding().tvLoadingMessage.setVisibility(View.VISIBLE);
            getBinding().tvLoadingMessage.setText(message);
        }
    }

    @Override
    public void customizeDialog(Dialog dialog) {
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setWindowAnimations(R.style.fade_window_animation);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(cancelable);
    }

    @Override
    public void show() {
        super.show();
        if (progressImg != null) {
            getBinding().ivLoadingProgress.startAnimation(loadingAnimation);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (progressImg != null) {
            getBinding().ivLoadingProgress.clearAnimation();
        }
    }

    /**
     * 设置加载动画,不设置则采用默认动画
     * @param loadingAnimation
     * @return
     */
    public LoadingDialog loadingAnimation(Animation loadingAnimation) {
        this.loadingAnimation = loadingAnimation;
        return this;
    }

    /**
     * 设置背景,不设置则使用默认背景,设置null不显示背景
     * @param background
     * @return
     */
    public LoadingDialog background(Drawable background) {
        this.background = background;
        return this;
    }

    /**
     * 设置加载中图片,不设置则采用默认图片,设置null不显示
     * @param progressImg
     * @return
     */
    public LoadingDialog progressImg(Drawable progressImg) {
        this.progressImg = progressImg;
        return this;
    }

    /**
     * 设置icon图片，不设置则使用默认图片,设置null不显示
     * @param icon
     * @return
     */
    public LoadingDialog icon(Drawable icon) {
        this.icon = icon;
        return this;
    }

    /**
     * 设置提示信息
     * @param message
     * @return
     */
    public LoadingDialog message(String message) {
        this.message = message;
        return this;
    }

    /**
     * 是否可以取消显示
     * @param cancelable
     * @return
     */
    public LoadingDialog cancelable(boolean cancelable) {
        this.cancelable = cancelable;
        return this;
    }
}
