package com.suncky.frame.account;

public class MemoryAccountStorage<T> implements AccountStorage<T> {
    private T account;
    private String token;
    @Override
    public void saveAccount(T account) {
        this.account = account;
    }

    @Override
    public void removeAccount() {
        this.account = null;
    }

    @Override
    public T getAccount() {
        return account;
    }

    @Override
    public void saveToken(String token) {
        this.token = token;
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public void removeToken() {
        this.token = null;
    }
}
