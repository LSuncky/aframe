package com.suncky.frame.account;

public interface AccountStorage<T> {
    String ACCOUNT_PREFERENCE_NAME = "account_preference";
    String SP_KEY_ACCOUNT_DATA = "account_data";
    String SP_KEY_ACCOUNT_TOKEN = "account_token";
    void saveAccount(T account);

    void removeAccount();

    T getAccount();

    void saveToken(String token);

    String getToken();

    void removeToken();
}
