package com.suncky.frame.utils.imagecompressor;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Utils {
    /**
     * 读取照片exif信息中的旋转角度
     * @param path 照片路径
     * @return角度
     */
    public static int getAngle(String path) {
        int degree = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(path);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return degree;
    }

    /**
     * 读取照片exif信息中的旋转角度
     * @param is 照片InputStream
     * @return角度
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static int getAngle(InputStream is) {
        int degree = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(is);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return degree;
    }

    /**
     * 压缩图片尺寸
     * @param srcBitmap 原图片
     * @param toWidth 宽度
     * @param toHeight 高度
     * @param uniformScale 是否等比例压缩
     * @return
     */
    public static Bitmap scaleBitmap(Bitmap srcBitmap, int toWidth, int toHeight, boolean uniformScale) {
        if (toWidth <= 0  && toHeight <= 0) {
            return srcBitmap;
        }
        Bitmap scaledBitmap;
        if (uniformScale) {
            float widthRatio = (float) toWidth / (float) srcBitmap.getWidth();
            float heightRatio = (float) toHeight / (float) srcBitmap.getHeight();
            float scale;
            if (widthRatio <= 0) {
                scale = heightRatio;
            } else if (heightRatio <= 0) {
                scale = widthRatio;
            }else {
                scale = Math.min(heightRatio, widthRatio);
            }
            Matrix matrix = new Matrix();
            matrix.setScale(scale, scale);
            scaledBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
        } else {
            int dstWidth = srcBitmap.getWidth();
            int dstHeight = srcBitmap.getHeight();
            if (toWidth > 0f) {
                dstWidth = toWidth;
            }
            if (toHeight > 0f) {
                dstHeight = toHeight;
            }
            scaledBitmap = Bitmap.createScaledBitmap(srcBitmap, dstWidth, dstHeight, true);
        }
        if (scaledBitmap != null && scaledBitmap != srcBitmap) {
            srcBitmap.recycle();
        }
        return scaledBitmap;
    }

    /**
     * 旋转Bitmap
     * @param srcBitmap 原图片
     * @param rotateDegree 旋转角度
     * @return
     */
    public static Bitmap getRotateBitmap(Bitmap srcBitmap, float rotateDegree){
        if (rotateDegree == 0) {
            return srcBitmap;
        }
        Matrix matrix = new Matrix();
        matrix.postRotate(rotateDegree);
        Bitmap rotaBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, false);
        if (srcBitmap != rotaBitmap) {
            srcBitmap.recycle();
        }
        return rotaBitmap;
    }

    /**
     * 写文件
     *
     * @param is       原始文件流
     * @param destPath 目标文件目录
     */
    public static void writeFile(InputStream is, String destPath) throws Exception {
        File file = new File(destPath);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        } else if (!file.exists()) {
            file.createNewFile();
        }
        BufferedInputStream bis = new BufferedInputStream(is);
        BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(file, false));
        byte[] bt = new byte[8192];
        int count;
        while ((count = is.read(bt)) > 0) {
            os.write(bt, 0, count);
        }
        os.flush();
        os.close();
        bis.close();
    }

    public static File createFile(String path) {
        File file = new File(path);
        try {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            } else if (!file.exists()) {
                file.createNewFile();
            }
        } catch (Exception e) {

        }
        return file;
    }
}
