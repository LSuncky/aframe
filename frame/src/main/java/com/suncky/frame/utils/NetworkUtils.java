package com.suncky.frame.utils;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import androidx.annotation.RequiresPermission;

import com.suncky.frame.AppConfig;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class NetworkUtils {

    @RequiresPermission(ACCESS_NETWORK_STATE)
    public static boolean isConnected() {
        NetworkInfo info = getActiveNetworkInfo();
        return info != null && info.isConnected();
    }

    @RequiresPermission(ACCESS_NETWORK_STATE)
    private static NetworkInfo getActiveNetworkInfo() {
        ConnectivityManager cm =
                (ConnectivityManager) AppConfig.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) return null;
        return cm.getActiveNetworkInfo();
    }

    /**
     * 从http头文件信息Content-Disposition中获取文件名
     *
     * @param disposition Content-Disposition字符串
     * @param postfix     是否包含文件后缀
     * @return
     */
    public static String getContentDispositionFileName(String disposition, boolean postfix) {
        if (TextUtils.isEmpty(disposition)) {
            return null;
        }
        String fileName = null;
        String[] parts = disposition.split(";");
        for (String part : parts) {
            if (TextUtils.isEmpty(part)) {
                continue;
            }
            if (part.contains("filename=")) {
                fileName = part.replace("filename=", "");
                break;
            }
            if (part.contains("filename*=")) {
                fileName = part.replace("filename*=", "");
                break;
            }
        }
        if (fileName == null) {
            return null;
        }
        //去掉全部可能出现的其他字符
        fileName = fileName.replace("utf-8", "").replace("UTF-8", "")
                .replace("''", "").replace("\"", "").trim();
        try {
            //还原被编码的文件名
            fileName = URLDecoder.decode(fileName,"UTF-8");
        } catch (UnsupportedEncodingException ignored) {}
        if (!postfix) {
            int dotIndex = fileName.indexOf('.');
            if (dotIndex > 0) {
                fileName = fileName.substring(0, dotIndex);
            }
        }
        return fileName;
    }

    /**
     * 从url中获取文件名
     *
     * @param url     文件地址
     * @param postfix 是否包含文件后缀
     * @retur
     */
    public static String getUrlFileName(String url, boolean postfix) {
        if (TextUtils.isEmpty(url)) {
            return null;
        }
        String name = url.substring(url.lastIndexOf('/') + 1);
        if (name.contains("?")) {
            name = name.substring(0, name.indexOf('?'));
        }
        if (!postfix) {
            int dotIndex = name.indexOf('.');
            if (dotIndex > 0) {
                name = name.substring(0, dotIndex);
            }
        }
        return name;
    }
}
