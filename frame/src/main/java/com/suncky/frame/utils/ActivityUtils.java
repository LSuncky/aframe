package com.suncky.frame.utils;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.view.View;
import android.view.ViewParent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class ActivityUtils {

    @Nullable
    public static Activity findActivity(View view) {
        View targetView = view;
        while (true) {
            if (targetView == null) {
                return null;
            }
            Context context = targetView.getContext();
            if (context != null && context instanceof Activity) {
                return (Activity) context;
            }
            ViewParent viewParent = targetView.getParent();
            if (viewParent == null || !(viewParent instanceof View)) {
                return null;
            }
            targetView = (View) viewParent;
        }
    }

    @Nullable
    public static <T extends Activity> T findActivity(View view, Class<T> classz) {
        Activity activity = findActivity(view);
        if (activity == null || !classz.isInstance(activity)){
            return null;
        }
        return (T) activity;
    }

    public static boolean isActivityContext(Context context) {
        return context instanceof Activity;
    }

    public static boolean isActivityDestroyed(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return activity == null || activity.isFinishing() || activity.isDestroyed();
        } else {
            return activity == null || activity.isFinishing();
        }
    }

    /**
     * Return the activity by context.
     *
     * @param context The context.
     * @return the activity by context.
     */
    public static Activity getActivityByContext(Context context) {
        if (context instanceof Activity) return (Activity) context;
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        return null;
    }

    /**
     * Return the name of launcher activity.
     *
     * @param pkg The name of the package.
     * @return the name of launcher activity
     */
    public static String getLauncherActivity(@NonNull Context context, final String pkg) {
        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setPackage(pkg);
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> info = pm.queryIntentActivities(intent, 0);
        ResolveInfo next = info.iterator().next();
        if (next != null) {
            return next.activityInfo.name;
        }
        return "no launcher activity of " + pkg;
    }
}
