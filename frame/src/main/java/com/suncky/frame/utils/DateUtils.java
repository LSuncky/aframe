package com.suncky.frame.utils;

import android.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateUtils {

    public static String getDateStr(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH", Locale.getDefault());
        return sdf.format(date);
    }

    public static String getCurrDateTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return sdf.format(new Date());
    }

    public static String convertDateToString(Date dateTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINESE);
        return df.format(dateTime);
    }

    public static String convertDateToString(Date dateTime, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format, Locale.CHINESE);
        return df.format(dateTime);
    }

    public static String convertMillisecondsToString(long milliseconds, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format, Locale.CHINESE);
        return df.format(new Date(milliseconds));
    }

    public static Date convertStringToDate(String dateTime, String format) {
        try {
            return new SimpleDateFormat(format, Locale.getDefault()).parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public static String convertDateTimeFormat(String dateTime, String fromFormat, String toFormat) {
        Date date;
        try {
            date = new SimpleDateFormat(fromFormat, Locale.getDefault()).parse(dateTime);
            return new SimpleDateFormat(toFormat, Locale.getDefault()).format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 功能：判断字符串是否为日期格式
     *
     * @param strDate
     * @return
     */
    public static boolean isDate(String strDate) {
        Pattern pattern = Pattern
                .compile("^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1-2][0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$");
        Matcher m = pattern.matcher(strDate);
        if (m.matches()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 返回(H:M:S)格式的字符串
     *
     * @param millSeconds
     *            总时间 毫秒
     * @return %02d:%02d:%02d
     * @exception
     */
    public static String getHMS(long millSeconds) {
        long seconds=millSeconds/1000;
        long minute = seconds / 60;
        long hour = minute / 60;
        long second = seconds % 60;
        minute %= 60;
        if (hour > 0) {
            return String.format(Locale.SIMPLIFIED_CHINESE,"%02d:%02d:%02d", hour, minute, second);
        } else if (minute > 0) {
            return String.format(Locale.SIMPLIFIED_CHINESE,"%02d:%02d", minute, second);
        } else if (second >= 0) {
            return String.format(Locale.SIMPLIFIED_CHINESE,"%02d", second);
        }else {
            return "";
        }
    }

    /**
     * 返回这天小时分秒格式的字符串
     *
     * @param millSeconds
     *            总时间 毫秒
     * @return %02d天%02d小时%02d分%02d秒
     * @exception
     */
    public static String getChnDHMS(long millSeconds) {
        String tempString = "";
        long seconds = millSeconds / 1000;
        long minute = seconds / 60;
        long hour = minute / 60;
        long day = hour / 24;
        long second = seconds % 60;
        hour %= 24;
        minute %= 60;
        if (day > 0) {
            return String.format(Locale.SIMPLIFIED_CHINESE,"%02d天%02d小时%02d分%02d秒",day, hour, minute, second);
        } else if (hour > 0) {
            return String.format(Locale.SIMPLIFIED_CHINESE,"%02d小时%02d分%02d秒", hour, minute, second);
        } else if (minute > 0) {
            return String.format(Locale.SIMPLIFIED_CHINESE,"%02d分%02d秒", minute, second);
        } else if (second >= 0) {
            return String.format(Locale.SIMPLIFIED_CHINESE,"%02d秒", second);
        }else {
            return "";
        }
    }

    /**
     * 两个时间间隔的天数
     * @param date1 时间字符串
     * @param date2 时间字符串
     * @param format 时间字符串格式
     * @return
     */
    public static int daysBetween(String date1, String date2,String format) {
        if (TextUtils.isEmpty(date1) || TextUtils.isEmpty(date2) || TextUtils.isEmpty(format)) {
            return 0;
        }
        DateFormat df = new SimpleDateFormat(format);
        Date one=null;
        Date two=null;
        try {
            one = df.parse(date1);
            two = df.parse(date2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return daysBetween(one, two);
    }

    /**
     * 两个时间间隔的天数
     * @param date1
     * @param date2
     * @return
     */
    public static int daysBetween(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            return 0;
        }
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(date1);
        c2.setTime(date2);
        setTimeToMidnight(c1);
        setTimeToMidnight(c2);
        long millis1 = c1.getTimeInMillis();
        long millis2 = c2.getTimeInMillis();
        long intervalMs = Math.abs(millis1 - millis2);
        return millisecondsToDays(intervalMs);
    }

    /**
     * 毫秒换算成天数
     * @param intervalMs
     * @return
     */
    public static int millisecondsToDays(long intervalMs) {
        return (int) (intervalMs / (1000 * 86400));
    }

    /**
     * 设置成当天0点
     * @param calendar
     */
    public static void setTimeToMidnight(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
    }

    /**
     * 获取间隔年数
     * @param startDate
     * @param endDate
     * @return
     */
    public static int yearsBetween(Date date1, Date date2) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(date1);
        c2.setTime(date2);
        return Math.abs(c2.get(Calendar.YEAR) - c1.get(Calendar.YEAR));
    }

    /**
     * 获取datestr是周几
     * @param datestr
     * @param format
     * @return
     */
    public static String getChnWeekDay(String datestr,String format) {
        if (TextUtils.isEmpty(datestr) || TextUtils.isEmpty(format)) {
            return null;
        }
        DateFormat df = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = df.parse(datestr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return getChnWeekDay(date);
    }

    public static String getChnWeekDay(Date date) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return getChnWeekDay(c);
    }

    /**
     * 获取c是周几
     * @param c
     * @return
     */
    public static String getChnWeekDay(Calendar c) {
        String day = null;
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.SUNDAY:
                day="周日";
                break;
            case Calendar.MONDAY:
                day="周一";
                break;
            case Calendar.TUESDAY:
                day="周二";
                break;
            case Calendar.WEDNESDAY:
                day="周三";
                break;
            case Calendar.THURSDAY:
                day="周四";
                break;
            case Calendar.FRIDAY:
                day="周五";
                break;
            case Calendar.SATURDAY:
                day="周六";
                break;
        }
        return day;
    }
}
