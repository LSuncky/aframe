package com.suncky.frame.utils;

import android.icu.text.DecimalFormat;
import android.os.Build;
import android.text.TextUtils;

import java.math.RoundingMode;

public class NumberUtils {
    /**
     * 格式化小数
     * @param f 要格式化的原数字
     * @param digits 保留小数的位数
     * @param halfUp 是否四舍五入
     * @param zero 小数不足位,是否补0
     */
    public static String decimalsFormat(double f, int digits, boolean halfUp, boolean zero) {
        if (digits < 0 || digits > 16) {
            return Double.toString(f);
        }
        if (digits == 0) {
            if (halfUp) {
                return String.valueOf(Math.round(f));//四舍五入取整(相当于Math.floor(x+0.5);Math.round(1.4)=1,Math.round(1.6)=2,Math.round(-1.5)=-1,Math.round(-1.6)=-2)
            } else {
                return String.valueOf((long)Math.floor(f));//向下取整(Math.floor(1.4)=1.0,Math.floor(1.5)=1.0,Math.floor(-1.4)=-2.0,Math.floor(-1.6)=-2.0)
            }
        }
        String ln;
        String pattern;
        if (zero) {
            ln = "0000000000000000";
            pattern = "0." + ln.substring(0, digits);
        } else {
            ln = "################";
            pattern = "#." + ln.substring(0, digits);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            DecimalFormat df = new DecimalFormat(pattern);
            df.setRoundingMode(halfUp ? RoundingMode.HALF_UP.ordinal() : RoundingMode.DOWN.ordinal());
            return df.format(f);
        } else {
            java.text.DecimalFormat df = new java.text.DecimalFormat(pattern);
            df.setRoundingMode(halfUp ? RoundingMode.HALF_UP : RoundingMode.DOWN);
            return df.format(f);
        }
    }

    /**
     * 格式化小数
     * @param f 要格式化的原数字
     * @param digits 保留小数的位数
     * @param halfUp 是否四舍五入
     * @param zero 小数不足位,是否补0
     */
    public static String decimalsFormat(String f, int digits, boolean halfUp, boolean zero) {
        if (TextUtils.isEmpty(f)) {
            return f;
        }
        return decimalsFormat(Double.parseDouble(f), digits, halfUp, zero);
    }

    /**
     * 格式化小数
     * @param f 要格式化的原数字
     * @param digits 保留小数的位数
     * @param halfUp 是否四舍五入
     * @param zero 小数不足位,是否补0
     */
    public static String decimalsFormat(Double f, int digits, boolean halfUp, boolean zero) {
        if (f==null) {
            return null;
        }
        return decimalsFormat(f.doubleValue(), digits, halfUp, zero);
    }

    /**
     * 格式化小数,四舍五入,小数不足位不补0
     * @param f 要格式化的原数字
     * @param digits 保留小数的位数
     */
    public static String decimalsFormat(double f, int digits) {
        return decimalsFormat(f,digits,true,false);
    }

    /**
     * 格式化小数,四舍五入,小数不足位不补0
     * @param f 要格式化的原数字
     * @param digits 保留小数的位数
     */
    public static String decimalsFormat(Double f, int digits) {
        return decimalsFormat(f,digits,true,false);
    }

    /**
     * 格式化小数,四舍五入,小数不足位不补0
     * @param f 要格式化的原数字
     * @param digits 保留小数的位数
     */
    public static String decimalsFormat(String f, int digits) {
        return decimalsFormat(f, digits,true,false);
    }
}
