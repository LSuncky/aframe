package com.suncky.frame.utils;

import android.text.Html;
import android.widget.EditText;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

/**
 * Created by yxy on 2015/8/18.
 */
public class TextViewUtils {
    /**
     * 判断TextView 或者 EditText 是否为空  true 空
     */
    public static boolean checkTextIsEmpty(TextView view) {
            if (view != null  && !view.getText().toString().equals("")) {
                return false;
            } else {
                return true;
            }
    }

    /**
     * 设置EditText可不可编辑
     * @param editText
     * @param editAble
     */
    @BindingAdapter("editable")
    public static void setEditAble(EditText editText, boolean editAble){
        if (editText == null) {
            return;
        }
        editText.setFocusable(editAble);
        editText.setFocusableInTouchMode(editAble);
        editText.setEnabled(editAble);
    }

    /**
     * 加载html内容
     * @param textView
     * @param htmlText html内容
     */
    @BindingAdapter("htmlText")
    public static void setHtmlText(TextView textView, String htmlText) {
        if (htmlText == null)
            return;
        textView.setText(Html.fromHtml(htmlText).toString());
    }
}
