package com.suncky.frame.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * gson 相关工具
 */
public class GsonUtils {
    private static Gson gson;

    public static Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder().registerTypeAdapterFactory(TypeAdapters.newFactory(int.class, Integer.class, INTEGER))
                    .registerTypeAdapterFactory(TypeAdapters.newFactory(long.class, Long.class, LONG))
                    //解决json转Map<Key,Object>时，long和int默认为double类型的问题
                    .registerTypeAdapterFactory(ObjectTypeAdapter.FACTORY)
                    .create();
        }
        return gson;
    }


    /**
     * 对象转为json串，不含null
     * @param obj
     * @return
     */
    public static String objectToJson(Object obj) {
        return getGson().toJson(obj);
    }


    /**
     * json字符串转化为对象
     * @param cls 对象的类型class
     * @param json json数据
     * @return
     * @param <T>
     */
    public static <T> T jsonToObject(Class<T> cls, String json) {
        if (json == null) {
            return null;
        }
        return getGson().fromJson(json, cls);
    }

    /**
     * json字符串转化为对象
     * @param cls 对象的类型class
     * @param json JsonElement对象
     * @return
     * @param <T>
     */
    public static <T> T jsonToObject(Class<T> cls, JsonElement json) {
        if (json == null) {
            return null;
        }
        return getGson().fromJson(json, cls);
    }

    /**
     * json字符串转化为List
     * @param cls List元素类型的Class对象，只能是引用类型
     * @param json json字符串
     * @return
     */
    public static <T> List<T> jsonToList(Class<T> cls, String json) {
        return getGson().fromJson(json, TypeToken.getParameterized(List.class, cls).getType());
    }

    /**
     * JsonElement对象转化为List
     * @param cls List元素类型的Class对象，只能是引用类型
     * @param json JsonElement对象
     * @return
     */
    public static <T> List<T> jsonToList(Class<T> cls, JsonElement json) {
        return getGson().fromJson(json, TypeToken.getParameterized(List.class, cls).getType());
    }

    /**
     * json转化为数组
     * @param cls 数组类型的Class对象，可以是基本类型和引用类型
     * @param json json字符串
     * @return
     */
    public static <T> T[] jsonToArray(Class<T[]> cls, String json) {
        return getGson().fromJson(json,TypeToken.get(cls).getType());
    }

    /**
     * json转化为数组
     * @param cls 数组元素类型的Class对象，只能是引用类型
     * @param json json字符串
     * @return
     */
    public static <T> T[] jsonToObjectArray(Class<T> cls, String json) {
        return getGson().fromJson(json, TypeToken.getArray(cls).getType());
    }

    /**
     * json转为带有类型参数的对象
     * @param json json数据
     * @param cls 本体类型Class对象
     * @param pCls 参数类型Class对象
     * @return 本体类型实例
     * @param <T> 本体类型
     */
    public static <T> T jsonToParamTypeObject(String json, Class<T> cls, Class... pCls) {
        return getGson().fromJson(json, TypeToken.getParameterized(cls, pCls).getType());
    }


    /**
     * 对象转化为JsonObject实例
     * @param o
     * @return
     */
    public static JsonObject toJsonObject(Object o) {
        return getGson().toJsonTree(o).getAsJsonObject();
    }

    /**
     * List转化为JsonArray实例
     *
     * @param o
     * @return
     */
    public static JsonArray toJsonArray(List o) {
        return getGson().toJsonTree(o).getAsJsonArray();
    }

    /**
     * List转为json
     *
     * @param o
     * @return
     */
    public static String listToJson(List o) {
        return toJsonArray(o).toString();
    }

    /**
     * json转为map
     * @param json json数据
     * @return
     */
    public static <V> Map<String, V> jsonToMap(String json) {
        return getGson().fromJson(json, new TypeToken<Map<String, V>>() {
        }.getType());
    }

    /**
     * 对象转为map
     * @param o 要转换的对象
     * @return
     */
    public static <V> Map<String, V> objectToMap(Object o) {
        return jsonToMap(objectToJson(o));
    }


    //int类型的解析器
    private static final TypeAdapter<Number> INTEGER = new TypeAdapter<Number>() {
        @Override
        public Number read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            try {
                return in.nextInt();
            } catch (NumberFormatException e) {
                //这里解析int出错，那么捕获异常并且返回默认值，因为nextInt出错中断了方法，没有完成位移，所以调用nextString()方法完成位移。
                in.nextString();
                return -1;
            }
        }

        @Override
        public void write(JsonWriter out, Number value) throws IOException {
            out.value(value);
        }
    };
    //long类型的解析器
    private static final TypeAdapter<Number> LONG = new TypeAdapter<Number>() {
        @Override
        public Number read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            try {
                return in.nextLong();
            } catch (Exception e) {
                in.nextString();
            }
            return -1;
        }

        @Override
        public void write(JsonWriter out, Number value) throws IOException {
            out.value(value);

        }
    };

    //重写ObjectTypeAdapter类，修改NUMBER类型默认返回double的问题
    public static final class ObjectTypeAdapter extends TypeAdapter<Object> {
        public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
            @SuppressWarnings("unchecked")
            @Override
            public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
                if (type.getRawType() == Object.class || type.getRawType() == Map.class) {
                    return (TypeAdapter<T>) new ObjectTypeAdapter(gson);
                }
                return null;
            }
        };

        private final Gson gson;

        ObjectTypeAdapter(Gson gson) {
            this.gson = gson;
        }

        @Override
        public Object read(JsonReader in) throws IOException {
            JsonToken token = in.peek();
            switch (token) {
                case BEGIN_ARRAY:
                    List<Object> list = new ArrayList<Object>();
                    in.beginArray();
                    while (in.hasNext()) {
                        list.add(read(in));
                    }
                    in.endArray();
                    return list;
                case BEGIN_OBJECT:
                    Map<String, Object> map = new LinkedTreeMap<String, Object>();
                    in.beginObject();
                    while (in.hasNext()) {
                        map.put(in.nextName(), read(in));
                    }
                    in.endObject();
                    return map;
                case STRING:
                    return in.nextString();
                case NUMBER:
                    double dNum = in.nextDouble();
                    long lNum = (long) dNum;
                    if (lNum == dNum) {
                        return lNum;
                    }
                    return dNum;
                case BOOLEAN:
                    return in.nextBoolean();
                case NULL:
                    in.nextNull();
                    return null;
                default:
                    throw new IllegalStateException();
            }
        }

        @SuppressWarnings("unchecked")
        @Override
        public void write(JsonWriter out, Object value) throws IOException {
            if (value == null) {
                out.nullValue();
                return;
            }

            TypeAdapter<Object> typeAdapter = (TypeAdapter<Object>) gson.getAdapter(value.getClass());
            if (typeAdapter instanceof com.google.gson.internal.bind.ObjectTypeAdapter) {
                out.beginObject();
                out.endObject();
                return;
            }

            typeAdapter.write(out, value);
        }
    }
}
