package com.suncky.frame.utils.glide;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;

/**
 * Glide图片加载工具
 */
public class GlideUtils {

    /**
     * 加载图片url
     * @param view
     * @param url
     */
    @BindingAdapter(value = {"imageUrl", "radius", "isCircle", "placeHolder", "downSampleStrategy"}, requireAll = false)
    public static void load(ImageView view, String url, int radius, boolean isCircle, Drawable defRes, DownsampleStrategy downSampleStrategy) {
        if (view == null || view.getContext() == null || url == null) {
            return;
        }
        Glide.with(view.getContext()).load(url).apply(getRequestOptions(radius, isCircle, defRes, downSampleStrategy)).into(view);
    }

    /**
     * 加载drawable
     * @param view ImageView
     * @param resDrawable drawable
     * @param radius
     * @param isCircle
     * @param defRes
     */
    @BindingAdapter(value = {"imageDrawable", "radius", "isCircle", "placeHolder", "downSampleStrategy"}, requireAll = false)
    public static void load(ImageView view, Drawable resDrawable, int radius, boolean isCircle, Drawable defRes, DownsampleStrategy downSampleStrategy) {
        if (view == null || view.getContext() == null || resDrawable == null) {
            return;
        }
        Glide.with(view.getContext()).load(resDrawable).apply(getRequestOptions(radius,isCircle,defRes,downSampleStrategy)).into(view);
    }


    /**
     * 加载图片
     *
     * @param context
     * @param url
     * @param view
     * @param defRes
     */
    public static void load(Context context, String url, ImageView view, int defRes) {
        load(context, url, view, 0, false, defRes);
    }

    /**
     * 记载为原型图片
     *
     * @param context
     * @param url
     * @param view
     * @param defRes
     */
    public static void loadCircle(Context context, String url, ImageView view, int defRes) {
        load(context, url, view, 0, true, defRes);
    }

    /**
     * 加载为圆角图片
     *
     * @param context
     * @param url
     * @param view
     * @param radius
     * @param defRes
     */
    public static void loadRadius(Context context, String url, ImageView view, int radius, int defRes) {
        load(context, url, view, radius, false, defRes);
    }

    /**
     * 加载网络图片
     *
     * @param context
     * @param url
     * @param view
     * @param radius
     * @param isCircle
     * @param defRes
     */
    public static void load(Context context, String url, ImageView view, int radius, boolean isCircle, int defRes) {
        load(context, url, view, radius, isCircle, defRes,null,null);
    }

    /**
     * 加载网络图片
     *
     * @param context
     * @param url
     * @param view
     * @param radius
     * @param isCircle
     * @param defRes
     * @param downsampleStrategy
     */
    public static void load(Context context, String url, ImageView view, int radius, boolean isCircle, int defRes, DownsampleStrategy downsampleStrategy) {
        load(context, url, view, radius, isCircle, defRes,downsampleStrategy, null);
    }

    /**
     * 加载网络图片,设置监听
     *
     * @param context
     * @param url
     * @param view
     * @param radius
     * @param isCircle
     * @param defRes
     * @param requestListener
     */
    public static void load(Context context, String url, ImageView view, int radius, boolean isCircle, int defRes, RequestListener<Drawable> requestListener) {
        load(context, url, view, radius, isCircle, defRes,null, requestListener);
    }

    /**
     * 加载网络图片
     *
     * @param context 上下文
     * @param url 图片地址
     * @param view ImageView
     * @param radius 圆角半径(如isCircle为true,此参数被忽略)
     * @param isCircle true-加载为圆形图
     * @param defRes 占位图
     * @param downsampleStrategy 采样优化参数
     * @param requestListener 监听器
     */
    public static void load(Context context, String url, ImageView view, int radius, boolean isCircle, int defRes, DownsampleStrategy downsampleStrategy, RequestListener<Drawable> requestListener) {
        if (context == null || url == null || view == null) {
            return;
        }
        if (requestListener != null) {
            Glide.with(context).load(url).apply(getRequestOptions(radius, isCircle, defRes, downsampleStrategy)).listener(requestListener).into(view);
        } else {
            Glide.with(context).load(url).apply(getRequestOptions(radius, isCircle, defRes, downsampleStrategy)).into(view);
        }
    }

    /**
     * 加载uri
     * @param context
     * @param uri
     * @param view
     * @param defRes
     */
    public static void load(Context context, Uri uri, ImageView view, int defRes) {
        load(context, uri, view, 0, false, defRes);
    }

    /**
     * 加载uri
     * @param context
     * @param uri
     * @param view
     * @param radius
     * @param isCircle
     * @param defRes
     */
    public static void load(Context context, Uri uri, ImageView view, int radius, boolean isCircle, int defRes) {
        load(context, uri, view, radius, isCircle, defRes,null);
    }

    /**
     * 加载uri
     * @param context
     * @param uri
     * @param view
     * @param radius
     * @param isCircle
     * @param defRes
     * @param downsampleStrategy
     */
    public static void load(Context context, Uri uri, ImageView view, int radius, boolean isCircle, int defRes, DownsampleStrategy downsampleStrategy) {
        if (context == null || uri == null || view == null) {
            return;
        }
        Glide.with(context).load(uri).apply(getRequestOptions(radius, isCircle, defRes, downsampleStrategy)).into(view);
    }

    /**
     * 加载资源图片
     *
     * @param context
     * @param view
     * @param defRes
     */
    public static void load(Context context, int resId, ImageView view, int defRes) {
        load(context, resId, view, 0, false, defRes);
    }

    /**
     * 加载资源图片
     *
     * @param context
     * @param resId
     * @param view
     * @param radius
     * @param isCircle
     * @param defRes
     */
    public static void load(Context context, int resId, ImageView view, int radius, boolean isCircle, int defRes) {
        load(context, resId, view, radius, isCircle, defRes, null);
    }

    /**
     * 加载资源图片
     *
     * @param context
     * @param resId
     * @param view
     * @param radius
     * @param isCircle
     * @param defRes
     * @param downsampleStrategy
     */
    public static void load(Context context, int resId, ImageView view, int radius, boolean isCircle, int defRes, DownsampleStrategy downsampleStrategy) {
        if (context == null || resId == 0 || view == null) {
            return;
        }
        Glide.with(context).load(resId).apply(getRequestOptions(radius, isCircle, defRes, downsampleStrategy)).into(view);
    }

    /**
     * 加载本地gif
     * @param context
     * @param resId
     * @param view
     */
    public static void loadGif(Context context, int resId, ImageView view) {
        if (context == null) {
            return;
        }
        Glide.with(context).asGif().load(resId).into(view);
    }

    /**
     * 获取加载配置
     * @param radius 圆角半径(如isCircle为true,此参数被忽略)
     * @param isCircle true-加载为圆形图
     * @param defRes 占位图
     * @param downsampleStrategy 采样优化参数
     */
    public static RequestOptions getRequestOptions(int radius, boolean isCircle, int defRes, DownsampleStrategy downsampleStrategy) {
        RequestOptions options;
        if (isCircle) {
            options = RequestOptions.circleCropTransform();
        } else if (radius > 0) {
            options = RequestOptions.noTransformation().optionalTransform(new RoundedCorners(radius));
        } else {
            options = RequestOptions.noTransformation();
        }
        if (defRes != 0) {
            options = options.placeholder(defRes);
        }
        if (downsampleStrategy != null) {
            options = options.downsample(downsampleStrategy);
        }
        return options;
    }

    /**
     * 获取加载配置
     * @param radius 圆角半径(如isCircle为true,此参数被忽略)
     * @param isCircle true-加载为圆形图
     * @param defRes 占位图
     * @param downsampleStrategy 采样优化参数
     */
    public static RequestOptions getRequestOptions(int radius, boolean isCircle, Drawable defRes, DownsampleStrategy downsampleStrategy) {
        RequestOptions options;
        if (isCircle) {
            options = RequestOptions.circleCropTransform();
        } else if (radius > 0) {
            options = RequestOptions.noTransformation().optionalTransform(new RoundedCorners(radius));
        } else {
            options = RequestOptions.noTransformation();
        }
        options = options.placeholder(defRes);
        if (downsampleStrategy != null) {
            options = options.downsample(downsampleStrategy);
        }
        return options;
    }
}
