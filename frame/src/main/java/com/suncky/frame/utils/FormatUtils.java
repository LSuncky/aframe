/*
 * @description
 * CheckFormatUtils.java
 * classes:com.vcyber.commonutils.CheckFormatUtils
 * @author yym create at 2014-10-17����10:24:16
*/
package com.suncky.frame.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *格式验证工具类
 */
public class FormatUtils {

    /**
     * 判断手机号格式是否合理
     *
     * @param mobile 手机号
     * @return boolean
     */
    public static boolean isMobile(String mobile) {
        if (mobile == null) {
            return false;
        }
        boolean isValid = false;
//        String expression = "^(13[0-9]|14[0-9]|15[0-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9])[0-9]{8}$";
        String expression = "1[3456789]\\d{9}$";
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(mobile);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    /**
     * 判断固话格式(区号不必要)
     *
     * @param fixedPhone 固话
     * @return 是否匹配
     */
    public static boolean isFixedPhone(String fixedPhone) {
        if (fixedPhone == null) {
            return false;
        }
        String reg = "^(010|02[0-9]|0[3-9][1-9]\\d{1})?\\d{7,8}$";
        return Pattern.matches(reg, fixedPhone);
    }

    /**
     * 判断固话格式(区号必要)
     *
     * @param fixedPhone 固话
     * @return 是否匹配
     */
    public static boolean isFixedPhones(String fixedPhone) {
        if (fixedPhone == null) {
            return false;
        }
        String reg = "^(010|02[0-9]|0[3-9][1-9]\\d{1}){1}\\d{7,8}$";
        return Pattern.matches(reg, fixedPhone);
    }

    /**
     * 判断是否固话(区号必要)或者手机
     *
     * @param mobileOrFixedPhone
     * @return
     */
    public static boolean isMobileOrFixedPhones(String mobileOrFixedPhone) {
        return isMobile(mobileOrFixedPhone) || isFixedPhones(mobileOrFixedPhone);
    }

    /**
     * 判断是否固话(区号不必要)或者手机
     *
     * @param mobileOrFixedPhone
     * @return
     */
    public static boolean isMobileOrFixedPhone(String mobileOrFixedPhone) {
        return isMobile(mobileOrFixedPhone) || isFixedPhone(mobileOrFixedPhone);
    }

    /**
     * 判断邮箱格式是否合理
     *
     * @param email 邮箱地址
     * @return boolean
     * @author thinkpad create at 2014-10-17下午5:10:48
     */
    public static boolean isEmail(String email) {
        if (email == null) {
            return false;
        }
        Pattern p = Pattern.compile("^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\\.([a-zA-Z0-9_-])+)+$");
        Matcher m = p.matcher(email);
        return m.matches();
    }

    /**
     * 判断车牌号格式是否正确
     *
     * @param carNumber 车牌号
     * @return boolean
     * @author thinkpad create at 2014-10-17下午5:11:51
     */
    public static boolean isCarNumber(String carNumber) {
        if (carNumber == null) {
            return false;
        }
        String vehicleNoStyle = "[\u4e00-\u9fa5]{1}[A-Z_a-z]{1}[A-Z_a-z_0-9]{5}";
        Pattern pattern = Pattern.compile(vehicleNoStyle);
        Matcher matcher = pattern.matcher(carNumber);
        return matcher.matches();
    }

    /**
     * 判断用户名格式是否正确（汉字、字母或数字）
     *
     * @param nickname 用户名
     * @return boolean
     * @author thinkpad create at 2014-10-17下午5:11:55
     */
    public static boolean isNickName(String nickname) {
        if (nickname == null) {
            return false;
        }
        String vehicleNoStyle = "^[\u4e00-\u9fa5A-Za-z0-9]*$";
        Pattern pattern = Pattern.compile(vehicleNoStyle);
        Matcher matcher = pattern.matcher(nickname);
        return matcher.matches();
    }

    /**
     * 密码格式校验
     *
     * @param passWord         密码
     * @param minLen           最小长度
     * @param maxLen           最大长度
     * @return
     */
    public static boolean checkPassword(String passWord, int minLen, int maxLen) {
        return checkPassword(passWord, minLen, maxLen, false, false, false, false);
    }

    /**
     * 密码格式校验
     *
     * @param passWord         密码
     * @param minLen           最小长度
     * @param maxLen           最大长度
     * @param lowerCase        是否必须包含小写字母
     * @param upperCase        是否必须包含大写字母
     * @param digits           是否必须包含数字
     * @param specialCharacter 是否必须包含特殊字符
     * @return
     */
    public static boolean checkPassword(String passWord, int minLen, int maxLen, boolean lowerCase, boolean upperCase, boolean digits, boolean specialCharacter) {
        if (passWord == null) {
            return false;
        }
        if (minLen <= 0) {
            minLen = 1;
        }
        if (maxLen < minLen) {
            maxLen = minLen;
        }
        if (passWord.length() < minLen || passWord.length() > maxLen) {
            return false;
        }
        StringBuilder regexBuilder = new StringBuilder("^");//开始
        if (lowerCase) {
            regexBuilder.append("(?=.*[a-z])");//必须包含至少一个小写字母
        }
        if (upperCase) {
            regexBuilder.append("(?=.*[A-Z])");//必须包含至少一个大写字母
        }
        if (digits) {
            regexBuilder.append("(?=.*[0-9])");//必须包含至少一个数字
        }
        if (specialCharacter) {
            regexBuilder.append("(?=.*[~!@#$%^&*()_+{}|:\"<>?`=\\[\\]\\;',./-])");//必须包含至少一个特殊字符~!@#$%^&*()_+{}|:"<>?`=[]\;',./-
        }
        regexBuilder.append("(?=\\S+$)")//不能包含空格
                .append(".{").append(minLen).append(",").append(maxLen).append("}")//限制长度范围.{minLen,maxLen}
                .append("$");//结束
        //示例:^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%^&*()_+{}|:"<>?`=\[\]\;',./-])(?=\S+$).{6,18}$
        //^ 开始
        //(?=.*[0-9]) 必须包含至少一个数字
        //(?=.*[a-z]) 必须包含至少一个小写字母[^\u4e00-\u9fa5]
        //(?=.*[A-Z]) 必须包含至少一个大写字母
        //(?=.*[~!@#$%^&*()_+{}|:"<>?`=\[\]\;',./-]) 必须包含至少一个特殊字符~!@#$%^&*()_+{}|:"<>?`=[]\;',./-
        //(?=\S+$) 不能包含空格
        //.{6,18} 最少6个字符,最多18个字符
        //$ 结束
        Pattern pattern = Pattern.compile(regexBuilder.toString());
        Matcher matcher = pattern.matcher(passWord);
        return matcher.matches();
    }
}
