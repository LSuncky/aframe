package com.suncky.frame.utils;

import java.util.Locale;

/**
 * @author gl
 * @time 2022/3/22 10:34
 */
public class TimeUtils {
    /**
     * 时长格式转换-毫秒转换成"时:分:秒"(HH:mm:ss)格式,不显示占位0
     * @param time 时长(毫秒)
     * @return
     */
    public static String getFormatHMS(long time) {
        return getFormatHMS(time, false);
    }

    /**
     * 时长格式转换-毫秒转换成"时:分:秒"(HH:mm:ss)格式
     * @param time 时长(毫秒)
     * @param zero 是否显示占位0
     * @return
     */
    public static String getFormatHMS(long time,boolean zero) {
        time = time / 1000;
        int s = (int) (time % 60);
        int m = (int) (time / 60);
        int h = (int) (time / 3600);
        if (zero || h > 0) {
            return String.format(Locale.CHINESE, "%02d:%02d:%02d", h, m, s);
        } else if (m > 0) {
            return String.format(Locale.CHINESE, "%02d:%02d", m, s);
        } else if (s > 0) {
            return String.format(Locale.CHINESE, "%02d", s);
        } else return "0";

    }

    /**
     * 时长格式转换-毫秒，毫秒转换成天小时时分秒格式的字符串(x天x小时x分x秒)
     * @param time 时长(毫秒)
     * @return
     */
    public static String getFormatChDHMS(long time) {
        if (time <= 0) {
            return null;
        }
        String result = "";
        int seconds = (int) (time / 1000);
        int minute = seconds / 60;
        int hour = minute / 60;
        int day = hour / 24;
        int second = seconds % 60;
        hour %= 24;
        minute %= 60;
        if (day > 0) {
            result += day + "天";
        }
        if (hour > 0) {
            result += hour + "小时";
        }
        if (minute > 0) {
            result += minute + "分";
        }
        if (second > 0) {
            result += second + "秒";
        }
        return result;
    }
}
