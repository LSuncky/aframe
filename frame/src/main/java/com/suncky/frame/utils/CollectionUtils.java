package com.suncky.frame.utils;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CollectionUtils {

    /**
     * 数据集合是否为空
     * @param list
     * @return
     * @param <T>
     */
    public static <T> boolean isEmpty(Collection<T> list) {
        return list == null || list.isEmpty();
    }

    /**
     * map是否为空
     * @param map
     * @return
     * @param <K>
     * @param <V>
     */
    public static <K, V> boolean isEmpty(Map<K, V> map) {
        return map == null || isEmpty(map.keySet());
    }

    /**
     * 分隔符接字符串集合中的所有元素
     * @param iterable 字符串集合
     * @param c 分隔符
     * @return 拼接后的字符串
     */
    public static String joinToString(Iterable<String> iterable, CharSequence c) {
        return joinToString(iterable, c,0);
    }

    /**
     * 分隔符接字符串集合中的元素
     * @param iterable 字符串集合
     * @param c 分隔符
     * @param start   起始元素索引(从0开始)
     * @return 拼接后的字符串
     */
    public static String joinToString(Iterable<String> iterable, CharSequence c, int start) {
        return joinToString(iterable, c, start, -1);
    }

    /**
     * 分隔符接字符串集合中的元素
     * @param iterable 字符串集合
     * @param c 分隔符
     * @param start   起始元素索引(从0开始)
     * @param count      拼接元素的数量(小于0时不限制数量)
     * @return 拼接后的字符串
     */
    public static String joinToString(Iterable<String> iterable, CharSequence c, int start, int count) {
        return joinTo(new StringBuilder(), iterable, c, start, count).toString();
    }

    /**
     * 分隔符拼接集合中的所有元素
     *
     * @param appendable 拼接集合元素的Appendable实例
     * @param iterable 元素集合
     * @param c          分隔符号
     * @return 拼接集合元素的Appendable实例
     */
    public static <T> Appendable joinTo(@NonNull Appendable appendable, Iterable<T> iterable, CharSequence c) {
        return joinTo(appendable, iterable, c, 0);
    }

    /**
     * 分隔符拼接集合中的所有元素
     *
     * @param appendable 拼接集合元素的Appendable实例
     * @param iterable 元素集合
     * @param c          分隔符号
     * @param start      起始元素索引(从0开始)
     * @return 拼接集合元素的Appendable实例
     */
    public static <T> Appendable joinTo(@NonNull Appendable appendable, Iterable<T> iterable, CharSequence c, int start) {
        return joinTo(appendable, iterable, c, start, -1);
    }


    /**
     * 分隔符拼接集合中的元素
     *
     * @param appendable 组个集合元素的Appendable实例
     * @param iterable 元素集合
     * @param c          分隔符号
     * @param start      起始元素索引(从0开始)
     * @param count      拼接元素的数量(小于0时不限制数量)
     * @return 拼接集合元素的Appendable实例
     */

    public static <T> Appendable joinTo(@NonNull Appendable appendable, Iterable<T> iterable, CharSequence c, int start, int count) {
        if (iterable == null || !iterable.iterator().hasNext()) {
            return appendable;
        }
        if (start < 0) {
            start = 0;
        }
        try {
            int elementIndex = 0;//当前遍历的元素序号
            int joinCount = 0;//已组合的元素数量(不计空元素)
            for (T o : iterable) {
                if (elementIndex < start) {
                    ++elementIndex;
                    continue;
                }
                //count小于0时不限制组合元素的数量
                if (count >= 0 && elementIndex - start >= count) {
                    break;
                }
                if (o == null) {
                    ++elementIndex;
                    continue;
                }
                if (c != null && c.length() > 0 && joinCount > 0) {
                    appendable.append(c);
                }
                appendable.append(o.toString());
                ++elementIndex;
                ++joinCount;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return appendable;
    }

}
