package com.suncky.frame.function;

@FunctionalInterface
public interface Procedure {
    void execute();
}
