package com.suncky.frame.http;

import com.suncky.frame.http.config.IOkHttpConfig;

public class ApiCreator {

    public static void init(IOkHttpConfig config) {
        HttpWrapper.getInstance().init(config);
    }

    public static <T> T createApi(Class<T> clazz) {
        if (clazz == null) {
            return null;
        }
        return HttpWrapper.getInstance().getRetrofit().create(clazz);
    }

    public static <T> T createApiWithBaseUrl(String baseUrl, Class<T> clazz) {
        if (clazz == null) {
            return null;
        }
        return HttpWrapper.getInstance().getRetrofit(baseUrl).create(clazz);
    }
}
