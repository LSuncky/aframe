package com.suncky.frame.http.bean;

/**
 * 数据分页信息
 * @author gl
 * @time 2021/4/14 14:52
 */
public interface Page {
    /**
     * 当前页码
     * @return
     */
    int currentPage();

    /**
     * 总页数
     * @return
     */
    int totalPage();

    /**
     * 当前数据条数
     * @return
     */
    int currentResult();

    /**
     * 总数据条数
     * @return
     */
    int totalResult();

    /**
     * 每页数据量
     * @return
     */
    int pageSize();
}
