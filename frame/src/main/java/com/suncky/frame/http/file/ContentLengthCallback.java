package com.suncky.frame.http.file;

public abstract class ContentLengthCallback {
   public void onStart() {

   }

   /**
    * 获得下载文件大小
    * @param length 文件大小字节数
    */
   public abstract void onSuccess(long length);

   public abstract void onFailure(String message);
}
