package com.suncky.frame.http.config;

import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

import okhttp3.Cache;
import okhttp3.CookieJar;
import okhttp3.Interceptor;
import retrofit2.Converter;

public interface IOkHttpConfig {

    /**
     * @return interceptor for doing something on the request or response
     */
    List<Interceptor> getInterceptors();

    List<Interceptor> getNetworkInterceptors();

    /**
     * @return cache for OkHttp lib
     */
    Cache getCache();

    /**
     * @return protocol + domain,like "http://api.ffan.com"
     */
    String getBaseUrl();

    /**
     * @return response data converter , json,xml, etc.
     */
    List<Converter.Factory> getConverter();

    /**
     * @return ssl for https
     */
    SSLSocketFactory getSSlSocketFactory();

    /**
     * @return verifier for request
     */
    HostnameVerifier getHostnameVerifier();

    /**
     * @return cookie manager
     */
    CookieJar getCookieJar();

    /**
     *
     * @return  The number of seconds
     */
    int getConnectOutTime();

    /**
     *
     * @return  The number of seconds
     */
    int getReadOutTime();

    /**
     *
     * @return  The number of seconds
     */
    int getWriteOutTime();
}
