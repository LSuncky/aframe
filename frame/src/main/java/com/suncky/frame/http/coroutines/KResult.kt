package com.suncky.frame.http.coroutines

import com.suncky.frame.http.bean.Page
import com.suncky.frame.http.bean.Result
import kotlin.Exception

interface KResult<E,P:Page>:Result<E,P> {

    override fun getData(): E?
    override fun getCode(): Int
    override fun getPage(): P?
    override fun getMessage(): String?
    fun getException(): Exception?


    fun setCode(code:Int?)
    fun setPage(page: P?)
    fun setData(page: E?)
    fun setMessage(message: String?)
    fun setException(e: Exception?)
}