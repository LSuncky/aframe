package com.suncky.frame.http;

import com.suncky.frame.http.bean.Page;
import com.suncky.frame.http.bean.Result;

/**
 * okHttp callback wrapper
 */
public abstract class DataCallback<E,P extends Page,R extends Result<E,P>> extends ResultCallback<E,P,R> {

    @Override
    public void onResult(R result) {
        if (result.isSuccess()) {
            onSuccess(result.getData(), result.getPage());
        } else {
            onError(result.getCode(), result.getMessage());
        }
    }

    @Override
    public void onFailure(String message) {
        onError(-1,message);
    }

    public abstract void onSuccess(E data, P page);
    public abstract void onError(int code, String message);
}
