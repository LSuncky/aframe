package com.suncky.frame.http.bean;

public interface Result<E,P extends Page> {
    /**
     * 返回码
     * @return
     */
    int getCode();

    /**
     * 数据
     * @return
     */
    E getData();

    /**
     * 分页信息
     * @return
     */
    P getPage();

    /**
     * 描述信息
     * @return
     */
    String getMessage();

    /**
     *
     * @return
     */
    boolean isSuccess();
}
