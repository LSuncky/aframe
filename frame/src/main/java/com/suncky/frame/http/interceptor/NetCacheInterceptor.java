package com.suncky.frame.http.interceptor;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class NetCacheInterceptor implements Interceptor {

    private final int maxAge;//秒

    public NetCacheInterceptor(int maxAge) {
        this.maxAge = maxAge;
    }

    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());

        return response.newBuilder()
                .header("Cache-Control", "public, max-age=" + maxAge)
                .removeHeader("Pragma")
                .build();
    }
}
