package com.suncky.frame.http.bean

/**
 * 请求异常返回结果
 */
interface ErrorResult {
    /**
     * 异常状态吗
     */
    fun getStatusCode():Int?

    /**
     * 异常信息
     */
    fun getMessage():String?
}