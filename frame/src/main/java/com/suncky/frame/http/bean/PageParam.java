package com.suncky.frame.http.bean;

/**
 * 分页参数
 * @author gl
 * @time 2021/9/2 17:39
 */
public interface PageParam {
    /**设置页码**/
    void setPage(Integer page);
    /**设置行数**/
    void setRows(Integer rows);
    /**获取页码**/
    Integer getPage();
    /**获取行数**/
    Integer getRows();

}
