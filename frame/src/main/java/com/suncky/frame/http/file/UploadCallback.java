package com.suncky.frame.http.file;

import com.suncky.frame.http.bean.Page;
import com.suncky.frame.http.bean.Result;

public abstract class UploadCallback<E,P extends Page,T extends Result<E,P>> {


    public UploadCallback() {
    }


    public void onStart() {

    }

    /**
     * 取消上传任务
     * @param requestTag 已取消的上传请求的tag
     */
    public void onCancel(Object requestTag) {

    }

    public abstract void onProgress(long progress, long total);

    public abstract void onSuccess(T result);

    public abstract void onFailure(String message);

}
