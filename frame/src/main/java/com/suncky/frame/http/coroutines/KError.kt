package com.suncky.frame.http.coroutines

enum class KError(val code: Int, val message: String?){
    SUCCESS(200,"成功"),
    UNKNOWN_ERROR(0,"未知错误"),
    HTTP_STATUS_ERROR(1,"网络错误"),
    CONNECT_FAIL(2,"网络连接失败"),
    SOCKET_TIMEOUT(3,"网络连接超时"),
    UNKNOWN_HOST(4,"未知服务器"),
    JSON_ERROR(5,"数据解析失败"),
    SSL_HANDSHAKE(6,"证书验证异常")
}