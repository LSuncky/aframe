package com.suncky.frame.http;

import androidx.annotation.NonNull;

import com.suncky.frame.http.bean.Page;
import com.suncky.frame.http.bean.Result;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author gl
 * @time 2021/5/27 13:16
 */
public abstract class ResultCallback<E,P extends Page,R extends Result<E,P>> implements Callback<R> {
    @Override
    public void onResponse(@NonNull Call<R> call, Response<R> response) {
        int responseCode = response.code();
        if (responseCode == 200) {
            R body = response.body();
            if (body != null) {
                onResult(body);
            } else {
                onFailure("result is null");
            }
        } else {
            onFailure(response.message());
        }
    }

    @Override
    public void onFailure(@NonNull Call<R> call, Throwable t) {
        onFailure(t.getMessage());
    }

    public abstract void onResult(R result);
    public abstract void onFailure(String message);
}
