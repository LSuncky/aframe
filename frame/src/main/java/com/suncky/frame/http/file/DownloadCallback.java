package com.suncky.frame.http.file;

public abstract class DownloadCallback<T> {


    protected DownloadCallback() {
    }

    public void onStart() {

    }

    public void onCanceled(){

    }

    public abstract void onProgress(long progress, long total);

    public abstract void onSuccess(T result);

    public abstract void onFailure(String message);
}
