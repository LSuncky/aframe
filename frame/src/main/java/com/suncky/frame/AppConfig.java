package com.suncky.frame;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Environment;
import android.util.DisplayMetrics;

import com.suncky.frame.utils.FileUtil;

public class AppConfig {

    public static Context appContext;
    /**
     * 版本名
     */
    public static String versionName;
    /**
     * 版本号
     */
    public static int versionCode;
    /**
     * 屏幕宽度
     */
    public static int screenWidth;
    /**
     * 屏幕高度
     */
    public static int screenHeight;
    /**
     * 状态栏高度
     */
    public static int statusBarHeight;
    /**
     * density=densityDpi/标准dpi(160)dp（设备无关像素）与px(像素)之间的转换则由density值决定
     */
    public static float density;
    /**
     * 谷歌定义 densityDpi=160为基准。<br/>
     * DisplayMetrics中的densityDpi就是dpi
     */
    public static int densityDpi;
    /**
     * DisplayMetrics中的scaledDensity就是sp
     */
    public static float scaledDensity;

    /**
     * 缓存路径
     */
    public static String cachePath;

    /**
     * 图片缓存路径
     */
    public static String imagePath;
    /**
     * 文件缓存路径
     */
    public static String filePath;

    public static boolean debug;

    /**
     * application的onCreate执行
     */
    public static void initInApp(Context context) {
        intAppContext(context);
        initDisplay(context);
        initVersionByPackage(context);
        initPath(context);
    }

    private static void intAppContext(Context context) {
        appContext = context;
    }

    private static void initDisplay(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
        density = metrics.density;
        densityDpi = metrics.densityDpi;
        scaledDensity = metrics.scaledDensity;
        statusBarHeight = getStatusBarHeight();
    }

    private static void initVersionByPackage(Context context) {
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_CONFIGURATIONS);
            versionName = pi.versionName;
            versionCode = pi.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void initPath(Context context) {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            cachePath = context.getExternalCacheDir().getAbsolutePath();
        } else {
            cachePath = context.getCacheDir().getAbsolutePath();
        }
        imagePath = cachePath + "/image/";
        filePath = cachePath + "/file/";
        FileUtil.createDirectory(AppConfig.imagePath);
        FileUtil.createDirectory(AppConfig.filePath);
    }

    public static Context getAppContext() {
        return appContext;
    }

    //获取状态栏高度
    private static int getStatusBarHeight() {
        int result = 0;
        Resources resources = Resources.getSystem();
        int resourceId = resources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
