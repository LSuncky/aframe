package com.suncky.frame.refresh;

import androidx.annotation.NonNull;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

/**
 * 针对SmartRefreshLayout控件的封装
 * @author gl
 * @time 2021/4/14 15:34
 */
public class SmartRefreshView extends RefreshView<SmartRefreshLayout>{
    public SmartRefreshView(SmartRefreshLayout view) {
        super(view);
    }



    @Override
    protected void finishRefresh() {
        getView().finishRefresh();
    }

    @Override
    protected void finishLoadMore() {
        getView().finishLoadMore();
    }

    @Override
    protected void refreshEnable(boolean enable) {
        getView().setEnableRefresh(enable);
    }

    @Override
    protected void loadMoreEnable(boolean enable) {
        getView().setEnableLoadMore(enable);
    }

    @Override
    public void autoRefresh() {
        getView().autoRefresh();
    }

    @Override
    public void autoLoadMore() {
        getView().autoLoadMore();
    }

    @Override
    protected void setNoMoreData(boolean noMoreData) {
        getView().setNoMoreData(noMoreData);
    }

    @Override
    protected void fitRefreshLoadMoreListener(RefreshLoadMoreListener refreshLoadMoreListener) {
        view.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                if (refreshLoadMoreListener != null) {
                    refreshLoadMoreListener.onLoadMore();
                }
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                if (refreshLoadMoreListener != null) {
                    refreshLoadMoreListener.onRefresh();
                }
            }
        });
    }

    @Override
    public RefreshState getRefreshState() {
        com.scwang.smartrefresh.layout.constant.RefreshState state = getView().getState();
        if (state==com.scwang.smartrefresh.layout.constant.RefreshState.None){
            return RefreshState.NONE;
        } else if (state==com.scwang.smartrefresh.layout.constant.RefreshState.PullDownToRefresh) {
            return RefreshState.PULL_DOWN;
        } else if (state==com.scwang.smartrefresh.layout.constant.RefreshState.Refreshing) {
            return RefreshState.PULL_DOWN_REFRESHING;
        } else if (state==com.scwang.smartrefresh.layout.constant.RefreshState.PullDownCanceled) {
            return RefreshState.PULL_DOWN_CANCELED;
        } else if (state==com.scwang.smartrefresh.layout.constant.RefreshState.RefreshFinish) {
            return RefreshState.PULL_DOWN_REFRESH_FINISHED;
        } else if (state==com.scwang.smartrefresh.layout.constant.RefreshState.PullUpToLoad) {
            return RefreshState.PULL_UP;
        } else if (state==com.scwang.smartrefresh.layout.constant.RefreshState.Loading) {
            return RefreshState.PULL_UP_LOADING;
        } else if (state==com.scwang.smartrefresh.layout.constant.RefreshState.PullUpCanceled) {
            return RefreshState.PULL_UP_CANCELED;
        } else if (state==com.scwang.smartrefresh.layout.constant.RefreshState.LoadFinish) {
            return RefreshState.PULL_UP_LOAD_FINISHED;
        }else {
            return RefreshState.NONE;
        }
    }
}
