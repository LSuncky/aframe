package com.suncky.frame.refresh;

import androidx.annotation.NonNull;

import com.suncky.frame.http.bean.Page;

/**
 * 针对SmartRefreshLayout控件的刷新、分页工具类
 *
 * @author gl
 * @time 2021/4/14 17:43
 */
public class SmartRefreshLoadMore<T, P extends Page> extends RefreshLoadMoreImpl<SmartRefreshView, T, P> {

    public SmartRefreshLoadMore(@NonNull SmartRefreshView refreshView) {
        super(refreshView);
    }
}
