package com.suncky.frame.refresh;

/**
 * 上下拉监听
 * @author gl
 * @time 2021/4/14 16:04
 */
public interface RefreshLoadMoreListener {
    void onRefresh();
    void onLoadMore();
}
