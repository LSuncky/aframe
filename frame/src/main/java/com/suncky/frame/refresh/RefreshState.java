package com.suncky.frame.refresh;

/**
 * 控件刷新状态
 */
public enum RefreshState {
    /**初始状态or上下拉完成恢复到静止状态**/
    NONE,
    /**正在下拉**/
    PULL_DOWN,
    /**下拉正在刷新**/
    PULL_DOWN_REFRESHING,
    /**下拉刷新完成**/
    PULL_DOWN_REFRESH_FINISHED,
    /**下拉刷新取消**/
    PULL_DOWN_CANCELED,
    /**正在上拉**/
    PULL_UP,
    /**上拉正在加载**/
    PULL_UP_LOADING,
    /**上拉加载完成**/
    PULL_UP_LOAD_FINISHED,
    /**上拉加载取消**/
    PULL_UP_CANCELED
}
