package com.suncky.frame.refresh;

import android.view.View;

import androidx.annotation.NonNull;

import com.suncky.frame.http.bean.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * 刷新、分页工具类
 *
 * @author gl
 * @time 2021/4/14 15:53
 */
public class RefreshLoadMoreImpl<R extends RefreshView<? extends View>, T, P extends Page> implements RefreshLoadMore<T, P> {

    private int page = 1;
    private List<T> data;
    private int refreshType = TYPE_REFRESH;
    private final R refreshView;
    private RefreshLoadMoreListener refreshLoadMoreListener;

    public RefreshLoadMoreImpl(@NonNull R refreshView) {
        this.refreshView = refreshView;
        init();
    }

    private void init() {
        data = new ArrayList<>();
        refreshView.fitRefreshLoadMoreListener(new RefreshLoadMoreListener() {
            @Override
            public void onRefresh() {
                refreshType = TYPE_REFRESH;
                if (refreshLoadMoreListener != null) {
                    refreshLoadMoreListener.onRefresh();
                }
            }

            @Override
            public void onLoadMore() {
                refreshType = TYPE_LOAD_MORE;
                if (refreshLoadMoreListener != null) {
                    refreshLoadMoreListener.onLoadMore();
                }
            }
        });
    }

    public R getRefreshView() {
        return refreshView;
    }

    @Override
    public List<T> getData() {
        return data;
    }

    @Override
    public int currentPage() {
        return page;
    }

    @Override
    public int nextPage() {
        if (TYPE_LOAD_MORE == refreshType) {
            return page + 1;
        } else{
            return 1;
        }
    }

    @Override
    public int getRefreshType() {
        return refreshType;
    }

    @Override
    public void setRefreshType(int refreshType) {
        this.refreshType = refreshType;
    }

    @Override
    public void finish() {
        if (TYPE_LOAD_MORE == refreshType) {
            refreshView.finishLoadMore();
        } else if(TYPE_REFRESH == refreshType){
            refreshView.finishRefresh();
        }
    }

    @Override
    public void onResult(List<T> result, P dataPage) {
        if (dataPage != null) {
            if (dataPage.currentPage() < dataPage.totalPage()) {
                page = dataPage.currentPage();
                refreshView.setNoMoreData(false);
            } else if (dataPage.currentPage() == dataPage.totalPage()) {
                page = dataPage.currentPage();
                refreshView.setNoMoreData(true);
            } else if (dataPage.totalPage() > 0) {
                refreshView.setNoMoreData(true);
            }
        }
        if (TYPE_REFRESH == refreshType) {
            data.clear();
        }
        if (result != null && result.size() > 0) {
            data.addAll(result);
        }
    }

    @Override
    public void refreshEnable(boolean enable) {
        refreshView.setRefreshEnable(enable);
    }

    @Override
    public void loadMoreEnable(boolean enable) {
        refreshView.setLoadMoreEnable(enable);
    }

    @Override
    public void autoRefresh() {
        refreshView.autoRefresh();
    }

    @Override
    public void autoLoadMore() {
        refreshView.autoLoadMore();
    }


    @Override
    public void setRefreshLoadMoreListener(RefreshLoadMoreListener refreshLoadMoreListener) {
        this.refreshLoadMoreListener = refreshLoadMoreListener;
    }
}
