package com.suncky.frame.refresh;

import android.view.View;

import androidx.annotation.NonNull;

/**
 * 数据刷新、分页控件
 * @author gl
 * @time 2021/4/14 15:08
 */
public abstract class RefreshView<V extends View> {
    protected V view;
    protected boolean refreshEnable = true;
    protected boolean loadMoreEnable = true;

    public RefreshView(V view) {
        this.view = view;
    }

    protected abstract void finishRefresh();
    protected abstract void finishLoadMore();
    protected abstract void refreshEnable(boolean enable);
    protected abstract void loadMoreEnable(boolean enable);
    public abstract void autoRefresh();
    public abstract void autoLoadMore();
    protected abstract void setNoMoreData(boolean noMoreData);
    protected abstract void fitRefreshLoadMoreListener(RefreshLoadMoreListener refreshLoadMoreListener);
    public abstract RefreshState getRefreshState();

    public void setRefreshEnable(boolean enable) {
        this.refreshEnable = enable;
        refreshEnable(enable);
    }
    public void setLoadMoreEnable(boolean enable) {
        this.loadMoreEnable = enable;
        loadMoreEnable(enable);
    }

    public V getView() {
        return view;
    }

    public boolean isRefreshEnable() {
        return refreshEnable;
    }

    public boolean isLoadMoreEnable() {
        return loadMoreEnable;
    }
}
