package com.suncky.frame.refresh;

import com.suncky.frame.http.bean.Page;

import java.util.List;

/**
 * 数据刷新、分页
 * @author gl
 * @time 2021/4/14 14:25
 */
public interface RefreshLoadMore<T, P extends Page> {

    int TYPE_REFRESH = 1;
    int TYPE_LOAD_MORE = 2;

    void setRefreshLoadMoreListener(RefreshLoadMoreListener refreshLoadMoreListener);

    List<T> getData();

    int currentPage();

    int nextPage();

    /**
     * 获取数据刷新类型-刷新\加载更多
     * @return
     */
    int getRefreshType();

    /**
     * 设置数据刷新类型
     * @param refreshType
     */
    void setRefreshType(int refreshType);

    void finish();

    void onResult(List<T> result, P dataPage);

    void refreshEnable(boolean enable);
    void loadMoreEnable(boolean enable);

    void autoRefresh();

    void autoLoadMore();
}
