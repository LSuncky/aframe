package com.suncky.frame;

import android.app.Application;

public class ConfigApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppConfig.initInApp(this);
    }
}
