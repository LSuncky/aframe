package com.suncky.frame.base.adapter.listener;

/**
 * 列表适配器回调
 * Created by YXY on 8/1 0001.
 */
public interface ListAdapterListener {

    /**
     * 点击事件回调
     * @param position 位置
     * @param viewId 控件id
     */
    void onClickEvent(int position, int viewId);

    void onItemClickEvent(int position, int subPosition, int viewId);

}
