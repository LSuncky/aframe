package com.suncky.frame.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.suncky.frame.dialog.LoadingDialog;
import com.suncky.frame.utils.LogUtils;
import com.suncky.frame.utils.SingleClickManager;

public abstract class BaseFragment extends Fragment implements View.OnClickListener {
    public String TAG;
    protected View mRootView;//根布局
    protected boolean initFlag=false;
    protected Bundle initInstanceState;
    private LoadingDialog loadingPop;

    public BaseFragment() {
        TAG = this.getClass().toString();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initInstanceState = savedInstanceState;
        if (mRootView == null) {
            mRootView = getContentView();
        }
        if (mRootView == null) {
            return null;
        }
        ViewGroup parent = (ViewGroup) mRootView.getParent();
        if (parent != null) {
            parent.removeView(mRootView);
        }
        return mRootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mRootView == null) {
            return;
        }
        if (!isLazy()) {
            init();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isLazy()) {
            //以FragmentTransaction.setMaxLifecycle方式实现的懒加载
            init();
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //兼容setUserVisibleHint方式实现的懒加载
        if (isLazy() && isVisibleToUser) {
            init();
        }
    }

    private void init() {
        if (initFlag) {
            return;
        }
        initFlag = true;
        initView(mRootView, initInstanceState);
        initData(initInstanceState);
        mRootView.post(new Runnable() {
            @Override
            public void run() {
                if (!isAdded()) {
                    return;
                }
                startLoading();
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (SingleClickManager.isReClick(id)) {
            return;
        }
        onSingledClick(v);
    }

    /**
     * 防重复点击事件响应
     * 防重复点击，需重写此方法，在此方法中执行点击事件
     * @param v
     */
    protected void onSingledClick(View v) {

    }

    /**
     * 显示加载框
     */
    public void showLoadingPop() {
        showLoadingPop(null);
    }

    /**
     * 显示加载框
     * @param title 提示信息
     */
    public void showLoadingPop(String title) {
        if (loadingPop != null) {
            if (loadingPop.isShowing()) {
                loadingPop.dismiss();
            }
        }
        loadingPop = buildLoadingDialog(title);
        loadingPop.show();
    }

    public void dismissLoadingPop() {
        if (loadingPop != null && loadingPop.isShowing()) {
            loadingPop.dismiss();
        }
    }

    /**
     * 构建加载框
     * @param title 提示信息
     * @return
     */
    protected LoadingDialog buildLoadingDialog(String title) {
        return LoadingDialog.with(getContext()).message(title);
    }

    protected abstract int getLayoutResId();

    protected View getContentView() {
        if (getLayoutResId() != 0) {
            return getLayoutInflater().inflate(getLayoutResId(), null);
        }
        return null;
    }

    /**
     * 初始化view
     * @param rootView content view
     * @param savedInstanceState previous saved state
     */
    protected void initView(View rootView, @Nullable Bundle savedInstanceState) {

    }

    /**
     * 初始化数据
     * @param savedInstanceState previous saved state
     */
    protected void initData(Bundle savedInstanceState) {

    }

    /**
     * 请求数据
     */
    protected void startLoading(){

    }

    /**
     * 是否懒加载
     * @return true-是 false-否
     */
    protected boolean isLazy() {
        return false;
    }
}
