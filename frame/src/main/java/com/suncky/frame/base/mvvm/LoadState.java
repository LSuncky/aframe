package com.suncky.frame.base.mvvm;

public class LoadState {
    public static final int PRE_LOAD = 1;//加载前
    public static final int LOADING = 2;//加载中
    public static final int FINISHED = 3;//加载完成

    private final int state;//加载状态
    private final int id;//加载id
    private String desc;//描述信息

    private LoadState(int state) {
        this.state = state;
        this.id = -1;
    }

    private LoadState(int state, int id) {
        this.state = state;
        this.id = id;
    }

    private LoadState(int state, int id, String desc) {
        this.state = state;
        this.id = id;
        this.desc = desc;
    }

    /**
     * 获取加载状态
     * @return
     */
    public int getState() {
        return state;
    }

    /**
     * 获取加载id
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * 获取描述信息
     * @return
     */
    public String getDesc() {
        return desc;
    }

    public static LoadState newInstance(int state, int id, String desc) {
        return new LoadState(state, id, desc);
    }

    public static LoadState newInstance(int state, int id) {
        return new LoadState(state, id);
    }

    public static LoadState newInstance(int state) {
        return new LoadState(state, -1);
    }
}
