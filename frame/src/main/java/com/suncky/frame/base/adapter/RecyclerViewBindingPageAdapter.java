package com.suncky.frame.base.adapter;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;

import com.suncky.frame.base.adapter.holder.RecyclerViewBindingHolder;
import com.suncky.frame.base.adapter.holder.RecyclerViewHolder;
import com.suncky.frame.base.adapter.listener.OnItemClickListener;
import com.suncky.frame.base.adapter.listener.OnItemLongClickListener;
import com.suncky.frame.base.adapter.listener.RecyclerAdapterListener;
import com.suncky.frame.base.adapter.listener.RecyclerBindingAdapterListener;

/**
 *
 * Created by HY_PC on 2019 2/11 0011 17:57.
 */
public abstract class RecyclerViewBindingPageAdapter<M,B extends ViewDataBinding> extends PagedListAdapter<M,RecyclerViewBindingHolder<B>> implements RecyclerViewBindingHolder.SubViewEventListener<B> {
    protected Context mContext;
    protected RecyclerAdapterListener mListener;
    protected RecyclerBindingAdapterListener<B> mBindingAdapterListener;
    protected OnItemClickListener itemClickListener;
    protected OnItemLongClickListener itemLongClickListener;
    protected int mMarkedPosition = -1;//标记位置
    private final SparseArray<RecyclerViewBindingHolder<B>> holderArray = new SparseArray<>();//保存ViewHolder和position的对应关系

    public RecyclerViewBindingPageAdapter(Context context, DiffUtil.ItemCallback<M> itemCallback) {
        this(context, itemCallback,null);
    }
    public RecyclerViewBindingPageAdapter(Context context, DiffUtil.ItemCallback<M> itemCallback, RecyclerBindingAdapterListener<B> listener){
        super(itemCallback);
        this.mContext = context;
        this.mBindingAdapterListener = listener;
    }

    @NonNull
    @Override
    public RecyclerViewBindingHolder<B> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewBindingHolder<>(getDataBinding(LayoutInflater.from(mContext), parent, viewType), this);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewBindingHolder<B> holder, int position) {
        convert(holder, getItem(position), position);
    }

    /**
     * 创建并返回ViewDataBinding对象
     * @param parent 父view
     * @param viewType {@link #getItemViewType(int)}返回的view类型，用于多种item布局的情况
     * @return ViewDataBinding对象
     */
    public abstract B getDataBinding(@NonNull LayoutInflater inflater, ViewGroup parent, int viewType);

    public abstract void convert(RecyclerViewBindingHolder<B> holder, M item, int position);

    @Nullable
    @Override
    public M getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    /**
     * 获取标记位置
     * @return
     */
    public int getMarkedPosition() {
        return mMarkedPosition;
    }

    /**
     * 获取标记位置
     * @param position
     */
    public void setMarkedPosition(int position) {
        this.mMarkedPosition = position;
    }

    /**
     * 获取标记位置下的数据
     * @return
     */
    public M getMarkedPositionItem(){
        return getItem(mMarkedPosition);
    }

    /**
     * 刷新标记位置
     * @param position
     */
    public void refresh(int position) {
        setMarkedPosition(position);
        notifyDataSetChanged();
    }

    /**
     * 移除数据，不重新绑定数据
     * @param position
     */
    public void remove(int position) {
        if (getCurrentList() == null || position < 0 || position > getCurrentList().size() - 1) {
            return;
        }
        getCurrentList().remove(position);
        notifyItemRemoved(position);
    }

    /**
     * 移除数据，重新绑定数据
     * @param position
     */
    public void removeUpdate(int position) {
        if (getCurrentList() == null || position < 0 || position > getCurrentList().size() - 1) {
            return;
        }
        getCurrentList().remove(position);
        notifyDataSetChanged();
    }

    /**
     * 清空数据源
     */
    public void clearData() {
        if (getCurrentList() != null) {
            getCurrentList().clear();
        }
        notifyDataSetChanged();
    }

    /**
     * 设置事件监听
     * @deprecated use {@link #setBindingAdapterListener(RecyclerBindingAdapterListener)} instead
     * @param listener
     */
    @Deprecated
    public void setAdapterListener(RecyclerAdapterListener listener) {
        mListener = listener;
    }

    /**
     * 设置事件监听
     * @param mBindingAdapterListener
     */
    public void setBindingAdapterListener(RecyclerBindingAdapterListener<B> mBindingAdapterListener) {
        this.mBindingAdapterListener = mBindingAdapterListener;
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener itemLongClickListener) {
        this.itemLongClickListener = itemLongClickListener;
    }

    @Override
    public void onClick(View v, int position, B binding) {
        if (v == holderArray.get(position).itemView && itemClickListener != null) {
            itemClickListener.onItemClick(v, position);
            return;
        }
        if (mListener != null) {
            mListener.onClickEvent(position, v.getId());
        }
        if (mBindingAdapterListener != null) {
            mBindingAdapterListener.onClickEvent(position, v.getId(), binding);
        }
    }

    @Override
    public void onLongClick(View v, int position, B binding) {
        if (v == holderArray.get(position).itemView && itemLongClickListener != null) {
            itemLongClickListener.onItemLongClick(v, position);
            return;
        }
        if (mListener != null) {
            mListener.onLongClickEvent(position, v.getId());
        }
        if (mBindingAdapterListener != null) {
            mBindingAdapterListener.onClickEvent(position, v.getId(), binding);
        }
    }

    @Override
    public void onItemClick(View v, int position, int subPosition, B binding) {

    }
}
