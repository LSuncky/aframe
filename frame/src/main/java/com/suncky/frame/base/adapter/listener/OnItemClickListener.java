package com.suncky.frame.base.adapter.listener;

import android.view.View;

/**
 * Created by HY_PC on 2019 2/12 0012 11:28.
 */
public interface OnItemClickListener {
    void onItemClick(View v, int position);
}
