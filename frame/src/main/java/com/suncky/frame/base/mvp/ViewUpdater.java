package com.suncky.frame.base.mvp;

import androidx.annotation.NonNull;

@FunctionalInterface
public interface ViewUpdater<V extends IView> {
    void update(@NonNull V view);
}
