package com.suncky.frame.base;

import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public abstract class BaseDataBindingActivity<B extends ViewDataBinding> extends BaseActivity {

    private B dataBinding;

    public B getBinding() {
        return dataBinding;
    }

    @Override
    protected View getContentView() {
        View view = super.getContentView();
        if (view != null) {
            dataBinding = DataBindingUtil.bind(view);
        }
        return view;
    }
}
