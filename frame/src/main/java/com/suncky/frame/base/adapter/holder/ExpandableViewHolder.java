package com.suncky.frame.base.adapter.holder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestListener;
import com.suncky.frame.utils.SingleClickManager;
import com.suncky.frame.utils.glide.GlideUtils;

/**
 * Created by HY_PC on 2018 11/26 0026 14:01.
 */
public class ExpandableViewHolder implements View.OnClickListener{

    public static final int VIEW_TYPE_GROUP = 1;
    public static final int VIEW_TYPE_CHILD = 2;

    private final SparseArray<View> mViews;
    private int groupPosition = -1;
    private int childPosition = -1;
    private View mConvertView;
    private ViewEventListener mEventListener;
    private int viewType;

    private ExpandableViewHolder(Context context, ViewGroup parent, int layoutId,
                                 int groupPosition,int childPosition, ViewEventListener eventListener) {
        this.groupPosition = groupPosition;
        this.childPosition = childPosition;
        this.mViews = new SparseArray<>();
        if (layoutId != 0) {
            mConvertView = LayoutInflater.from(context).inflate(layoutId, parent, false);
            // setTag
            mConvertView.setTag(this);
        }
        this.mEventListener = eventListener;
    }

    /**
     * 拿到一个ViewHolder对象
     *
     * @param context
     * @param convertView
     * @param parent
     * @param layoutId
     * @param groupPosition
     * @return
     */
    public static ExpandableViewHolder getGroupViewHolder(Context context, View convertView,
                                           ViewGroup parent, int layoutId, int groupPosition, ViewEventListener eventListener) {
        if (convertView == null) {
            return new ExpandableViewHolder(context, parent, layoutId, groupPosition,-1, eventListener);
        }else {
            ExpandableViewHolder viewHolder = (ExpandableViewHolder) convertView.getTag();
            viewHolder.setGroupPosition(groupPosition);
            return viewHolder;
        }
    }

    public static ExpandableViewHolder getChildViewHolder(Context context, View convertView,
                                                          ViewGroup parent, int layoutId, int groupPosition, int childPosition, ViewEventListener eventListener) {
        if (convertView == null) {
            return new ExpandableViewHolder(context, parent, layoutId, groupPosition, childPosition, eventListener);
        } else {
            ExpandableViewHolder viewHolder = (ExpandableViewHolder) convertView.getTag();
            viewHolder.setGroupPosition(groupPosition);
            viewHolder.setChildPosition(childPosition);
            return viewHolder;
        }
    }

    public View getConvertView() {
        return mConvertView;
    }

    /**
     * 通过控件的Id获取对于的控件，如果没有则加入views
     *
     * @param viewId
     * @return
     */
    public <T extends View> T getView(int viewId) {
        View view = mViews.get(viewId);
        if (view == null && mConvertView != null) {
            view = mConvertView.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * 为TextView设置字符串
     * @param viewId
     * @param text
     * @return
     */
    public ExpandableViewHolder setText(int viewId, String text) {
        TextView view = getView(viewId);
        view.setText(text);
        return this;
    }

    /**
     * 为TextView设置字符串资源
     * @param viewId
     * @param resId
     * @return
     */
    public ExpandableViewHolder setText(int viewId, int resId) {
        TextView view = getView(viewId);
        view.setText(resId);
        return this;
    }

    /**
     * 为ImageView设置图片
     * @param viewId
     * @param resId
     * @return
     */
    public ExpandableViewHolder setImageResource(int viewId, int resId) {
        ImageView view = getView(viewId);
        view.setImageResource(resId);
        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param bm
     * @return
     */
    public ExpandableViewHolder setImageBitmap(int viewId, Bitmap bm) {
        ImageView view = getView(viewId);
        view.setImageBitmap(bm);
        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param url
     * @return
     */
    public ExpandableViewHolder setImageUrl(int viewId, String url, int defRes) {
        if (mConvertView == null) {
            return this;
        }
        ImageView view = getView(viewId);
        GlideUtils.load(mConvertView.getContext(), url, view, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片url和监听
     *
     * @param viewId
     * @param url
     * @param requestListener
     * @return
     */
    public ExpandableViewHolder setImageUrl(int viewId, String url, int defRes, RequestListener<Drawable> requestListener) {
        if (mConvertView == null) {
            return this;
        }
        ImageView view = getView(viewId);
        GlideUtils.load(mConvertView.getContext(), url, view, 0,false,defRes, requestListener);
        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param url
     * @return
     */
    public ExpandableViewHolder setImageUrl(int viewId, String url , int radius, boolean isCircle, int defRes) {
        if (mConvertView == null) {
            return this;
        }
        ImageView view = getView(viewId);
        GlideUtils.load(mConvertView.getContext(), url, view, radius, isCircle, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片url和监听
     *
     * @param viewId
     * @param url
     * @param requestListener
     * @return
     */
    public ExpandableViewHolder setImageUrl(int viewId, String url , int radius, boolean isCircle, int defRes, RequestListener<Drawable> requestListener) {
        if (mConvertView == null) {
            return this;
        }
        ImageView view = getView(viewId);
        GlideUtils.load(mConvertView.getContext(), url, view, radius, isCircle, defRes, requestListener);
        return this;
    }

    /**
     * 设置点击事件
     *
     * @param viewId
     * @return
     */
    public ExpandableViewHolder setClickListener(int viewId) {
        getView(viewId).setOnClickListener(this);
        return this;
    }

    /**
     * 设置可见性
     * @param viewId
     * @param visibility
     */
    public ExpandableViewHolder setVisibility(int viewId, int visibility) {
        getView(viewId).setVisibility(visibility);
        return this;
    }

    /**
     * 设置背景
     * @param viewId
     * @param resId
     * @return
     */
    public ExpandableViewHolder setBackgroundResource(int viewId, int resId){
        getView(viewId).setBackgroundResource(resId);
        return this;
    }

    /**
     * 设置文字颜色
     * @param viewId
     * @param color
     * @return
     */
    public ExpandableViewHolder setTextColor(int viewId, int color){
        TextView view = getView(viewId);
        view.setTextColor(color);
        return this;
    }

    public ExpandableViewHolder setEnable(int viewId, boolean enable){
        getView(viewId).setEnabled(enable);
        return this;
    }


    public void setGroupPosition(int groupPosition) {
        this.groupPosition = groupPosition;
    }

    public void setChildPosition(int childPosition) {
        this.childPosition = childPosition;
    }

    public int getGroupPosition() {
        return groupPosition;
    }

    public int getChildPosition() {
        return childPosition;
    }

    @Override
    public void onClick(View v) {
        if (SingleClickManager.isReClick(v.getId())) {
            return;
        }
        if (mEventListener != null) {
            if (childPosition >= 0) {
                mEventListener.onChildViewClick(v, groupPosition, childPosition);
            } else {
                mEventListener.onGroupViewClick(v, groupPosition);
            }
        }
    }

    public interface ViewEventListener {
        void onGroupViewClick(View v, int groupPosition);
        void onChildViewClick(View v, int groupPosition,int childPosition);
    }
}
