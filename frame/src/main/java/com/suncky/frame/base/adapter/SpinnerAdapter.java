package com.suncky.frame.base.adapter;

import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.suncky.frame.base.adapter.holder.ViewHolder;
import com.suncky.frame.base.adapter.listener.ListAdapterListener;
import com.suncky.frame.utils.CollectionUtils;

import java.util.List;

/**
 * 适用于Spinner控件的adapter，
 * 用于显示内容的TextView的id须设为android:id/text1
 * @author gl
 * @time 2024-6-6
 */
public abstract class SpinnerAdapter<T> extends ArrayListAdapter<T>{
    public SpinnerAdapter(Context context) {
        super(context);
    }

    public SpinnerAdapter(Context context, List<T> data) {
        super(context, data);
    }

    public SpinnerAdapter(Context context, List<T> data, int dropDownLayoutId) {
        super(context, data, dropDownLayoutId);
    }

    public SpinnerAdapter(Context context, List<T> data, int dropDownLayoutId, int contentLayoutId) {
        super(context, data, dropDownLayoutId, contentLayoutId);
    }

    public SpinnerAdapter(Context context, List<T> data, int dropDownLayoutId, int contentLayoutId, ListAdapterListener listener) {
        super(context, data, dropDownLayoutId, contentLayoutId, listener);
    }

    protected int selectedDropdownTextColor= -1;
    protected int selectDropDownPosition = -1;

    /**
     * 设置选中的下拉项字体颜色
     */
    public void setSelectedDropdownTextColor(int selectedDropdownTextColor) {
        this.selectedDropdownTextColor = selectedDropdownTextColor;
    }

    /**
     * 设置选中的下拉列表位置
     */
    public void selectDropDownPosition(int selectDropDownPosition) {
        this.selectDropDownPosition = selectDropDownPosition;
        notifyDataSetChanged();
    }

    /**
     * 设置选中的下拉列表项
     */
    public void selectDropDownItem(T item) {
        if (mDatas == null || mDatas.size() == 0) {
            return;
        }
        this.selectDropDownPosition = mDatas.indexOf(item);
        notifyDataSetChanged();
    }

    @Override
    public void convert(ViewHolder holder, T item, int position) {
        holder.setText(android.R.id.text1, onGetContentText(item, position));
    }

    @Override
    public void convertDropDownView(ViewHolder holder, T item, int position) {
        holder.setText(android.R.id.text1, onGetDropDownText(item, position));
        if (position == selectDropDownPosition) {
            holder.setTextColor(android.R.id.text1, selectedDropdownTextColor);
        }else {
            holder.setTextColor(android.R.id.text1, dropdownTextColor);
        }
    }

    /**
     * 获取控件上显示的内容
     */
    public abstract String onGetContentText(T item, int position);

    /**
     * 获取下拉显示的内容
     */
    public abstract String onGetDropDownText(T item, int position);
}
