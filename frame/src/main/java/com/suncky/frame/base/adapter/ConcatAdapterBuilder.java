package com.suncky.frame.base.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

/**
 * ConcatAdapter管理类
 */
public class ConcatAdapterBuilder {

    /**
     * 创建一个默认配置的ConcatAdapter对象
     * {@link ConcatAdapter.Config#isolateViewTypes} 设置为 {@code true},
     * {@link ConcatAdapter.Config#stableIdMode} 设置为 {@link ConcatAdapter.Config.StableIdMode#NO_STABLE_IDS}.
     *
     * @param adapters
     * @return
     */
    @SafeVarargs
    public static ConcatAdapter defaultConfigAdapter(@NonNull RecyclerView.Adapter<? extends RecyclerView.ViewHolder>... adapters) {
        return new ConcatAdapter(ConcatAdapter.Config.DEFAULT, adapters);
    }

    private boolean mIsolateViewTypes = ConcatAdapter.Config.DEFAULT.isolateViewTypes;
    private ConcatAdapter.Config.StableIdMode mStableIdMode = ConcatAdapter.Config.DEFAULT.stableIdMode;
    private List<? extends RecyclerView.Adapter<? extends RecyclerView.ViewHolder>> adapters;

    /**
     * Sets whether {@link ConcatAdapter} should isolate view types of nested adapters from
     * each other.
     *
     * @param isolateViewTypes {@code true} if {@link ConcatAdapter} should override view
     *                         types of nested adapters to avoid view type
     *                         conflicts, {@code false} otherwise.
     *                         Defaults to {@link ConcatAdapter.Config#DEFAULT}'s
     *                         {@link ConcatAdapter.Config#isolateViewTypes} value ({@code true}).
     * @return this
     * @see ConcatAdapter.Config#isolateViewTypes
     */
    @NonNull
    public ConcatAdapterBuilder setIsolateViewTypes(boolean isolateViewTypes) {
        mIsolateViewTypes = isolateViewTypes;
        return this;
    }

    /**
     * Sets how the {@link ConcatAdapter} should handle stable ids
     * ({@link RecyclerView.Adapter#hasStableIds()}). See documentation in {@link ConcatAdapter.Config#stableIdMode}
     * for details.
     *
     * @param stableIdMode The stable id mode for the {@link ConcatAdapter}. Defaults to
     *                     {@link ConcatAdapter.Config#DEFAULT}'s {@link ConcatAdapter.Config#stableIdMode} value
     *                     ({@link ConcatAdapter.Config.StableIdMode#NO_STABLE_IDS}).
     * @return this
     * @see ConcatAdapter.Config#stableIdMode
     */
    @NonNull
    public ConcatAdapterBuilder setStableIdMode(@NonNull ConcatAdapter.Config.StableIdMode stableIdMode) {
        mStableIdMode = stableIdMode;
        return this;
    }

    @NonNull
    public ConcatAdapterBuilder serAdapters(@NonNull RecyclerView.Adapter<? extends RecyclerView.ViewHolder>... adapters) {
        this.adapters = Arrays.asList(adapters);
        return this;
    }

    @NonNull
    public ConcatAdapterBuilder serAdapters(@NonNull List<? extends RecyclerView.Adapter<? extends RecyclerView.ViewHolder>> adapters) {
        this.adapters = adapters;
        return this;
    }

    /**
     * @return A new instance of {@link ConcatAdapter} with the given parameters.
     */
    @NonNull
    public ConcatAdapter build() {
        ConcatAdapter.Config config = new ConcatAdapter.Config.Builder().setIsolateViewTypes(mIsolateViewTypes).setStableIdMode(mStableIdMode).build();
        return new ConcatAdapter(config, adapters);
    }
}
