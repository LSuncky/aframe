package com.suncky.frame.base.adapter.listener;

import android.view.View;
import android.widget.ExpandableListView;

import com.suncky.frame.utils.SingleClickManager;

/**
 * 验证重复点击的ExpandableListView.OnChildClickListener
 */
public abstract class OnChildClickCheckListener implements ExpandableListView.OnChildClickListener{
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        if (!SingleClickManager.isReClick(v.getId())) {
            return onChildClickChecked(parent, v, groupPosition, childPosition, id);
        }
        return false;
    }

    public abstract boolean onChildClickChecked(ExpandableListView parent, View v, int groupPosition, int childPosition, long id);
}
