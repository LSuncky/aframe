package com.suncky.frame.base.adapter.listener;

/**
 * 列表适配器回调
 * Created by YXY on 8/1 0001.
 */
public class SimpleRecyclerAdapterListener implements RecyclerAdapterListener{


    @Override
    public void onClickEvent(int position, int viewId) {

    }

    @Override
    public void onLongClickEvent(int position, int viewId) {

    }
}
