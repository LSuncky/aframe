package com.suncky.frame.base.adapter;

import android.content.Context;
import android.view.View;

import com.suncky.frame.base.adapter.holder.PagerViewHolder;

import java.util.List;

/**
 * 带滑动选项卡PagerAdapter
 * Created by YXY on 4/16 0016.
 */
public class TabPagerAdapter extends PagerAdapter<View> {
    private String[] titles;

    public TabPagerAdapter(Context context, List<View> dataList, String[] titles) {
        super(context, dataList);
        this.titles = titles;
    }

    public void setPagerView(List<View> views, String[] titles) {
        this.titles = titles;
        setData(views);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (titles != null && titles.length > position) {
            return titles[position];
        }
        return super.getPageTitle(position);
    }

    @Override
    protected View getItemView(int position) {
        return getData().get(position);
    }

    @Override
    protected void convert(PagerViewHolder holder, View item, int position) {

    }
}
