package com.suncky.frame.base.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import com.suncky.frame.AppConfig
import com.suncky.frame.R
import com.suncky.frame.base.adapter.holder.RecyclerViewHolder
import com.suncky.frame.utils.LogUtils
import com.suncky.frame.utils.glide.GlideUtils

/**
 * 可增删图片grid列表适配器
 * @param data 图片列表
 * @param edit 是否可修改图片数量
 * @param maxCount 可修改状态下最大图片数量
 * @param columns 每行图片的列数
 * @author gl
 */
abstract class ImageEditGridAdapter(
    context: Context,
    data: List<String>?,
    private var edit: Boolean = true,
    private var maxCount: Int = 6,
    private val columns: Int = 3,
    private var imageWidth: Int? = null,
    private var imageHeight: Int? = null,
) : RecyclerViewAdapter<String>(context, data, R.layout.item_image_edit_list) {

    @Deprecated("this function is deprecated", ReplaceWith("addImage(imgs)"), DeprecationLevel.HIDDEN)
    override fun appendData(data: List<String?>?) {}

    @Deprecated("this function is deprecated", ReplaceWith("addImage(img)"), DeprecationLevel.HIDDEN)
    override fun appendData(data: String?) {}

    @Deprecated("this function is deprecated", ReplaceWith("addImage(imgs)"), DeprecationLevel.HIDDEN)
    override fun insertData(position: Int, datas: List<String?>?) {}

    @Deprecated("this function is deprecated", ReplaceWith("addImage(img)"), DeprecationLevel.HIDDEN)
    override fun insertData(position: Int, data: String?) {}

    @Deprecated("this function is deprecated", ReplaceWith("removeImage(position)"), DeprecationLevel.HIDDEN)
    override fun remove(position: Int) {}


    /**
     * 是否可编辑(添加\删除)
     */
    open fun setEdit(edit: Boolean) {
        this.edit = edit
        notifyDataSetChanged()
    }

    /**
     * 最大图片数量
     */
    open fun setMaxCount(count: Int) {
        this.maxCount = count
        notifyDataSetChanged()
    }

    /**
     * 图片数量
     */
    open fun getImageCount():Int {
        return data?.size?:0
    }

    /**
     * 添加图片
     */
    open fun addImage(img:String?) {
        img?:return
        if (data == null) {
            data = mutableListOf()
        }
        data.add(img)
        notifyDataSetChanged()

    }

    /**
     * 添加图片
     */
    open fun addImage(imgs:List<String>?) {
        if (imgs.isNullOrEmpty()) {
            return
        }
        if (data == null) {
            data = mutableListOf()
        }
        data.addAll(imgs)
        notifyDataSetChanged()
    }

    /**
     * 设置图片数据
     */
    open fun setImage(imgs:List<String>?) {
        if (data == null) {
            data = mutableListOf()
        }
        data.clear()
        if (!imgs.isNullOrEmpty()) {
            data.addAll(imgs)
        }
        notifyDataSetChanged()
    }

    /**
     * 移除图片
     */
    open fun removeImage(position: Int) {
        removeUpdate(position)
    }

    /**
     * 清除所有图片
     */
    open fun clearImage() {
        clearData()
    }

    override fun getItemCount(): Int {
        val count = getImageCount()
        return if (edit && count < maxCount) {
            count + 1
        } else {
            count
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        val llImage = holder.getView<FrameLayout>(R.id.ll_image)
        if (imageWidth == null) {
            llImage.layoutParams.width =
                (AppConfig.screenWidth - mContext.resources.getDimensionPixelOffset(R.dimen.dp_10) * (columns + 1) - mContext.resources.getDimensionPixelOffset(
                    R.dimen.dp_15
                ) * 2) / columns
        }else{
            llImage.layoutParams.width= imageWidth as Int
        }
        if (imageHeight == null) {
            llImage.layoutParams.height = llImage.layoutParams.width
        }else{
            llImage.layoutParams.height= imageHeight as Int
        }
        return holder
    }

    override fun convert(holder: RecyclerViewHolder?, item: String?, position: Int) {
        holder?.setClickListener(R.id.iv_del)?.setClickListener(R.id.iv_pic)
        if (edit) {
            if (getImageCount() < maxCount && (position == itemCount - 1)) {
                holder?.setVisibility(R.id.iv_del,View.GONE)
                holder?.getView<ImageView>(R.id.iv_pic)?.scaleType=ImageView.ScaleType.FIT_XY
                GlideUtils.load(mContext, R.drawable.aframe_image_pick, holder?.getView<ImageView>(R.id.iv_pic), R.drawable.ic_aframe_def_img)
            } else {
                holder?.setVisibility(R.id.iv_del,View.VISIBLE)
                holder?.getView<ImageView>(R.id.iv_pic)?.scaleType=ImageView.ScaleType.CENTER_CROP
                GlideUtils.load(mContext, onGetImageUrl(item, position), holder?.getView<ImageView>(R.id.iv_pic), R.drawable.ic_aframe_def_img)
            }
        } else {
            holder?.setVisibility(R.id.iv_del,View.GONE)
            holder?.getView<ImageView>(R.id.iv_pic)?.scaleType=ImageView.ScaleType.CENTER_CROP
            GlideUtils.load(mContext, onGetImageUrl(item, position), holder?.getView<ImageView>(R.id.iv_pic), R.drawable.ic_aframe_def_img)
        }
//        LogUtils.i("convert", "position=$position item= $item")
    }

    override fun onClick(v: View?, position: Int) {
        super.onClick(v, position)
        when (v?.id) {
            R.id.iv_del -> onDelete(position)//点击删除按钮
            R.id.iv_pic -> onClickImage(position)//点击图片列表
        }
    }

    private fun onClickImage(position: Int) {
        if (edit && getImageCount() < maxCount && (position == itemCount - 1)) {
            //点击列表末尾新增按钮
            onAddImage(position)
        } else {
            //点击列表中的图片
            onViewImage(getItem(position), position)
        }
    }

    /**
     * 删除图片
     */
    open fun onDelete(position: Int){
        removeImage(position)
    }

    /**
     * 添加图片
     */
    open fun onAddImage(position: Int){

    }

    /**
     * 查看图片
     */
    open fun onViewImage(item: String?, position: Int){

    }

    /**
     * 获取图片url
     */
    abstract fun onGetImageUrl(item: String?, position: Int): String?
}