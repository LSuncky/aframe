package com.suncky.frame.base.adapter.listener;

import android.view.View;

/**
 * Created by HY_PC on 2019 2/12 0012 11:29.
 */
public interface OnItemLongClickListener {
    void onItemLongClick(View v, int position);
}
