package com.suncky.frame.base.mvvm.kt

import com.suncky.frame.http.bean.Page

open class StateObserverListener<E,P : Page> {
    var onSuccess: (data: E?, page: P?) -> Unit = { data, page -> }

    var onError: (code: Int, message: String?) -> Unit = { code, message -> }

    var onPreLoad:()->Unit = {}

    var onLoading:()->Unit = {}

    var onLoadFinish:()->Unit = {}
}