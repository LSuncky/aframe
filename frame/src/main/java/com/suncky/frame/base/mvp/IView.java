package com.suncky.frame.base.mvp;

import android.content.Context;

public interface IView {
    Context getContext();
    void onPreLoading(int id);
    void onFinishLoading(int id);
}
