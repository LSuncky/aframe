package com.suncky.frame.base.adapter.listener;

import android.view.View;
import android.widget.AdapterView;

import com.suncky.frame.utils.SingleClickManager;

/**
 * 验证重复点击的AdapterView.OnItemClickListener
 */
public abstract class OnItemClickCheckListener implements AdapterView.OnItemClickListener{
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (!SingleClickManager.isReClick(view.hashCode())) {
            onItemClickChecked(parent, view, position, id);
        }
    }

    public abstract void onItemClickChecked(AdapterView<?> parent, View view, int position, long id);
}
