package com.suncky.frame.base.mvp;

import androidx.annotation.NonNull;

@FunctionalInterface
public interface DataViewUpdater<T,V extends IView> {
    void update(T t, @NonNull V view);
}
