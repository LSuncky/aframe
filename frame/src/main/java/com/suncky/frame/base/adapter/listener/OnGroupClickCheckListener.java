package com.suncky.frame.base.adapter.listener;

import android.view.View;
import android.widget.ExpandableListView;

import com.suncky.frame.utils.SingleClickManager;

/**
 * 验证重复点击的ExpandableListView.OnGroupClickListener
 */
public abstract class OnGroupClickCheckListener implements ExpandableListView.OnGroupClickListener{
    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        if (!SingleClickManager.isReClick(v.getId())) {
            return onGroupClickChecked(parent, v, groupPosition, id);
        }
        return false;
    }

    public abstract boolean onGroupClickChecked(ExpandableListView parent, View v, int groupPosition, long id);
}
