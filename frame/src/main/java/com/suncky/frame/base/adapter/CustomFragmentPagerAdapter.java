package com.suncky.frame.base.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by YXY on 5/19 0019.
 */
public class CustomFragmentPagerAdapter extends FragmentPagerAdapter {
    private List<? extends Fragment> fragments;
    private String[] titles;
    public CustomFragmentPagerAdapter(FragmentManager fm, List<? extends Fragment> fragments) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        if (fragments != null) {
            this.fragments = fragments;
        } else {
            this.fragments = new ArrayList<>();
        }
    }

    public CustomFragmentPagerAdapter(FragmentManager fm, List<? extends Fragment> fragments, String[] titles) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.fragments = fragments;
        this.titles = titles;
    }

    public void setFragments(List<? extends Fragment> fragments, String[] titles) {
        this.fragments = fragments;
        this.titles = titles;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        if (fragments == null) {
            return 0;
        }
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (titles != null && titles.length > position) {
            return titles[position];
        }
        return super.getPageTitle(position);
    }
}
