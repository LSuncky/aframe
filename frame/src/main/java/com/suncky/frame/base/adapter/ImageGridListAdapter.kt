package com.suncky.frame.base.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.suncky.frame.AppConfig
import com.suncky.frame.R
import com.suncky.frame.base.adapter.holder.RecyclerViewHolder

/**
 * 图片grid列表
 * @param data 图片列表
 * @param columns 每行图片的列数
 */
abstract class ImageGridListAdapter(
    context: Context,
    data: List<String>?,
    private val columns: Int = 3,
    private var imageWidth: Int? = null,
    private var imageHeight:Int?=null
) : RecyclerViewAdapter<String>(context, data, R.layout.item_image_list) {

    @Deprecated("this function is deprecated", ReplaceWith("addImage(imgs)"), DeprecationLevel.HIDDEN)
    override fun appendData(data: List<String?>?) {}

    @Deprecated("this function is deprecated", ReplaceWith("addImage(img)"), DeprecationLevel.HIDDEN)
    override fun appendData(data: String?) {}

    @Deprecated("this function is deprecated", ReplaceWith("addImage(imgs)"), DeprecationLevel.HIDDEN)
    override fun insertData(position: Int, datas: List<String?>?) {}

    @Deprecated("this function is deprecated", ReplaceWith("addImage(img)"), DeprecationLevel.HIDDEN)
    override fun insertData(position: Int, data: String?) {}

    @Deprecated("this function is deprecated", ReplaceWith("removeImage(position)"), DeprecationLevel.HIDDEN)
    override fun remove(position: Int) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        val llImage = holder.getView<FrameLayout>(R.id.ll_image)
        if (imageWidth == null) {
            llImage.layoutParams.width =
                (AppConfig.screenWidth - mContext.resources.getDimensionPixelOffset(com.suncky.frame.R.dimen.dp_10) * (columns + 1) - mContext.resources.getDimensionPixelOffset(
                    com.suncky.frame.R.dimen.dp_15
                ) * 2) / columns
        }else{
            llImage.layoutParams.width= imageWidth as Int
        }
        if (imageHeight == null) {
            llImage.layoutParams.height = llImage.layoutParams.width
        }else{
            llImage.layoutParams.height= imageHeight as Int
        }
        llImage.requestLayout()
        return holder
    }

    override fun convert(holder: RecyclerViewHolder?, item: String?, position: Int) {
        holder?.setImageUrl(R.id.iv_pic, onGetImageUrl(item, position), R.drawable.ic_aframe_def_img)
            ?.setClickListener(R.id.iv_pic)
    }

    override fun onClick(v: View?, position: Int) {
        super.onClick(v, position)
        if (v?.id == R.id.iv_pic) {
            //点击列表中的图片
            onViewImage(getItem(position), position)
        }
    }

    /**
     * 图片数量
     */
    open fun getImageCount():Int {
        return data?.size?:0
    }

    /**
     * 添加图片
     */
    open fun addImage(img:String?) {
        img?:return
        if (data == null) {
            data = mutableListOf()
        }
        data.add(img)
        notifyDataSetChanged()
    }

    /**
     * 添加图片
     */
    open fun addImage(imgs:List<String>?) {
        if (imgs.isNullOrEmpty()) {
            return
        }
        if (data == null) {
            data = mutableListOf()
        }
        data.addAll(imgs)
        notifyDataSetChanged()
    }

    /**
     * 设置图片数据
     */
    open fun setImage(imgs:List<String>?) {
        if (data == null) {
            data = mutableListOf()
        }
        data.clear()
        if (!imgs.isNullOrEmpty()) {
            data.addAll(imgs)
        }
        notifyDataSetChanged()
    }

    /**
     * 移除图片
     */
    open fun removeImage(position: Int) {
        removeUpdate(position)
    }

    /**
     * 清除所有图片
     */
    open fun clearImage() {
        clearData()
    }

    /**
     * 获取图片url
     */
    abstract fun onGetImageUrl(item: String?, position: Int): String?


    /**
     * 查看图片
     */
    open fun onViewImage(item: String?, position: Int){

    }
}