package com.suncky.frame.base.adapter.holder;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestListener;
import com.suncky.frame.base.adapter.RecyclerViewAdapter;
import com.suncky.frame.base.adapter.listener.OnItemClickListener;
import com.suncky.frame.utils.SingleClickManager;
import com.suncky.frame.utils.glide.GlideUtils;

/**
 * Created by HY_PC on 2018 11/26 0026 14:01.
 */
public class RecyclerViewBindingHolder<B extends ViewDataBinding> extends RecyclerView.ViewHolder implements View.OnClickListener,
        View.OnLongClickListener, OnItemClickListener {
    private final SubViewEventListener<B> mEventListener;
    private final B binding;
    private View clickedView;//发生过点击事件的view

    public RecyclerViewBindingHolder(B binding, SubViewEventListener<B> eventListener) {
        super(binding.getRoot());
        this.mEventListener = eventListener;
        this.binding = binding;
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public static <B extends ViewDataBinding> RecyclerViewBindingHolder<B> create(ViewGroup parent,
                                                                                  @LayoutRes int layoutId, SubViewEventListener<B> eventListener) {
        B binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                layoutId, parent, false);
        return new RecyclerViewBindingHolder<>(binding, eventListener);
    }

    public B getBinding() {
        return binding;
    }

    /**
     * 为TextView设置字符串
     * @param view
     * @param text
     * @return
     */
    public RecyclerViewBindingHolder<B> setText(TextView view, String text) {
        view.setText(text);
        return this;
    }

    /**
     * 为TextView设置字符串资源
     * @param view
     * @param resId
     * @return
     */
    public RecyclerViewBindingHolder<B> setText(TextView view, int resId) {
        view.setText(resId);
        return this;
    }

    /**
     * 为ImageView设置图片资源
     * @param view
     * @param drawableId
     * @return
     */
    public RecyclerViewBindingHolder<B> setImageResource(ImageView view, int drawableId) {
        view.setImageResource(drawableId);
        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param view
     * @param bm
     * @return
     */
    public RecyclerViewBindingHolder<B> setImageBitmap(ImageView view, Bitmap bm) {
        view.setImageBitmap(bm);
        return this;
    }

    /**
     * 为ImageView设置图片url
     *
     * @param view
     * @param url
     * @return
     */
    public RecyclerViewBindingHolder<B> setImageUrl(ImageView view, String url, int defRes) {
        GlideUtils.load(itemView.getContext(), url, view, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片url和监听
     *
     * @param view
     * @param url
     * @param requestListener
     * @return
     */
    public RecyclerViewBindingHolder<B> setImageUrl(ImageView view, String url, int defRes, RequestListener<Drawable> requestListener) {
        GlideUtils.load(itemView.getContext(), url, view, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param view
     * @param url
     * @return
     */
    public RecyclerViewBindingHolder<B> setImageUrl(ImageView view, String url , int radius, boolean isCircle, int defRes) {
        GlideUtils.load(itemView.getContext(), url, view, radius, isCircle, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片url和监听
     *
     * @param view
     * @param url
     * @param requestListener
     * @return
     */
    public RecyclerViewBindingHolder<B> setImageUrl(ImageView view, String url , int radius, boolean isCircle, int defRes, RequestListener<Drawable> requestListener) {
        GlideUtils.load(itemView.getContext(), url, view, radius, isCircle, defRes, requestListener);
        return this;
    }

    /**
     * 设置点击事件
     *
     * @param view
     * @return
     */
    public RecyclerViewBindingHolder<B> setClickListener(View view) {
        view.setOnClickListener(this);
        return this;
    }

    /**
     * 设置item点击事件
     * @param recyclerView
     * @return
     */
    public RecyclerViewBindingHolder<B> setItemClickListener(RecyclerView recyclerView){
        if(recyclerView.getAdapter() instanceof RecyclerViewAdapter){
            ((RecyclerViewAdapter)recyclerView.getAdapter()).setOnItemClickListener(this);
        }
        return this;
    }

    /**
     * 设置可见性
     * @param view
     * @param visibility
     */
    public RecyclerViewBindingHolder<B> setVisibility(View view, int visibility) {
        view.setVisibility(visibility);
        return this;
    }

    /**
     * 设置背景
     * @param view
     * @param resId
     * @return
     */
    public RecyclerViewBindingHolder<B> setBackgroundResource(View view, int resId){
        view.setBackgroundResource(resId);
        return this;
    }

    /**
     * 设置文字颜色
     * @param view
     * @param color
     * @return
     */
    public RecyclerViewBindingHolder<B> setTextColor(TextView view, int color){
        view.setTextColor(color);
        return this;
    }

    public RecyclerViewBindingHolder<B> setEnable(View view, boolean edable){
        view.setEnabled(edable);
        return this;
    }

    /**
     * 设置适配器
     * @param recyclerView
     * @param adapter
     * @return
     */
    public RecyclerViewBindingHolder<B> setAdapter(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        recyclerView.setAdapter(adapter);
        return this;
    }

    /**
     * 获取适配器
     * @param recyclerView
     * @return
     */
    public RecyclerView.Adapter getAdapter(RecyclerView recyclerView){
        return recyclerView.getAdapter();
    }

    @Override
    public void onClick(View v) {
        if (SingleClickManager.isReClick(v.hashCode())) {
            return;
        }
        if (mEventListener != null) {
            mEventListener.onClick(v, getAdapterPosition(), binding);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mEventListener != null) {
            mEventListener.onLongClick(v, getAdapterPosition(), binding);
        }
        return true;//避免长按时间和点击事件同时触发
    }

    @Override
    public void onItemClick(View v, int position) {
        if (SingleClickManager.isReClick(v.hashCode())) {
            return;
        }
        if (mEventListener != null) {
            mEventListener.onItemClick(v, getAdapterPosition(), position, binding);
        }
    }

    public interface SubViewEventListener<B extends ViewDataBinding> {
        void onClick(View v, int position, B binding);
        void onLongClick(View v, int position, B binding);
        void onItemClick(View v, int position, int subPosition, B binding);
    }
}
