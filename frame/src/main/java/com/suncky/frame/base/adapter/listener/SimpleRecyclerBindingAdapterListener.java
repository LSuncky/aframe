package com.suncky.frame.base.adapter.listener;

import androidx.databinding.ViewDataBinding;

/**
 * 列表适配器回调
 * Created by YXY on 8/1 0001.
 */
public class SimpleRecyclerBindingAdapterListener<B extends ViewDataBinding> implements RecyclerBindingAdapterListener<B>{

    @Override
    public void onClickEvent(int position, int viewId, B binding) {

    }

    @Override
    public void onLongClickEvent(int position, int viewId, B binding) {

    }
}
