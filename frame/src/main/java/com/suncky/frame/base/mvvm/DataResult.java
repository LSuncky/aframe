package com.suncky.frame.base.mvvm;

import com.suncky.frame.http.bean.Page;
import com.suncky.frame.http.bean.Result;

/**
 * @deprecated use {@link com.suncky.frame.http.ResultCallback} instead
 * @param <E>
 * @param <P>
 */
@Deprecated
public class DataResult<E,P extends Page> implements Result<E,P> {
    private E data;
    private P page;
    private String message;
    private int code;
    private boolean isSuccess;

    protected DataResult(E data) {
        this(data, null);
    }

    protected DataResult(E data, P page) {
        this(data, page, null);
    }

    protected DataResult(E data, P page, String message) {
        this.data = data;
        this.page = page;
        this.message = message;
        this.isSuccess = true;
    }

    protected DataResult(String message, int code) {
        this(message, code, false);
    }

    protected DataResult(String message, int code, boolean isSuccess) {
        this.message = message;
        this.code = code;
        this.isSuccess = isSuccess;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public E getData() {
        return data;
    }

    @Override
    public P getPage() {
        return page;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public boolean isSuccess() {
        return isSuccess;
    }

    public static <E,P extends Page> DataResult<E,P> newInstance(E data) {
        return new DataResult<>(data);
    }

    public static <E,P extends Page> DataResult<E,P> newInstance(E data, P page) {
        return new DataResult<>(data, page);
    }

    public static <E,P extends Page> DataResult<E,P> newInstance(E data, P page, String message) {
        return new DataResult<>(data, page,message);
    }

    public static <E,P extends Page> DataResult<E,P> newInstance(String message, int code) {
        return new DataResult<>(message, code);
    }

    public static <E,P extends Page> DataResult<E,P> newInstance(String message, int code, boolean isSuccess) {
        return new DataResult<>(message, code, isSuccess);
    }
}
