package com.suncky.frame.base.mvp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
public abstract class Presenter<V extends IView> {
    @Nullable
    protected V view;

    public Presenter() {
    }

    public Presenter(@Nullable V view) {
        this.view = view;
    }

    @Nullable
    public V getView() {
        return view;
    }

    public void attach(@NonNull V view) {
        this.view = view;
    }
    public void detach() {
        view = null;
    }
    public boolean isAttached() {
        return view != null;
    }

    /**
     * 刷新View数据
     *
     * @param data   数据
     * @param action view刷新函数
     * @param <T>    数据类型
     */
    public <T> void updateView(T data, DataViewUpdater<T, V> action) {
        if (view != null) {
            action.update(data, view);
        }
    }

    /**
     * 刷新View
     * @param action view刷新函数
     */
    public void updateView(ViewUpdater<V> action) {
        if (view != null) {
            action.update(view);
        }
    }
}
