package com.suncky.frame.base.adapter.listener;

import androidx.databinding.ViewDataBinding;

/**
 * 列表适配器回调
 * Created by YXY on 8/1 0001.
 */
public interface RecyclerBindingAdapterListener<B extends ViewDataBinding> {

    /**
     * 点击事件回调
     *
     * @param position 位置
     * @param viewId   控件id
     */
    void onClickEvent(int position, int viewId, B binding);

    void onLongClickEvent(int position, int viewId, B binding);

}
