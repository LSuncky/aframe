package com.suncky.frame.base.adapter;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

/**
 * 可选接口
 * Created by HY_PC on 2018 12/17 0017 17:23.
 */
public interface Optional<T> {
    int TYPE_NOME=-1;//不选
    int TYPE_SINGLE=0;//单选
    int TYPE_MULTI=1;//多选

    /**
     * {@code TYPE_NONE,TYPE_SINGLE,TYPE_MULTI}
     * @return
     */
    @OptionalType int OptionType();

    /**
     * 设置选择类型
     * @param type
     */
    void setOptionType(@OptionalType int type);

    /**
     * 选中所有选项
     */
    void selectAll();

    /**
     * 取消全部已选择项
     */
    void unSelectAll();

    /**
     * 选中选项
     * @param item
     */
    void selectOption(T item);

    /**
     * 取消选中选项
     * @param item
     */
    void unselectOption(T item);


    /**
     * 选项是否被选中
     * @param item
     * @return
     */
    boolean isSelected(T item);

    /**
     * 获取已选择的选项列表
     * @return
     */
    List<T> getSelectedOptions();

    /**
     * 设置已选择项
     * @param options
     */
    void setSelectedOptions(List<T> options);

    /**
     * 增加已选择项
     * @param options
     */
    void addSelectedOptions(List<T> options);

    /**
     * 设置是否可取消已选择项
     * @param able
     */
    void cancelable(boolean able);

    /**
     * 是否可取消已选择项
     * @return
     */
    boolean isCancelable();

    @Retention(value = RetentionPolicy.SOURCE)
    @IntDef({TYPE_NOME, TYPE_SINGLE, TYPE_MULTI})
    @interface OptionalType{

    }
}
