package com.suncky.frame.base;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.suncky.frame.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * 标题栏管理类
 */
public class DefaultTitleHelper {
    private View titleView;
    private TextView titleName;
    private TextView rightText;
    private ImageView leftImage;
    private TextView leftText;
    private ImageView rightImage;

    private LinearLayout leftLayout;
    private LinearLayout rightLayout;

    private boolean attached = false;

    public void attach(View view) {
        if (view == null) {
            return;
        }
        this.titleView = view;
        leftImage = view.findViewById(R.id.title_default_left_image);
        leftText = view.findViewById(R.id.title_default_left_text);
        leftLayout = view.findViewById(R.id.title_default_left_container);
        rightImage = view.findViewById(R.id.title_default_right_image);
        rightText = view.findViewById(R.id.title_default_right);
        rightLayout = view.findViewById(R.id.title_default_right_container);
        titleName = view.findViewById(R.id.title_default_title);
        attached = true;
    }

    public void detach() {
        this.titleView = null;
        leftImage = null;
        leftLayout = null;
        rightText = null;
        rightLayout = null;
        titleName = null;
        attached = false;
    }

    public boolean isAttached() {
        return attached;
    }

    public void hide() {
        if (!attached) {
            return;
        }
        titleView.setVisibility(GONE);
    }

    public void show() {
        if (!attached) {
            return;
        }
        titleView.setVisibility(VISIBLE);
    }

    public View getView() {
        return titleView;
    }

    public ImageView getLeftImage() {
        return leftImage;
    }

    public TextView getRightText() {
        return rightText;
    }

    public ImageView getRightImage(){
        return rightImage;
    }

    public TextView getLeftText() {
        return leftText;
    }

    public TextView getTitle() {
        return titleName;
    }

    public void setTitle(String title) {
        setTitle(title, 1);
    }

    public void setTitle(String title, int maxLines) {
        if (!attached) {
            return;
        }
        titleName.setText(title);
        titleName.setMaxLines(maxLines);
    }

    public void setTitle(int res) {
        if (!attached) {
            return;
        }
        titleName.setText(titleView.getContext().getString(res));
    }

    public void setRightText(String text) {
        if (!attached) {
            return;
        }
        if (!TextUtils.isEmpty(text)) {
            rightText.setText(text);
            rightText.setVisibility(VISIBLE);
        } else {
            rightText.setText("");
            rightText.setVisibility(GONE);
        }
    }

    public void setRightImageResource(int resId) {
        if (!attached) {
            return;
        }
        if (resId != 0) {
            rightImage.setImageResource(resId);
            rightImage.setVisibility(VISIBLE);
        } else {
            rightImage.setVisibility(GONE);
        }
    }

    public void setRightTextVisible(boolean visible) {
        if (!attached) {
            return;
        }
        if (visible) {
            rightText.setVisibility(VISIBLE);
        } else {
            rightText.setVisibility(GONE);
        }
    }

    public void setRightImageVisible(boolean visible) {
        if (!attached) {
            return;
        }
        if (visible) {
            rightImage.setVisibility(VISIBLE);
        } else {
            rightImage.setVisibility(GONE);
        }
    }

    public void setLeftImageResource(int resId) {
        if (!attached) {
            return;
        }
        if (resId != 0) {
            leftImage.setImageResource(resId);
            leftImage.setVisibility(VISIBLE);
        } else {
            leftImage.setVisibility(GONE);
        }
    }

    public void setLeftText(String text) {
        if (!attached) {
            return;
        }
        if (!TextUtils.isEmpty(text)) {
            leftText.setText(text);
            leftText.setVisibility(VISIBLE);
        } else {
            leftText.setText("");
            leftText.setVisibility(GONE);
        }
    }

    public void setLeftImageVisible(boolean visible) {
        if (!attached) {
            return;
        }
        if (visible) {
            leftImage.setVisibility(VISIBLE);
        } else {
            leftImage.setVisibility(GONE);
        }
    }

    public void setLeftTextVisible(boolean visible) {
        if (!attached) {
            return;
        }
        if (visible) {
            leftText.setVisibility(VISIBLE);
        } else {
            leftText.setVisibility(GONE);
        }
    }

    public void setLeftView(View v, LinearLayout.LayoutParams layoutParams) {
        if (!attached) {
            return;
        }
        if (v == null) {
            leftLayout.removeAllViews();
            return;
        }
        leftLayout.removeAllViews();
        if (layoutParams == null) {
            leftLayout.addView(v);
        } else {
            leftLayout.addView(v, layoutParams);
        }
    }

    public void setLeftView(View... vs) {
        if (!attached) {
            return;
        }
        leftLayout.removeAllViews();
        if (vs == null || vs.length == 0) {
            return;
        }
        for (int i = 0; i < vs.length; ++i) {
            leftLayout.addView(vs[i], i);
        }
    }

    public void addLeftView(View v, LinearLayout.LayoutParams layoutParams) {
        if (!attached||v==null) {
            return;
        }
        if (layoutParams == null) {
            leftLayout.addView(v);
        } else {
            leftLayout.addView(v, layoutParams);
        }
    }

    public void setLeftViewVisible(int index, boolean visible) {
        if (!attached) {
            return;
        }
        if (leftLayout.getChildAt(index) != null) {
            leftLayout.getChildAt(index).setVisibility(visible ? VISIBLE : GONE);
        }
    }

    public void setRightView(View v, LinearLayout.LayoutParams layoutParams) {
        if (!attached) {
            return;
        }
        if (v == null) {
            rightLayout.removeAllViews();
            return;
        }
        rightLayout.removeAllViews();
        if (layoutParams == null) {
            rightLayout.addView(v);
        } else {
            rightLayout.addView(v, layoutParams);
        }
    }

    public void setRightView(View... vs) {
        if (!attached) {
            return;
        }
        rightLayout.removeAllViews();
        if (vs == null || vs.length == 0) {
            return;
        }
        for (int i = 0; i < vs.length; ++i) {
            rightLayout.addView(vs[i], i);
        }
    }

    public void addRightView(View v, LinearLayout.LayoutParams layoutParams) {
        if (!attached||v==null) {
            return;
        }
        if (layoutParams == null) {
            rightLayout.addView(v);
        } else {
            rightLayout.addView(v, layoutParams);
        }
    }

    public void setRightViewVisible(int index, boolean visible) {
        if (!attached) {
            return;
        }
        if (rightLayout.getChildAt(index) != null) {
            rightLayout.getChildAt(index).setVisibility(visible ? VISIBLE : GONE);
        }
    }
}
