package com.suncky.frame.base.adapter;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.NonNull;

import com.suncky.frame.base.adapter.holder.ViewHolder;
import com.suncky.frame.base.adapter.listener.ListAdapterListener;

import java.util.List;

/**
 * 适用于列表或者下拉列表({@link AdapterView})的适配器,例如({@link ListView} or {@link Spinner})
 * @author gl
 * @time 2021/9/1 14:13
 */
public abstract class ArrayListAdapter<T> extends ListAdapter<T> {
    /**
     * 下拉列表布局id
     */
    private int mDropLayoutId;
    protected SparseArray<ViewHolder> dropViewHolders;
    protected int contentTextColor = -1;
    protected int dropdownTextColor= -1;
    protected int contentTextSize=0;
    protected int dropdownTextSize=0;


    public ArrayListAdapter(Context context) {
        this(context,null);
    }

    public ArrayListAdapter(Context context, List<T> data) {
        this(context, data,0,0,null);
    }

    /**
     *
     * @param context
     * @param data 数据
     * @param dropDownLayoutId 下拉列表布局id
     */
    public ArrayListAdapter(Context context, List<T> data, int dropDownLayoutId) {
        this(context,data,dropDownLayoutId,dropDownLayoutId);
    }

    /**
     *
     * @param context
     * @param data 数据
     * @param dropDownLayoutId 下拉列表布局id
     * @param contentLayoutId 显示在控件上的布局id
     */
    public ArrayListAdapter(Context context, List<T> data, int dropDownLayoutId, int contentLayoutId) {
        this(context,data,dropDownLayoutId,contentLayoutId,null);
    }

    /**
     *
     * @param context
     * @param data 数据
     * @param contentLayoutId 显示在控件上的布局id
     * @param dropDownLayoutId 下拉列表布局id
     * @param listener 事件监听
     */
    public ArrayListAdapter(Context context, List<T> data, int dropDownLayoutId, int contentLayoutId, ListAdapterListener listener) {
        super(context, data, contentLayoutId, listener);
        this.mDropLayoutId = dropDownLayoutId;
        this.dropViewHolders = new SparseArray<>();
    }

    /**
     * 设置显示在控件上的字体颜色
     */
    public void setContentTextColor(int contentTextColor) {
        this.contentTextColor = contentTextColor;
    }

    /**
     * 设置显示在下拉列表上的字体颜色
     */
    public void setDropdownTextColor(int dropdownTextColor) {
        this.dropdownTextColor = dropdownTextColor;
    }

    /**
     * 设置显示在控件上的字体大小(px)
     */
    public void setContentTextSize(int contentTextSize) {
        this.contentTextSize = contentTextSize;
    }

    /**
     * 设置显示在下拉列表上的字体大小(px)
     */
    public void setDropdownTextSize(int dropdownTextSize) {
        this.dropdownTextSize = dropdownTextSize;
    }

    /**
     * 设置下拉布局id
     * @param dropLayoutId 下拉布局id
     */
    public void setDropLayoutId(int dropLayoutId) {
        this.mDropLayoutId = dropLayoutId;
    }

    /**
     * 在该类中已废弃,使用 {@link #getContentViewHolder(int)}
     * @param position
     * @return
     */
    @Deprecated
    public ViewHolder getViewHolder(int position) {
        return super.getViewHolder(position);
    }

    /**
     * 获取显示在控件上的ViewHolder对象
     * @param position
     * @return
     */
    public ViewHolder getContentViewHolder(int position) {
        return getViewHolder(position);
    }

    /**
     *  获取下拉列表中的ViewHolder对象
     * @param position
     * @return
     */
    public ViewHolder getDropDownViewHolder(int position) {
        return dropViewHolders.get(position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if ( mDropLayoutId== 0) {
            return null;
        }
        final ViewHolder viewHolder = obtainDropViewHolder(parent, convertView, position);
        if (viewHolder == null) {
            return null;
        }
        convertDropDownView(viewHolder, getItem(position), position);
        dropViewHolders.put(position, viewHolder);
        return viewHolder.getConvertView();
    }

    /**
     * 显示下拉列表item数据
     * @param holder
     * @param item
     * @param position
     */
    public abstract void convertDropDownView(ViewHolder holder, T item, int position);

    private ViewHolder obtainDropViewHolder(ViewGroup parent, View convertView, int position) {
        if (convertView == null) {
            return onCreateDropViewHolder(parent, position);
        }else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.setPosition(position);
            return viewHolder;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        ViewHolder holder = super.onCreateViewHolder(parent, position);
        if (contentTextColor >= 0) {
            holder.setTextColor(android.R.id.text1, contentTextColor);
        }
        if (contentTextSize > 0) {
            holder.setTextSizePx(android.R.id.text1, contentTextSize);
        }
        return holder;
    }

    /**
     * 创建DropDownViewHolder实例
     * @param parent
     * @param position
     * @return
     */
    public ViewHolder onCreateDropViewHolder(ViewGroup parent, int position) {
        ViewHolder holder = new ViewHolder(LayoutInflater.from(mContext).inflate(mDropLayoutId, parent, false), position, this);
        if (dropdownTextColor >= 0) {
            holder.setTextColor(android.R.id.text1, dropdownTextColor);
        }
        if (dropdownTextSize > 0) {
            holder.setTextSizePx(android.R.id.text1, dropdownTextSize);
        }
        return holder;
    }
}
