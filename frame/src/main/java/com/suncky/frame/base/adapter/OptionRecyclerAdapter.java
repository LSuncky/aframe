package com.suncky.frame.base.adapter;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;

/**
 * 可选列表适配器
 * Created by HY_PC on 2018 11/21 0021 18:42.
 */
public abstract class OptionRecyclerAdapter<T> extends RecyclerViewAdapter<T> implements Optional<T> {
    protected List<T> selectedOptions=new ArrayList<>();
    protected boolean enable = true;//是否启用选择
    private boolean cancelable = true;//是否可取消选择
    private @OptionalType int optionType;

    public OptionRecyclerAdapter(Context context) {
        super(context);
    }

    public OptionRecyclerAdapter(Context context, List<T> data) {
        super(context, data);
    }

    public OptionRecyclerAdapter(Context context, List<T> data, int itemLayoutId) {
        super(context, data, itemLayoutId);
    }

    /**
     *
     * @param context
     * @param data
     * @param itemLayoutId
     * @param optionEnable 是否启用选择
     */
    public OptionRecyclerAdapter(Context context, List<T> data, int itemLayoutId, boolean optionEnable) {
        super(context, data, itemLayoutId);
        this.enable = optionEnable;
    }

    @Override
    public void setOptionType(@OptionalType int optionType) {
        this.optionType = optionType;
    }

    /**
     * option enabled true,otherwise false. default true;
     * @return
     */
    public boolean isEnable() {
        return enable;
    }

    /**
     * set option enable
     * @param enable enable true,otherwise false
     */
    public void setEnable(boolean enable) {
        this.enable = enable;
        clearChoice();
    }

    public List<T> getSelectedOptions() {
        return selectedOptions;
    }

    /**
     * 强行清除已选项
     */
    public void clearChoice(){
        selectedOptions.clear();
        notifyDataSetChanged();
    }

    /**
     * 选项变化,更新选项状态
     * @param position
     */
    public void notifySelectChange(int position) {
        notifySelectChange(getItem(position));
    }

    /**
     * 选项变化,更新选项状态
     * @param o
     */
    public void notifySelectChange(T o) {
        if (o == null || !isEnable()) {
            return;
        }
        if (isSelected(o)) {
            if (!isCancelable()) {
                return;
            }
            selectedOptions.remove(o);
        } else {
            if (optionType == Optional.TYPE_NOME) {
                return;
            }
            if (optionType == Optional.TYPE_SINGLE) {
                selectedOptions.clear();
            }
            selectedOptions.add(o);
        }
        notifyDataSetChanged();
    }

    /**
     * 选项变化,更新选项状态
     * @param position
     * @deprecated user {@link #notifySelectChange(int)} instead
     */
    public void select(int position) {
        select(getItem(position));
    }

    /**
     * 选项变化,更新选项状态
     * @param o
     * @deprecated user {@link #notifySelectChange(Object)} instead
     */
    public void select(T o){
        if (o == null || !isEnable()) {
            return;
        }
        switch (OptionType()) {
            case Optional.TYPE_SINGLE:
                if (!isSelected(o)) {
                    selectedOptions.clear();
                    selectedOptions.add(o);
                }
                break;
            case Optional.TYPE_MULTI:
                if (!isSelected(o)) {
                    selectedOptions.add(o);
                } else {
                    selectedOptions.remove(o);
                }
                break;
            case Optional.TYPE_NOME:
                break;
        }
        notifyDataSetChanged();
    }

    public void selectOption(int position) {
        selectOption(getItem(position));
    }

    @Override
    public void selectOption(T item) {
        if (item == null || !isEnable() || isSelected(item)) {
            return;
        }
        if (optionType == Optional.TYPE_SINGLE) {
            selectedOptions.clear();
        }
        selectedOptions.add(item);
        notifyDataSetChanged();
    }

    public void unselectOption(int position) {
        unselectOption(getItem(position));
    }

    @Override
    public void unselectOption(T item) {
        if (item == null || !isEnable() || !isCancelable()) {
            return;
        }
        if (isSelected(item)) {
            selectedOptions.remove(item);
            notifyDataSetChanged();
        }
    }

    @Override
    public void selectAll(){
        if (isEnable() && OptionType() == Optional.TYPE_MULTI) {
            selectedOptions.clear();
            selectedOptions.addAll(mDatas);
            notifyDataSetChanged();
        }
    }

    @Override
    public void unSelectAll() {
        if (isEnable() && isCancelable()) {
            selectedOptions.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public boolean isSelected(T object){
        if (object == null) {
            return false;
        }
        return selectedOptions.contains(object);
    }

    @Override
    public void setSelectedOptions(List<T> selectedOptions) {
        if (selectedOptions == null || selectedOptions.size() == 0) {
            return;
        }
        this.selectedOptions.clear();
        this.selectedOptions.addAll(selectedOptions);
        notifyDataSetChanged();
    }

    @Override
    public void addSelectedOptions(List<T> options) {
        if (options == null || options.size() == 0) {
            return;
        }
        for (T t : options) {
            if (!this.selectedOptions.contains(t)) {
                this.selectedOptions.add(t);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int OptionType() {
        return optionType;
    }

    @Override
    public void cancelable(boolean able) {
        this.cancelable = able;
    }

    @Override
    public boolean isCancelable() {
        return this.cancelable;
    }
}
