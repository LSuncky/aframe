package com.suncky.frame.base.adapter.holder;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestListener;
import com.suncky.frame.base.adapter.RecyclerViewAdapter;
import com.suncky.frame.base.adapter.listener.OnItemClickListener;
import com.suncky.frame.utils.SingleClickManager;
import com.suncky.frame.utils.glide.GlideUtils;

/**
 * Created by HY_PC on 2018 11/26 0026 14:01.
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
        View.OnLongClickListener, OnItemClickListener {
    private final SparseArray<View> mViews;
    private SubViewEventListener mEventListener;

    public RecyclerViewHolder(View itemView, SubViewEventListener eventListener) {
        super(itemView);
        this.mViews = new SparseArray<>();
        this.mEventListener = eventListener;
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public RecyclerViewHolder(ViewGroup parent, int layoutId, SubViewEventListener eventListener) {
        super(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent,
                false));
        this.mViews = new SparseArray<>();
        this.mEventListener = eventListener;
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    /**
     * 通过控件的Id获取对于的控件，如果没有则加入views
     *
     * @param viewId
     * @return
     */
    public <T extends View> T getView(int viewId) {
        View view = mViews.get(viewId);
        if (view == null) {
            view = itemView.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * 为TextView设置字符串
     * @param viewId
     * @param text
     * @return
     */
    public RecyclerViewHolder setText(int viewId, String text) {
        TextView view = getView(viewId);
        view.setText(text);
        return this;
    }

    /**
     * 为TextView设置字符串资源
     * @param viewId
     * @param resId
     * @return
     */
    public RecyclerViewHolder setText(int viewId, int resId) {
        TextView view = getView(viewId);
        view.setText(resId);
        return this;
    }

    /**
     * 为ImageView设置图片资源
     * @param viewId
     * @param drawableId
     * @return
     */
    public RecyclerViewHolder setImageResource(int viewId, int drawableId) {
        ImageView view = getView(viewId);
        GlideUtils.load(itemView.getContext(),drawableId,view,0);
        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param bm
     * @return
     */
    public RecyclerViewHolder setImageBitmap(int viewId, Bitmap bm) {
        ImageView view = getView(viewId);
        view.setImageBitmap(bm);
        return this;
    }

    /**
     * 为ImageView设置图片url
     *
     * @param viewId
     * @param url
     * @return
     */
    public RecyclerViewHolder setImageUrl(int viewId, String url, int defRes) {
        ImageView view = getView(viewId);
        GlideUtils.load(itemView.getContext(), url, view, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片url和监听
     *
     * @param viewId
     * @param url
     * @param requestListener
     * @return
     */
    public RecyclerViewHolder setImageUrl(int viewId, String url, int defRes, RequestListener<Drawable> requestListener) {
        ImageView view = getView(viewId);
        GlideUtils.load(itemView.getContext(), url, view, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param url
     * @return
     */
    public RecyclerViewHolder setImageUrl(int viewId, String url , int radius, boolean isCircle, int defRes) {
        ImageView view = getView(viewId);
        GlideUtils.load(itemView.getContext(), url, view, radius, isCircle, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片url和监听
     *
     * @param viewId
     * @param url
     * @param requestListener
     * @return
     */
    public RecyclerViewHolder setImageUrl(int viewId, String url , int radius, boolean isCircle, int defRes, RequestListener<Drawable> requestListener) {
        ImageView view = getView(viewId);
        GlideUtils.load(itemView.getContext(), url, view, radius, isCircle, defRes, requestListener);
        return this;
    }

    /**
     * 设置点击事件
     *
     * @param viewId
     * @return
     */
    public RecyclerViewHolder setClickListener(int viewId) {
        getView(viewId).setOnClickListener(this);
        return this;
    }

    /**
     * 设置item点击事件
     * @param viewId
     * @return
     */
    public RecyclerViewHolder setItemClickListener(int viewId){
        RecyclerView recyclerView = getView(viewId);
        if(recyclerView.getAdapter() instanceof RecyclerViewAdapter){
            ((RecyclerViewAdapter<?>)recyclerView.getAdapter()).setOnItemClickListener(this);
        }
        return this;
    }

    /**
     * 设置可见性
     * @param viewId
     * @param visibility
     */
    public RecyclerViewHolder setVisibility(int viewId, int visibility) {
        getView(viewId).setVisibility(visibility);
        return this;
    }

    /**
     * 设置背景
     * @param viewId
     * @param resId
     * @return
     */
    public RecyclerViewHolder setBackgroundResource(int viewId, int resId){
        getView(viewId).setBackgroundResource(resId);
        return this;
    }

    /**
     * 设置文字颜色
     * @param viewId
     * @param color
     * @return
     */
    public RecyclerViewHolder setTextColor(int viewId, int color){
        TextView view = getView(viewId);
        view.setTextColor(color);
        return this;
    }

    public RecyclerViewHolder setEnable(int viewId, boolean edable){
        getView(viewId).setEnabled(edable);
        return this;
    }

    /**
     * 设置适配器
     * @param viewId
     * @param adapter
     * @return
     */
    public RecyclerViewHolder setAdapter(int viewId, RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter) {
        RecyclerView recyclerView = getView(viewId);
        recyclerView.setAdapter(adapter);
        return this;
    }

    /**
     * 获取适配器
     * @param viewId
     * @return
     */
    public RecyclerView.Adapter<? extends RecyclerView.ViewHolder> getAdapter(int viewId){
        RecyclerView recyclerView = getView(viewId);
        return (RecyclerView.Adapter<? extends RecyclerView.ViewHolder>)recyclerView.getAdapter();
    }

    @Override
    public void onClick(View v) {
        if (SingleClickManager.isReClick(v.hashCode())) {
            return;
        }
        if (mEventListener != null) {
            mEventListener.onClick(v, getAdapterPosition());
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mEventListener != null) {
            mEventListener.onLongClick(v, getAdapterPosition());
        }
        return true;//避免长按时间和点击事件同时触发
    }

    @Override
    public void onItemClick(View v, int position) {
        if (SingleClickManager.isReClick(v.hashCode())) {
            return;
        }
        if (mEventListener != null) {
            mEventListener.onItemClick(v, getAdapterPosition(), position);
        }
    }

    public interface SubViewEventListener {
        void onClick(View v, int position);
        void onLongClick(View v, int position);
        void onItemClick(View v, int position, int subPosition);
    }
}
