package com.suncky.frame.base.mvvm.kt

import androidx.lifecycle.LifecycleOwner
import com.suncky.frame.base.mvvm.StateLiveData
import com.suncky.frame.base.mvvm.StateObserver
import com.suncky.frame.http.bean.Page
import com.suncky.frame.http.bean.Result

open class KStateLiveData<E,P : Page, R : Result<E, P>>: StateLiveData<E,P,R>() {
    fun observeState(owner: LifecycleOwner?, listener: StateObserverListener<E,P>.()->Unit) {
        val callback= StateObserverListener<E,P>().also(listener)
        val observer= object : StateObserver<E, P, R>() {
            override fun onSuccess(data: E?, page: P?) {
                callback.onSuccess(data, page)
            }

            override fun onError(code: Int, message: String?) {
                callback.onError(code, message)
            }

            override fun onPreLoad() {
                callback.onPreLoad()
            }

            override fun onLoading() {
                callback.onLoading()
            }

            override fun onLoadFinish() {
                callback.onLoadFinish()
            }
        }
        super.observeState(owner, observer)
    }
}