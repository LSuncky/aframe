package com.suncky.frame.base.mvvm;

import androidx.lifecycle.Observer;

import com.suncky.frame.http.bean.Page;
import com.suncky.frame.http.bean.Result;

/**
 * @author gl
 * @time 2021/12/8 17:43
 */
public abstract class StateObserver<E,P extends Page,R extends Result<E,P>>{
    private final Observer<R> observer = new Observer<R>() {
        @Override
        public void onChanged(R r) {
            StateObserver.this.onChanged(r);
        }
    };
    public void onChanged(R r) {
        if (r == null) {
            return;
        }
        if (r.isSuccess()) {
            onSuccess(r.getData(), r.getPage());
        } else {
            onError(r.getCode(),r.getMessage());
        }
    }

    public Observer<R> getObserver() {
        return observer;
    }

    public abstract void onSuccess(E data, P page);
    public abstract void onError(int code, String message);

    public void onPreLoad() {

    }

    public void onLoading() {

    }

    public void onLoadFinish() {

    }
}
