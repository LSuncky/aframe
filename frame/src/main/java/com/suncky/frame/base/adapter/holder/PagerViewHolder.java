package com.suncky.frame.base.adapter.holder;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestListener;
import com.suncky.frame.utils.glide.GlideUtils;

/**
 * 管理pager页面中的view
 * Created by HY_PC on 2018 11/26 0026 14:01.
 */
public class PagerViewHolder implements View.OnClickListener, AdapterView.OnItemClickListener {
    private final SparseArray<View> mViews;
    private int mPosition;
    private View mRootView;
    private ViewEventListener mEventListener;

    private PagerViewHolder(ViewGroup parent, View contentView,
                            int position, ViewEventListener eventListener) {
        this.mPosition = position;
        this.mRootView = contentView;
        this.mViews = new SparseArray<View>();
        this.mEventListener = eventListener;
    }

    /**
     * 拿到一个ViewHolder对象
     * @param parent
     * @param position
     * @return
     */
    public static PagerViewHolder get(View contentView, ViewGroup parent, int position,
                                      ViewEventListener eventListener) {
        PagerViewHolder viewHolder = (PagerViewHolder) contentView.getTag();
        if (viewHolder == null) {
            viewHolder = new PagerViewHolder(parent,contentView, position, eventListener);
            contentView.setTag(viewHolder);
        }
        return viewHolder;
    }

    /**
     * 获取item布局
     * @param <T>
     * @return
     */
    public <T extends View> T getRootView() {
        return (T) mRootView;
    }

    /**
     * 通过控件的Id获取对于的控件，如果没有则加入views
     *
     * @param viewId
     * @return
     */
    public <T extends View> T getView(int viewId) {
        View view = mViews.get(viewId);
        if (view == null) {
            view = mRootView.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * 为TextView设置字符串
     * @param viewId
     * @param text
     * @return
     */
    public PagerViewHolder setText(int viewId, String text) {
        TextView view = getView(viewId);
        view.setText(text);
        return this;
    }

    /**
     * 为TextView设置字符串资源
     * @param viewId
     * @param resId
     * @return
     */
    public PagerViewHolder setText(int viewId, int resId) {
        TextView view = getView(viewId);
        view.setText(resId);
        return this;
    }

    /**
     * 为ImageView设置图片资源
     * @param viewId
     * @param drawableId
     * @return
     */
    public PagerViewHolder setImageResource(int viewId, int drawableId) {
        ImageView view = getView(viewId);
        view.setImageResource(drawableId);

        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param bm
     * @return
     */
    public PagerViewHolder setImageBitmap(int viewId, Bitmap bm) {
        ImageView view = getView(viewId);
        view.setImageBitmap(bm);
        return this;
    }

    /**
     * 为ImageView设置图片url
     *
     * @param viewId
     * @param url
     * @return
     */
    public PagerViewHolder setImageUrl(int viewId, String url, int defRes) {
        ImageView view = getView(viewId);
        GlideUtils.load(mRootView.getContext(), url, view, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片url和监听
     *
     * @param viewId
     * @param url
     * @param requestListener
     * @return
     */
    public PagerViewHolder setImageUrl(int viewId, String url, int defRes, RequestListener<Drawable> requestListener) {
        ImageView view = getView(viewId);
        GlideUtils.load(mRootView.getContext(), url, view, 0,false,defRes, requestListener);
        return this;
    }

    /**
     * 为ImageView设置图片url
     *
     * @param viewId
     * @param url
     * @return
     */
    public PagerViewHolder setImageUrl(int viewId, String url , int radius, boolean isCircle, int defRes) {
        ImageView view = getView(viewId);
        GlideUtils.load(mRootView.getContext(), url, view, radius, isCircle, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片url和监听
     *
     * @param viewId
     * @param url
     * @param requestListener
     * @return
     */
    public PagerViewHolder setImageUrl(int viewId, String url , int radius, boolean isCircle, int defRes, RequestListener<Drawable> requestListener) {
        ImageView view = getView(viewId);
        GlideUtils.load(mRootView.getContext(), url, view, radius, isCircle, defRes, requestListener);
        return this;
    }

    /**
     * 设置点击事件
     *
     * @param viewId
     * @return
     */
    public PagerViewHolder setClickListener(int viewId) {
        getView(viewId).setOnClickListener(this);
        return this;
    }

    /**
     * 设置可见性
     * @param viewId
     * @param visibility
     */
    public PagerViewHolder setVisibility(int viewId, int visibility) {
        getView(viewId).setVisibility(visibility);
        return this;
    }

    /**
     * 设置背景
     * @param viewId
     * @param resId
     * @return
     */
    public PagerViewHolder setBackgroundResource(int viewId, int resId){
        getView(viewId).setBackgroundResource(resId);
        return this;
    }

    /**
     * 设置文字颜色
     * @param viewId
     * @param color
     * @return
     */
    public PagerViewHolder setTextColor(int viewId, int color){
        TextView view = getView(viewId);
        view.setTextColor(color);
        return this;
    }

    public PagerViewHolder setEnable(int viewId, boolean edable){
        getView(viewId).setEnabled(edable);
        return this;
    }

    /**
     * 设置item点击事件
     * @param viewId
     * @return
     */
    public PagerViewHolder setItemClickListener(int viewId){
        AdapterView<?> adapterView = getView(viewId);
        adapterView.setOnItemClickListener(this);
        return this;
    }

    /**
     * 设置适配器
     * @param viewId
     * @param adapter
     * @return
     */
    public PagerViewHolder setAdapter(int viewId, Adapter adapter) {
        AdapterView<Adapter> adapterView = getView(viewId);
        adapterView.setAdapter(adapter);
        return this;
    }

    /**
     * 获取适配器
     * @param viewId
     * @param <T>
     * @return
     */
    public <T extends Adapter> T getAdapter(int viewId){
        AdapterView<T> adapterView = getView(viewId);
        return (T) adapterView.getAdapter();
    }

    public int getPosition() {
        return mPosition;
    }

    @Override
    public void onClick(View v) {
        if (mEventListener != null) {
            mEventListener.onClick(v, mPosition);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mEventListener != null) {
            mEventListener.onItemClick(parent, mPosition, position);
        }
    }

    public interface ViewEventListener {
        void onClick(View v, int position);
        void onItemClick(AdapterView<?> v, int position, int subPosition);
    }
}
