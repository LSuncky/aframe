package com.suncky.frame.base.mvvm;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BaseViewModel extends ViewModel {
    protected MutableLiveData<LoadState> loadState = new MutableLiveData<>();

    public MutableLiveData<LoadState> getLoadState() {
        return loadState;
    }

    public void setLoadState(int state, int id, String desc) {
        loadState.setValue(LoadState.newInstance(state, id, desc));
    }
    public void setLoadState(int state, int id) {
        loadState.setValue(LoadState.newInstance(state, id));
    }

    public void setLoadState(int state) {
        loadState.setValue(LoadState.newInstance(state));
    }

    public void postLoadState(int state, int id, String desc) {
        loadState.postValue(LoadState.newInstance(state, id, desc));
    }

    public void postLoadState(int state, int id) {
        loadState.postValue(LoadState.newInstance(state, id));
    }

    public void postLoadState(int state) {
        loadState.postValue(LoadState.newInstance(state));
    }

    public void onPreLoad(int id, String desc) {
        postLoadState(LoadState.PRE_LOAD, id, desc);
    }

    public void onPreLoad(String desc) {
        onPreLoad(-1, desc);
    }

    public void onLoading(int id, String desc) {
        postLoadState(LoadState.LOADING, id, desc);
    }

    public void onLoading(String desc) {
        onLoading(-1, desc);
    }

    public void onLoadFinish(int id, String desc) {
        postLoadState(LoadState.FINISHED, id, desc);
    }

    public void onLoadFinish(String desc) {
        onLoadFinish(-1, desc);
    }
}
