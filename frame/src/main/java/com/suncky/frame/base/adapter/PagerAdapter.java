package com.suncky.frame.base.adapter;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;

import com.suncky.frame.base.adapter.holder.PagerViewHolder;
import com.suncky.frame.base.adapter.listener.ListAdapterListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/12/16.
 */
public abstract class PagerAdapter<T> extends androidx.viewpager.widget.PagerAdapter implements PagerViewHolder.ViewEventListener {

    protected Context mContext;
    protected List<T> mDataList;
    protected SparseArray<View> views;
    protected ListAdapterListener mListener;

    public PagerAdapter(Context context){
        this.mContext=context;
        views = new SparseArray<>();
    }

    public PagerAdapter(Context context, List<T> dataList){
        this.mContext=context;
        views = new SparseArray<>();
        if (dataList==null){
            mDataList=new ArrayList<>();
        }else {
            this.mDataList = dataList;
        }
    }

    /**
     * 设置adapter监听
     * @param listener
     */
    public void setListener(ListAdapterListener listener){
        this.mListener = listener;
    }

    public void setData(List<T> dataList){
        if (dataList==null){
            mDataList=new ArrayList<>();
        }else {
            this.mDataList = dataList;
        }
        notifyDataSetChanged();
    }

    public void addData(List<T> dataList){
        if (dataList == null) {
            return;
        }
        if (mDataList==null){
            mDataList=new ArrayList<>();
        }
        mDataList.addAll(dataList);
        notifyDataSetChanged();
    }

    public void addData(T data){
        if (data == null) {
            return;
        }
        if (mDataList==null){
            mDataList=new ArrayList<>();
        }
        mDataList.add(data);
        notifyDataSetChanged();
    }

    public void removeData(int position) {
        if (mDataList == null || position < 0 || position >= mDataList.size()) {
            return;
        }
        mDataList.remove(position);
        notifyDataSetChanged();
    }

    public void removeData(T data) {
        if (mDataList == null || data == null) {
            return;
        }
        mDataList.remove(data);
        notifyDataSetChanged();
    }

    public List<T> getData() {
        return mDataList;
    }

    public T getItem(int position) {
        if (mDataList == null || position < 0 || position > mDataList.size() - 1) {
            return null;
        }
        return mDataList.get(position);
    }

    @NonNull
    @Override
    public final Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = views.get(position);
        if (view == null) {
            view = getItemView(position);
            convert(PagerViewHolder.get(view, container, position, this), getItem(position), position);
            views.put(position, view);
        } else if (willUpdate(position)) {
            convert(PagerViewHolder.get(view, container, position, this), getItem(position), position);
        }
        container.addView(view);
        return view;
    }

    /**
     * 返回position位置下的内容布局
     * @param position
     * @return
     */
    protected abstract View getItemView(int position);

    /**
     * 数据处理
     * @param holder
     * @param item
     * @param position
     */
    protected abstract void convert(PagerViewHolder holder, T item, int position);

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    /**
     * 当数据改变时是否刷新
     * @param position
     * @return true-刷新  false-不刷新  默认返回true
     */
    public boolean willUpdate(int position) {
        return true;
    }

    @Override
    public void onClick(View v, int position) {
        if (mListener != null) {
            mListener.onClickEvent(position, v.getId());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> v, int position, int subPosition) {
        if (mListener != null) {
            mListener.onItemClickEvent(position, subPosition, v.getId());
        }
    }
}
