package com.suncky.frame.base.adapter.holder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestListener;
import com.suncky.frame.utils.SingleClickManager;
import com.suncky.frame.utils.glide.GlideUtils;

/**
 * Created by HY_PC on 2018 11/26 0026 14:01.
 */
public class ViewHolder implements View.OnClickListener, AdapterView.OnItemClickListener {
    private final SparseArray<View> mViews;
    private int mPosition;
    private final View mConvertView;
    private final ViewEventListener mEventListener;

    public ViewHolder(View convertView,int position, ViewEventListener eventListener) {
        this.mPosition = position;
        this.mViews = new SparseArray<View>();
        mConvertView = convertView;
        // setTag
        mConvertView.setTag(this);
        this.mEventListener = eventListener;
    }

    public View getConvertView() {
        return mConvertView;
    }

    /**
     * 通过控件的Id获取对于的控件，如果没有则加入views
     *
     * @param viewId
     * @return
     */
    public <T extends View> T getView(int viewId) {
        View view = mViews.get(viewId);
        if (view == null) {
            view = mConvertView.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * 为TextView设置字符串
     * @param viewId
     * @param text
     * @return
     */
    public ViewHolder setText(int viewId, String text) {
        TextView view = getView(viewId);
        view.setText(text);
        return this;
    }

    /**
     * 为TextView设置字符串资源
     * @param viewId
     * @param resId
     * @return
     */
    public ViewHolder setText(int viewId, int resId) {
        TextView view = getView(viewId);
        view.setText(resId);
        return this;
    }

    /**
     * 为ImageView设置图片
     * @param viewId
     * @param resId
     * @return
     */
    public ViewHolder setImageResource(int viewId, int resId) {
        ImageView view = getView(viewId);
        view.setImageResource(resId);
        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param bm
     * @return
     */
    public ViewHolder setImageBitmap(int viewId, Bitmap bm) {
        ImageView view = getView(viewId);
        view.setImageBitmap(bm);
        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param url
     * @return
     */
    public ViewHolder setImageUrl(int viewId, String url, int defRes) {
        ImageView view = getView(viewId);
        GlideUtils.load(mConvertView.getContext(), url, view, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片url和监听
     *
     * @param viewId
     * @param url
     * @param requestListener
     * @return
     */
    public ViewHolder setImageUrl(int viewId, String url, int defRes, RequestListener<Drawable> requestListener) {
        ImageView view = getView(viewId);
        GlideUtils.load(mConvertView.getContext(), url, view, 0,false,defRes, requestListener);
        return this;
    }

    /**
     * 为ImageView设置图片
     *
     * @param viewId
     * @param url
     * @return
     */
    public ViewHolder setImageUrl(int viewId, String url , int radius, boolean isCircle, int defRes) {
        ImageView view = getView(viewId);
        GlideUtils.load(mConvertView.getContext(), url, view, radius, isCircle, defRes);
        return this;
    }

    /**
     * 为ImageView设置图片url和监听
     *
     * @param viewId
     * @param url
     * @param requestListener
     * @return
     */
    public ViewHolder setImageUrl(int viewId, String url , int radius, boolean isCircle, int defRes, RequestListener<Drawable> requestListener) {
        ImageView view = getView(viewId);
        GlideUtils.load(mConvertView.getContext(), url, view, radius, isCircle, defRes, requestListener);
        return this;
    }

    /**
     * 设置点击事件
     *
     * @param viewId
     * @return
     */
    public ViewHolder setClickListener(int viewId) {
        getView(viewId).setOnClickListener(this);
        return this;
    }

    /**
     * 设置可见性
     * @param viewId
     * @param visibility
     */
    public ViewHolder setVisibility(int viewId, int visibility) {
        getView(viewId).setVisibility(visibility);
        return this;
    }

    /**
     * 设置背景
     * @param viewId
     * @param resId
     * @return
     */
    public ViewHolder setBackgroundResource(int viewId, int resId){
        getView(viewId).setBackgroundResource(resId);
        return this;
    }

    /**
     * 设置文字颜色
     * @param viewId
     * @param color
     * @return
     */
    public ViewHolder setTextColor(int viewId, int color){
        TextView view = getView(viewId);
        view.setTextColor(color);
        return this;
    }

    /**
     * 设置文字大小
     * @param viewId
     * @param spSize The scaled pixel size
     * @return
     */
    public ViewHolder setTextSizeSp(int viewId, float spSize){
        TextView view = getView(viewId);
        view.setTextSize(spSize);
        return this;
    }

    /**
     * 设置文字大小
     * @param viewId
     * @param pxSize The pixel size
     * @return
     */
    public ViewHolder setTextSizePx(int viewId, float pxSize){
        TextView view = getView(viewId);
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX,pxSize);
        return this;
    }

    public ViewHolder setEnable(int viewId, boolean enable){
        getView(viewId).setEnabled(enable);
        return this;
    }

    /**
     * 设置item点击事件
     * @param viewId
     * @return
     */
    public ViewHolder setItemClickListener(int viewId){
        AdapterView<?> adapterView = getView(viewId);
        adapterView.setOnItemClickListener(this);
        return this;
    }

    /**
     * 设置适配器
     * @param viewId
     * @param adapter
     * @return
     */
    public ViewHolder setAdapter(int viewId, Adapter adapter) {
        AdapterView<Adapter> adapterView = getView(viewId);
        adapterView.setAdapter(adapter);
        return this;
    }

    /**
     * 获取适配器
     * @param viewId
     * @param <T>
     * @return
     */
    public <T extends Adapter> T getAdapter(int viewId){
        AdapterView<T> adapterView = getView(viewId);
        return (T) adapterView.getAdapter();
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        this.mPosition = position;
    }

    @Override
    public void onClick(View v) {
        if (SingleClickManager.isReClick(v.hashCode())) {
            return;
        }
        if (mEventListener != null) {
            mEventListener.onClick(v, mPosition);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (SingleClickManager.isReClick(view.hashCode())) {
            return;
        }
        if (mEventListener != null) {
            mEventListener.onItemClick(parent, mPosition, position);
        }
    }

    public interface ViewEventListener {
        void onClick(View v, int position);
        void onItemClick(AdapterView<?> v, int position, int subPosition);
    }
}
