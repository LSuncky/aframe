package com.suncky.frame.paging;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.paging.DataSource;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.suncky.frame.http.bean.Page;
import com.suncky.frame.http.bean.Result;

import java.util.List;

/**
 * paging分页组件工具类
 * @author gl
 * @time 2021/4/15 15:17
 */
public class PagingHelper<T,P extends Page, R extends Result<List<T>, P>> {
    private LiveData<PagedList<T>> pagedListLiveData;
    private IntegerPageKeyedDataSource<T,P,R> dataSource;
    private DataSource.Factory<Integer, T> factory;
    private LifecycleOwner owner;
    private DataChangeListener<T> dataChangeListener;
    private int pageSize = 20;
    private int prefetchDistance = -1;
    private int initialLoadSizeHint = -1;
    private boolean enablePlaceholders = true;

    public PagingHelper(LifecycleOwner owner) {
        this(owner,null);
    }

    public PagingHelper(LifecycleOwner owner, IntegerPageKeyedDataSource<T,P,R> dataSource) {
        this(owner,20, 10, dataSource);
    }

    public PagingHelper(LifecycleOwner owner, int pageSize, int prefetchDistance, IntegerPageKeyedDataSource<T,P,R> dataSource) {
        this(owner,pageSize, prefetchDistance, pageSize, dataSource);
    }

    public PagingHelper(LifecycleOwner owner, int pageSize, int prefetchDistance, int initialLoadSizeHint, IntegerPageKeyedDataSource<T,P,R> dataSource) {
        this(owner,pageSize, prefetchDistance, initialLoadSizeHint, true,dataSource);
    }

    public PagingHelper(LifecycleOwner owner, int pageSize, int prefetchDistance, int initialLoadSizeHint, boolean enablePlaceholders,
                        IntegerPageKeyedDataSource<T,P,R> dataSource) {
        this.owner = owner;
        this.pageSize = pageSize;
        this.prefetchDistance = prefetchDistance;
        this.initialLoadSizeHint = initialLoadSizeHint;
        this.enablePlaceholders = enablePlaceholders;
        this.dataSource = dataSource;
        init();
    }

    private void init() {
        factory = new DataSource.Factory<Integer, T>() {
            @NonNull
            @Override
            public DataSource<Integer, T> create() {
                return dataSource;
            }
        };
        this.pagedListLiveData = new LivePagedListBuilder<>(factory,
                new PagedList.Config.Builder().setPageSize(pageSize).setInitialLoadSizeHint(initialLoadSizeHint)
                        .setEnablePlaceholders(enablePlaceholders).setPrefetchDistance(prefetchDistance).build()).build();
    }

    public void setDataSource(IntegerPageKeyedDataSource<T,P,R> dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataChangeListener(DataChangeListener<T> dataChangeListener) {
        this.dataChangeListener = dataChangeListener;
        this.pagedListLiveData.observe(owner, new Observer<PagedList<T>>() {
            @Override
            public void onChanged(PagedList<T> pagedList) {
                if (dataChangeListener != null) {
                    dataChangeListener.onChanged(pagedList);
                }
            }
        });
    }

    public void refreshData() {
        pagedListLiveData = new LivePagedListBuilder<>(factory,
                new PagedList.Config.Builder().setPageSize(pageSize).setInitialLoadSizeHint(initialLoadSizeHint)
                        .setEnablePlaceholders(enablePlaceholders).setPrefetchDistance(prefetchDistance).build()).build();
        pagedListLiveData.observe(owner, new Observer<PagedList<T>>() {
            @Override
            public void onChanged(PagedList<T> pagedList) {
                if (dataChangeListener != null) {
                    dataChangeListener.onChanged(pagedList);
                }
            }
        });
    }

    public interface DataChangeListener<T>{
        void onChanged(PagedList<T> pagedList);
    }
}
