package com.suncky.frame.paging;

/**
 * @author gl
 * @time 2021/4/15 16:15
 */
public class LoadType {
    public static final int INIT = 1;
    public static final int APPEND = 2;
}
