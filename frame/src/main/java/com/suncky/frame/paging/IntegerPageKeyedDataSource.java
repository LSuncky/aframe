package com.suncky.frame.paging;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.suncky.frame.http.bean.Page;
import com.suncky.frame.http.bean.Result;

import java.util.List;

/**
 * 整型页码PagingDataSource
 * @author gl
 * @time 2021/4/15 15:12
 */
public abstract class IntegerPageKeyedDataSource<Value, P extends Page, R extends Result<List<Value>, P>> extends PageKeyedDataSource<Integer, Value> {

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Value> callback) {
        load(new PagingLoadParams(params, LoadType.INIT), new PagingCallBack<>(callback, LoadType.INIT));
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Value> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Value> callback) {
        load(new PagingLoadParams(params, LoadType.APPEND), new PagingCallBack<>(callback, LoadType.APPEND));
    }

    public abstract void load(PagingLoadParams loadParams, PagingCallBack<Value, P, R> callBack);
}
