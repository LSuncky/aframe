package com.suncky.frame.paging;

import androidx.paging.PageKeyedDataSource;

/**
 * @author gl
 * @time 2021/4/16 14:00
 */
public class PagingLoadParams {
    private PageKeyedDataSource.LoadInitialParams<Integer> initialParams;
    private PageKeyedDataSource.LoadParams<Integer> loadParams;
    private int loadType;

    public PagingLoadParams(PageKeyedDataSource.LoadInitialParams<Integer> initialParams, int loadType) {
        this.initialParams = initialParams;
        this.loadType = loadType;
    }

    public PagingLoadParams(PageKeyedDataSource.LoadParams<Integer> loadParams, int loadType) {
        this.loadParams = loadParams;
        this.loadType = loadType;
    }

    public int getLoadType() {
        return loadType;
    }

    public int getNextPage() {
        if (loadType == LoadType.APPEND && loadParams != null) {
            return loadParams.key;
        } else {
            return 1;
        }
    }
}
