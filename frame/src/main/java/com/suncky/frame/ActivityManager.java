package com.suncky.frame;


import android.app.Activity;
import android.text.TextUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ActivityManager {

    private static ActivityManager mInstance;

    private final List<WeakReference<Activity>> mList = new ArrayList<>();

    private ActivityManager() {
    }

    public static ActivityManager getInstance() {
        if (mInstance == null) {
            synchronized (ActivityManager.class) {
                if (mInstance == null) {
                    mInstance = new ActivityManager();
                }
            }
        }
        return mInstance;
    }

    public void attach(Activity activity) {
        clearInvalidData(activity);
        mList.add(new WeakReference<>(activity));
    }

    public void detach(Activity activity) {
        Iterator<WeakReference<Activity>> iterator = mList.iterator();
        while (iterator.hasNext()){
            WeakReference<Activity> weakReference = iterator.next();
            if (weakReference == null) {
                continue;
            }
            Activity weakReferenceActivity = weakReference.get();
            if (weakReferenceActivity == null) {
                continue;
            }
            if (weakReferenceActivity == activity){
                iterator.remove();
            }
        }
    }

    private void clearInvalidData(Activity activity) {
        List<WeakReference<Activity>> list = new ArrayList<>();
        list.add(null);
        for (WeakReference<Activity> weakReference : mList) {
            if (weakReference == null) {
                continue;
            }
            Activity weakReferenceActivity = weakReference.get();
            if (weakReferenceActivity == null || (weakReferenceActivity.getClass() == activity.getClass())) {
                list.add(weakReference);
            }
        }
        mList.removeAll(list);
    }

    public void finishAll() {
        for (WeakReference<Activity> weakReference : mList) {
            if (weakReference == null) {
                continue;
            }
            Activity activity = weakReference.get();
            if (activity == null) {
                continue;
            }
            activity.finish();
        }
    }

    public void finish(Class<Activity>... classes) {
        for (Class<Activity> c : classes) {
            for (WeakReference<Activity> weakReference : mList) {
                if (weakReference == null) {
                    continue;
                }
                Activity activity = weakReference.get();
                if (activity == null) {
                    continue;
                }
                if (TextUtils.equals(c.getName(), activity.getClass().getName())) {
                    activity.finish();
                }
            }
        }
    }

    public void finishOther(Class<? extends Activity> aClass) {
        for (WeakReference<Activity> weakReference : mList) {
            if (weakReference == null) {
                continue;
            }
            Activity activity = weakReference.get();
            if (activity == null) {
                continue;
            }
            if (!TextUtils.equals(aClass.getName(), activity.getClass().getName())) {
                activity.finish();
            }
        }
    }
}
